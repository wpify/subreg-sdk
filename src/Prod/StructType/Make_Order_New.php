<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_New StructType
 * @subpackage Structs
 */
class Make_Order_New extends AbstractStructBase
{
    /**
     * The registrant
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $registrant;
    /**
     * The admin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $admin;
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $tech;
    /**
     * The billing
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $billing;
    /**
     * The ns
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Ns
     */
    public $ns;
    /**
     * Constructor method for Make_Order_New
     * @uses Make_Order_New::setRegistrant()
     * @uses Make_Order_New::setAdmin()
     * @uses Make_Order_New::setTech()
     * @uses Make_Order_New::setBilling()
     * @uses Make_Order_New::setNs()
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $registrant
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $admin
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $tech
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $billing
     * @param \SubregSDK\Prod\StructType\Make_Order_Ns $ns
     */
    public function __construct(\SubregSDK\Prod\StructType\Make_Order_Contact $registrant = null, \SubregSDK\Prod\StructType\Make_Order_Contact $admin = null, \SubregSDK\Prod\StructType\Make_Order_Contact $tech = null, \SubregSDK\Prod\StructType\Make_Order_Contact $billing = null, \SubregSDK\Prod\StructType\Make_Order_Ns $ns = null)
    {
        $this
            ->setRegistrant($registrant)
            ->setAdmin($admin)
            ->setTech($tech)
            ->setBilling($billing)
            ->setNs($ns);
    }
    /**
     * Get registrant value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getRegistrant()
    {
        return $this->registrant;
    }
    /**
     * Set registrant value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $registrant
     * @return \SubregSDK\Prod\StructType\Make_Order_New
     */
    public function setRegistrant(\SubregSDK\Prod\StructType\Make_Order_Contact $registrant = null)
    {
        $this->registrant = $registrant;
        return $this;
    }
    /**
     * Get admin value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }
    /**
     * Set admin value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $admin
     * @return \SubregSDK\Prod\StructType\Make_Order_New
     */
    public function setAdmin(\SubregSDK\Prod\StructType\Make_Order_Contact $admin = null)
    {
        $this->admin = $admin;
        return $this;
    }
    /**
     * Get tech value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * Set tech value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $tech
     * @return \SubregSDK\Prod\StructType\Make_Order_New
     */
    public function setTech(\SubregSDK\Prod\StructType\Make_Order_Contact $tech = null)
    {
        $this->tech = $tech;
        return $this;
    }
    /**
     * Get billing value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getBilling()
    {
        return $this->billing;
    }
    /**
     * Set billing value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $billing
     * @return \SubregSDK\Prod\StructType\Make_Order_New
     */
    public function setBilling(\SubregSDK\Prod\StructType\Make_Order_Contact $billing = null)
    {
        $this->billing = $billing;
        return $this;
    }
    /**
     * Get ns value
     * @return \SubregSDK\Prod\StructType\Make_Order_Ns|null
     */
    public function getNs()
    {
        return $this->ns;
    }
    /**
     * Set ns value
     * @param \SubregSDK\Prod\StructType\Make_Order_Ns $ns
     * @return \SubregSDK\Prod\StructType\Make_Order_New
     */
    public function setNs(\SubregSDK\Prod\StructType\Make_Order_Ns $ns = null)
    {
        $this->ns = $ns;
        return $this;
    }
}
