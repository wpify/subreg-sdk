<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Contacts StructType
 * @subpackage Structs
 */
class Make_Order_Contacts extends AbstractStructBase
{
    /**
     * The admin
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $admin;
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $tech;
    /**
     * The billing
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $billing;
    /**
     * Constructor method for Make_Order_Contacts
     * @uses Make_Order_Contacts::setAdmin()
     * @uses Make_Order_Contacts::setTech()
     * @uses Make_Order_Contacts::setBilling()
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $admin
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $tech
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $billing
     */
    public function __construct(\SubregSDK\Prod\StructType\Make_Order_Contact $admin = null, \SubregSDK\Prod\StructType\Make_Order_Contact $tech = null, \SubregSDK\Prod\StructType\Make_Order_Contact $billing = null)
    {
        $this
            ->setAdmin($admin)
            ->setTech($tech)
            ->setBilling($billing);
    }
    /**
     * Get admin value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }
    /**
     * Set admin value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $admin
     * @return \SubregSDK\Prod\StructType\Make_Order_Contacts
     */
    public function setAdmin(\SubregSDK\Prod\StructType\Make_Order_Contact $admin = null)
    {
        $this->admin = $admin;
        return $this;
    }
    /**
     * Get tech value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * Set tech value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $tech
     * @return \SubregSDK\Prod\StructType\Make_Order_Contacts
     */
    public function setTech(\SubregSDK\Prod\StructType\Make_Order_Contact $tech = null)
    {
        $this->tech = $tech;
        return $this;
    }
    /**
     * Get billing value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getBilling()
    {
        return $this->billing;
    }
    /**
     * Set billing value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $billing
     * @return \SubregSDK\Prod\StructType\Make_Order_Contacts
     */
    public function setBilling(\SubregSDK\Prod\StructType\Make_Order_Contact $billing = null)
    {
        $this->billing = $billing;
        return $this;
    }
}
