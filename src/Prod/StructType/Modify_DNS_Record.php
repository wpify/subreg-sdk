<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Modify_DNS_Record StructType
 * @subpackage Structs
 */
class Modify_DNS_Record extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The record
     * @var \SubregSDK\Prod\StructType\Modify_DNS_Record_Record
     */
    public $record;
    /**
     * Constructor method for Modify_DNS_Record
     * @uses Modify_DNS_Record::setSsid()
     * @uses Modify_DNS_Record::setDomain()
     * @uses Modify_DNS_Record::setRecord()
     * @param string $ssid
     * @param string $domain
     * @param \SubregSDK\Prod\StructType\Modify_DNS_Record_Record $record
     */
    public function __construct($ssid = null, $domain = null, \SubregSDK\Prod\StructType\Modify_DNS_Record_Record $record = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setRecord($record);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get record value
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record_Record|null
     */
    public function getRecord()
    {
        return $this->record;
    }
    /**
     * Set record value
     * @param \SubregSDK\Prod\StructType\Modify_DNS_Record_Record $record
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record
     */
    public function setRecord(\SubregSDK\Prod\StructType\Modify_DNS_Record_Record $record = null)
    {
        $this->record = $record;
        return $this;
    }
}
