<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Accountings_Container StructType
 * @subpackage Structs
 */
class Get_Accountings_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Get_Accountings_Response
     */
    public $response;
    /**
     * Constructor method for Get_Accountings_Container
     * @uses Get_Accountings_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Get_Accountings_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Get_Accountings_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Get_Accountings_Response $response
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Get_Accountings_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
