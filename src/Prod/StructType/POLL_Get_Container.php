<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for POLL_Get_Container StructType
 * @subpackage Structs
 */
class POLL_Get_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\POLL_Get_Response
     */
    public $response;
    /**
     * Constructor method for POLL_Get_Container
     * @uses POLL_Get_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\POLL_Get_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\POLL_Get_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\POLL_Get_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\POLL_Get_Response $response
     * @return \SubregSDK\Prod\StructType\POLL_Get_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\POLL_Get_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
