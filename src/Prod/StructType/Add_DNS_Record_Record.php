<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Add_DNS_Record_Record StructType
 * @subpackage Structs
 */
class Add_DNS_Record_Record extends AbstractStructBase
{
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The content
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $content;
    /**
     * The prio
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $prio;
    /**
     * The ttl
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $ttl;
    /**
     * Constructor method for Add_DNS_Record_Record
     * @uses Add_DNS_Record_Record::setName()
     * @uses Add_DNS_Record_Record::setType()
     * @uses Add_DNS_Record_Record::setContent()
     * @uses Add_DNS_Record_Record::setPrio()
     * @uses Add_DNS_Record_Record::setTtl()
     * @param string $name
     * @param string $type
     * @param string $content
     * @param int $prio
     * @param int $ttl
     */
    public function __construct($name = null, $type = null, $content = null, $prio = null, $ttl = null)
    {
        $this
            ->setName($name)
            ->setType($type)
            ->setContent($content)
            ->setPrio($prio)
            ->setTtl($ttl);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Record
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Record
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get content value
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * Set content value
     * @param string $content
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Record
     */
    public function setContent($content = null)
    {
        // validation for constraint: string
        if (!is_null($content) && !is_string($content)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($content, true), gettype($content)), __LINE__);
        }
        $this->content = $content;
        return $this;
    }
    /**
     * Get prio value
     * @return int|null
     */
    public function getPrio()
    {
        return $this->prio;
    }
    /**
     * Set prio value
     * @param int $prio
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Record
     */
    public function setPrio($prio = null)
    {
        // validation for constraint: int
        if (!is_null($prio) && !(is_int($prio) || ctype_digit($prio))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($prio, true), gettype($prio)), __LINE__);
        }
        $this->prio = $prio;
        return $this;
    }
    /**
     * Get ttl value
     * @return int|null
     */
    public function getTtl()
    {
        return $this->ttl;
    }
    /**
     * Set ttl value
     * @param int $ttl
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Record
     */
    public function setTtl($ttl = null)
    {
        // validation for constraint: int
        if (!is_null($ttl) && !(is_int($ttl) || ctype_digit($ttl))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($ttl, true), gettype($ttl)), __LINE__);
        }
        $this->ttl = $ttl;
        return $this;
    }
}
