<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Redirects_Container StructType
 * @subpackage Structs
 */
class Get_Redirects_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Get_Redirects_Response
     */
    public $response;
    /**
     * Constructor method for Get_Redirects_Container
     * @uses Get_Redirects_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Get_Redirects_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Get_Redirects_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Get_Redirects_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Get_Redirects_Response $response
     * @return \SubregSDK\Prod\StructType\Get_Redirects_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Get_Redirects_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
