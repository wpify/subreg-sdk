<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_DNS_Zone StructType
 * @subpackage Structs
 */
class Set_DNS_Zone extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The records
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Set_DNS_Zone_Record[]
     */
    public $records;
    /**
     * Constructor method for Set_DNS_Zone
     * @uses Set_DNS_Zone::setSsid()
     * @uses Set_DNS_Zone::setDomain()
     * @uses Set_DNS_Zone::setRecords()
     * @param string $ssid
     * @param string $domain
     * @param \SubregSDK\Prod\StructType\Set_DNS_Zone_Record[] $records
     */
    public function __construct($ssid = null, $domain = null, array $records = array())
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setRecords($records);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Set_DNS_Zone
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Set_DNS_Zone
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get records value
     * @return \SubregSDK\Prod\StructType\Set_DNS_Zone_Record[]|null
     */
    public function getRecords()
    {
        return $this->records;
    }
    /**
     * This method is responsible for validating the values passed to the setRecords method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRecords method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRecordsForArrayConstraintsFromSetRecords(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $set_DNS_ZoneRecordsItem) {
            // validation for constraint: itemType
            if (!$set_DNS_ZoneRecordsItem instanceof \SubregSDK\Prod\StructType\Set_DNS_Zone_Record) {
                $invalidValues[] = is_object($set_DNS_ZoneRecordsItem) ? get_class($set_DNS_ZoneRecordsItem) : sprintf('%s(%s)', gettype($set_DNS_ZoneRecordsItem), var_export($set_DNS_ZoneRecordsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The records property can only contain items of type \SubregSDK\Prod\StructType\Set_DNS_Zone_Record, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Set_DNS_Zone_Record[] $records
     * @return \SubregSDK\Prod\StructType\Set_DNS_Zone
     */
    public function setRecords(array $records = array())
    {
        // validation for constraint: array
        if ('' !== ($recordsArrayErrorMessage = self::validateRecordsForArrayConstraintsFromSetRecords($records))) {
            throw new \InvalidArgumentException($recordsArrayErrorMessage, __LINE__);
        }
        $this->records = $records;
        return $this;
    }
    /**
     * Add item to records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Set_DNS_Zone_Record $item
     * @return \SubregSDK\Prod\StructType\Set_DNS_Zone
     */
    public function addToRecords(\SubregSDK\Prod\StructType\Set_DNS_Zone_Record $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Set_DNS_Zone_Record) {
            throw new \InvalidArgumentException(sprintf('The records property can only contain items of type \SubregSDK\Prod\StructType\Set_DNS_Zone_Record, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->records[] = $item;
        return $this;
    }
}
