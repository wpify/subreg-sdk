<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Redirects_Data StructType
 * @subpackage Structs
 */
class Get_Redirects_Data extends AbstractStructBase
{
    /**
     * The web
     * @var string
     */
    public $web;
    /**
     * The email
     * @var string
     */
    public $email;
    /**
     * Constructor method for Get_Redirects_Data
     * @uses Get_Redirects_Data::setWeb()
     * @uses Get_Redirects_Data::setEmail()
     * @param string $web
     * @param string $email
     */
    public function __construct($web = null, $email = null)
    {
        $this
            ->setWeb($web)
            ->setEmail($email);
    }
    /**
     * Get web value
     * @return string|null
     */
    public function getWeb()
    {
        return $this->web;
    }
    /**
     * Set web value
     * @param string $web
     * @return \SubregSDK\Prod\StructType\Get_Redirects_Data
     */
    public function setWeb($web = null)
    {
        // validation for constraint: string
        if (!is_null($web) && !is_string($web)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($web, true), gettype($web)), __LINE__);
        }
        $this->web = $web;
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \SubregSDK\Prod\StructType\Get_Redirects_Data
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
}
