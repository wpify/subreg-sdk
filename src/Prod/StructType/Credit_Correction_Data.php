<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Credit_Correction_Data StructType
 * @subpackage Structs
 */
class Credit_Correction_Data extends AbstractStructBase
{
}
