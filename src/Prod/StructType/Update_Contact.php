<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Update_Contact StructType
 * @subpackage Structs
 */
class Update_Contact extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The contact
     * @var \SubregSDK\Prod\StructType\Update_Contact_Contact
     */
    public $contact;
    /**
     * Constructor method for Update_Contact
     * @uses Update_Contact::setSsid()
     * @uses Update_Contact::setContact()
     * @param string $ssid
     * @param \SubregSDK\Prod\StructType\Update_Contact_Contact $contact
     */
    public function __construct($ssid = null, \SubregSDK\Prod\StructType\Update_Contact_Contact $contact = null)
    {
        $this
            ->setSsid($ssid)
            ->setContact($contact);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Update_Contact
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get contact value
     * @return \SubregSDK\Prod\StructType\Update_Contact_Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }
    /**
     * Set contact value
     * @param \SubregSDK\Prod\StructType\Update_Contact_Contact $contact
     * @return \SubregSDK\Prod\StructType\Update_Contact
     */
    public function setContact(\SubregSDK\Prod\StructType\Update_Contact_Contact $contact = null)
    {
        $this->contact = $contact;
        return $this;
    }
}
