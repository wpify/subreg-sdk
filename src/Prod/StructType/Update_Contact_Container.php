<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Update_Contact_Container StructType
 * @subpackage Structs
 */
class Update_Contact_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Update_Contact_Response
     */
    public $response;
    /**
     * Constructor method for Update_Contact_Container
     * @uses Update_Contact_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Update_Contact_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Update_Contact_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Update_Contact_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Update_Contact_Response $response
     * @return \SubregSDK\Prod\StructType\Update_Contact_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Update_Contact_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
