<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Add_DNS_Record_Data StructType
 * @subpackage Structs
 */
class Add_DNS_Record_Data extends AbstractStructBase
{
}
