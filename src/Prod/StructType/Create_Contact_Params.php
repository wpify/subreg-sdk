<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Create_Contact_Params StructType
 * @subpackage Structs
 */
class Create_Contact_Params extends AbstractStructBase
{
    /**
     * The regid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $regid;
    /**
     * The notify_email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $notify_email;
    /**
     * The vat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $vat;
    /**
     * The ident_type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ident_type;
    /**
     * The ident_number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ident_number;
    /**
     * The disclose
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $disclose;
    /**
     * Constructor method for Create_Contact_Params
     * @uses Create_Contact_Params::setRegid()
     * @uses Create_Contact_Params::setNotify_email()
     * @uses Create_Contact_Params::setVat()
     * @uses Create_Contact_Params::setIdent_type()
     * @uses Create_Contact_Params::setIdent_number()
     * @uses Create_Contact_Params::setDisclose()
     * @param string $regid
     * @param string $notify_email
     * @param string $vat
     * @param string $ident_type
     * @param string $ident_number
     * @param string[] $disclose
     */
    public function __construct($regid = null, $notify_email = null, $vat = null, $ident_type = null, $ident_number = null, array $disclose = array())
    {
        $this
            ->setRegid($regid)
            ->setNotify_email($notify_email)
            ->setVat($vat)
            ->setIdent_type($ident_type)
            ->setIdent_number($ident_number)
            ->setDisclose($disclose);
    }
    /**
     * Get regid value
     * @return string|null
     */
    public function getRegid()
    {
        return $this->regid;
    }
    /**
     * Set regid value
     * @param string $regid
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setRegid($regid = null)
    {
        // validation for constraint: string
        if (!is_null($regid) && !is_string($regid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($regid, true), gettype($regid)), __LINE__);
        }
        $this->regid = $regid;
        return $this;
    }
    /**
     * Get notify_email value
     * @return string|null
     */
    public function getNotify_email()
    {
        return $this->notify_email;
    }
    /**
     * Set notify_email value
     * @param string $notify_email
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setNotify_email($notify_email = null)
    {
        // validation for constraint: string
        if (!is_null($notify_email) && !is_string($notify_email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($notify_email, true), gettype($notify_email)), __LINE__);
        }
        $this->notify_email = $notify_email;
        return $this;
    }
    /**
     * Get vat value
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }
    /**
     * Set vat value
     * @param string $vat
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setVat($vat = null)
    {
        // validation for constraint: string
        if (!is_null($vat) && !is_string($vat)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vat, true), gettype($vat)), __LINE__);
        }
        $this->vat = $vat;
        return $this;
    }
    /**
     * Get ident_type value
     * @return string|null
     */
    public function getIdent_type()
    {
        return $this->ident_type;
    }
    /**
     * Set ident_type value
     * @param string $ident_type
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setIdent_type($ident_type = null)
    {
        // validation for constraint: string
        if (!is_null($ident_type) && !is_string($ident_type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ident_type, true), gettype($ident_type)), __LINE__);
        }
        $this->ident_type = $ident_type;
        return $this;
    }
    /**
     * Get ident_number value
     * @return string|null
     */
    public function getIdent_number()
    {
        return $this->ident_number;
    }
    /**
     * Set ident_number value
     * @param string $ident_number
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setIdent_number($ident_number = null)
    {
        // validation for constraint: string
        if (!is_null($ident_number) && !is_string($ident_number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ident_number, true), gettype($ident_number)), __LINE__);
        }
        $this->ident_number = $ident_number;
        return $this;
    }
    /**
     * Get disclose value
     * @return string[]|null
     */
    public function getDisclose()
    {
        return $this->disclose;
    }
    /**
     * This method is responsible for validating the values passed to the setDisclose method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDisclose method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDiscloseForArrayConstraintsFromSetDisclose(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $create_Contact_ParamsDiscloseItem) {
            // validation for constraint: itemType
            if (!is_string($create_Contact_ParamsDiscloseItem)) {
                $invalidValues[] = is_object($create_Contact_ParamsDiscloseItem) ? get_class($create_Contact_ParamsDiscloseItem) : sprintf('%s(%s)', gettype($create_Contact_ParamsDiscloseItem), var_export($create_Contact_ParamsDiscloseItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The disclose property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set disclose value
     * @throws \InvalidArgumentException
     * @param string[] $disclose
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function setDisclose(array $disclose = array())
    {
        // validation for constraint: array
        if ('' !== ($discloseArrayErrorMessage = self::validateDiscloseForArrayConstraintsFromSetDisclose($disclose))) {
            throw new \InvalidArgumentException($discloseArrayErrorMessage, __LINE__);
        }
        $this->disclose = $disclose;
        return $this;
    }
    /**
     * Add item to disclose value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Prod\StructType\Create_Contact_Params
     */
    public function addToDisclose($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The disclose property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->disclose[] = $item;
        return $this;
    }
}
