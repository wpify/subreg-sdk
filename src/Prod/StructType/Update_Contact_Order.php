<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Update_Contact_Order StructType
 * @subpackage Structs
 */
class Update_Contact_Order extends AbstractStructBase
{
    /**
     * The register
     * @var string
     */
    public $register;
    /**
     * The orderid
     * @var int
     */
    public $orderid;
    /**
     * Constructor method for Update_Contact_Order
     * @uses Update_Contact_Order::setRegister()
     * @uses Update_Contact_Order::setOrderid()
     * @param string $register
     * @param int $orderid
     */
    public function __construct($register = null, $orderid = null)
    {
        $this
            ->setRegister($register)
            ->setOrderid($orderid);
    }
    /**
     * Get register value
     * @return string|null
     */
    public function getRegister()
    {
        return $this->register;
    }
    /**
     * Set register value
     * @param string $register
     * @return \SubregSDK\Prod\StructType\Update_Contact_Order
     */
    public function setRegister($register = null)
    {
        // validation for constraint: string
        if (!is_null($register) && !is_string($register)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($register, true), gettype($register)), __LINE__);
        }
        $this->register = $register;
        return $this;
    }
    /**
     * Get orderid value
     * @return int|null
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
    /**
     * Set orderid value
     * @param int $orderid
     * @return \SubregSDK\Prod\StructType\Update_Contact_Order
     */
    public function setOrderid($orderid = null)
    {
        // validation for constraint: int
        if (!is_null($orderid) && !(is_int($orderid) || ctype_digit($orderid))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderid, true), gettype($orderid)), __LINE__);
        }
        $this->orderid = $orderid;
        return $this;
    }
}
