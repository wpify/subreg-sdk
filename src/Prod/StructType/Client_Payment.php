<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Client_Payment StructType
 * @subpackage Structs
 */
class Client_Payment extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The username
     * @var string
     */
    public $username;
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * Constructor method for Client_Payment
     * @uses Client_Payment::setSsid()
     * @uses Client_Payment::setUsername()
     * @uses Client_Payment::setAmount()
     * @uses Client_Payment::setCurrency()
     * @param string $ssid
     * @param string $username
     * @param float $amount
     * @param string $currency
     */
    public function __construct($ssid = null, $username = null, $amount = null, $currency = null)
    {
        $this
            ->setSsid($ssid)
            ->setUsername($username)
            ->setAmount($amount)
            ->setCurrency($currency);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Client_Payment
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set username value
     * @param string $username
     * @return \SubregSDK\Prod\StructType\Client_Payment
     */
    public function setUsername($username = null)
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        $this->username = $username;
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Prod\StructType\Client_Payment
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Prod\StructType\Client_Payment
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
}
