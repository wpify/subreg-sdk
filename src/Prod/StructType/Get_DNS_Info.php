<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_DNS_Info StructType
 * @subpackage Structs
 */
class Get_DNS_Info extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The dnstype
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $dnstype;
    /**
     * Constructor method for Get_DNS_Info
     * @uses Get_DNS_Info::setSsid()
     * @uses Get_DNS_Info::setDomain()
     * @uses Get_DNS_Info::setDnstype()
     * @param string $ssid
     * @param string $domain
     * @param string $dnstype
     */
    public function __construct($ssid = null, $domain = null, $dnstype = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setDnstype($dnstype);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get dnstype value
     * @return string|null
     */
    public function getDnstype()
    {
        return $this->dnstype;
    }
    /**
     * Set dnstype value
     * @param string $dnstype
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info
     */
    public function setDnstype($dnstype = null)
    {
        // validation for constraint: string
        if (!is_null($dnstype) && !is_string($dnstype)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dnstype, true), gettype($dnstype)), __LINE__);
        }
        $this->dnstype = $dnstype;
        return $this;
    }
}
