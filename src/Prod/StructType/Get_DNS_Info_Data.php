<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_DNS_Info_Data StructType
 * @subpackage Structs
 */
class Get_DNS_Info_Data extends AbstractStructBase
{
    /**
     * The in_zone
     * @var string
     */
    public $in_zone;
    /**
     * The dnssec
     * @var string
     */
    public $dnssec;
    /**
     * The dns
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Get_DNS_Info_Dn[]
     */
    public $dns;
    /**
     * Constructor method for Get_DNS_Info_Data
     * @uses Get_DNS_Info_Data::setIn_zone()
     * @uses Get_DNS_Info_Data::setDnssec()
     * @uses Get_DNS_Info_Data::setDns()
     * @param string $in_zone
     * @param string $dnssec
     * @param \SubregSDK\Prod\StructType\Get_DNS_Info_Dn[] $dns
     */
    public function __construct($in_zone = null, $dnssec = null, array $dns = array())
    {
        $this
            ->setIn_zone($in_zone)
            ->setDnssec($dnssec)
            ->setDns($dns);
    }
    /**
     * Get in_zone value
     * @return string|null
     */
    public function getIn_zone()
    {
        return $this->in_zone;
    }
    /**
     * Set in_zone value
     * @param string $in_zone
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Data
     */
    public function setIn_zone($in_zone = null)
    {
        // validation for constraint: string
        if (!is_null($in_zone) && !is_string($in_zone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($in_zone, true), gettype($in_zone)), __LINE__);
        }
        $this->in_zone = $in_zone;
        return $this;
    }
    /**
     * Get dnssec value
     * @return string|null
     */
    public function getDnssec()
    {
        return $this->dnssec;
    }
    /**
     * Set dnssec value
     * @param string $dnssec
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Data
     */
    public function setDnssec($dnssec = null)
    {
        // validation for constraint: string
        if (!is_null($dnssec) && !is_string($dnssec)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dnssec, true), gettype($dnssec)), __LINE__);
        }
        $this->dnssec = $dnssec;
        return $this;
    }
    /**
     * Get dns value
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Dn[]|null
     */
    public function getDns()
    {
        return $this->dns;
    }
    /**
     * This method is responsible for validating the values passed to the setDns method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDns method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDnsForArrayConstraintsFromSetDns(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_DNS_Info_DataDnsItem) {
            // validation for constraint: itemType
            if (!$get_DNS_Info_DataDnsItem instanceof \SubregSDK\Prod\StructType\Get_DNS_Info_Dn) {
                $invalidValues[] = is_object($get_DNS_Info_DataDnsItem) ? get_class($get_DNS_Info_DataDnsItem) : sprintf('%s(%s)', gettype($get_DNS_Info_DataDnsItem), var_export($get_DNS_Info_DataDnsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dns property can only contain items of type \SubregSDK\Prod\StructType\Get_DNS_Info_Dn, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set dns value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_DNS_Info_Dn[] $dns
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Data
     */
    public function setDns(array $dns = array())
    {
        // validation for constraint: array
        if ('' !== ($dnsArrayErrorMessage = self::validateDnsForArrayConstraintsFromSetDns($dns))) {
            throw new \InvalidArgumentException($dnsArrayErrorMessage, __LINE__);
        }
        $this->dns = $dns;
        return $this;
    }
    /**
     * Add item to dns value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_DNS_Info_Dn $item
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Data
     */
    public function addToDns(\SubregSDK\Prod\StructType\Get_DNS_Info_Dn $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Get_DNS_Info_Dn) {
            throw new \InvalidArgumentException(sprintf('The dns property can only contain items of type \SubregSDK\Prod\StructType\Get_DNS_Info_Dn, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dns[] = $item;
        return $this;
    }
}
