<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Data StructType
 * @subpackage Structs
 */
class Make_Order_Data extends AbstractStructBase
{
    /**
     * The orderid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $orderid;
    /**
     * Constructor method for Make_Order_Data
     * @uses Make_Order_Data::setOrderid()
     * @param string $orderid
     */
    public function __construct($orderid = null)
    {
        $this
            ->setOrderid($orderid);
    }
    /**
     * Get orderid value
     * @return string|null
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
    /**
     * Set orderid value
     * @param string $orderid
     * @return \SubregSDK\Prod\StructType\Make_Order_Data
     */
    public function setOrderid($orderid = null)
    {
        // validation for constraint: string
        if (!is_null($orderid) && !is_string($orderid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderid, true), gettype($orderid)), __LINE__);
        }
        $this->orderid = $orderid;
        return $this;
    }
}
