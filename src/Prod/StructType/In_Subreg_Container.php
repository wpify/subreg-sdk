<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for In_Subreg_Container StructType
 * @subpackage Structs
 */
class In_Subreg_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\In_Subreg_Response
     */
    public $response;
    /**
     * Constructor method for In_Subreg_Container
     * @uses In_Subreg_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\In_Subreg_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\In_Subreg_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\In_Subreg_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\In_Subreg_Response $response
     * @return \SubregSDK\Prod\StructType\In_Subreg_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\In_Subreg_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
