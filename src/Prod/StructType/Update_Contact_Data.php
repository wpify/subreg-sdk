<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Update_Contact_Data StructType
 * @subpackage Structs
 */
class Update_Contact_Data extends AbstractStructBase
{
    /**
     * The orders
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Update_Contact_Order[]
     */
    public $orders;
    /**
     * Constructor method for Update_Contact_Data
     * @uses Update_Contact_Data::setOrders()
     * @param \SubregSDK\Prod\StructType\Update_Contact_Order[] $orders
     */
    public function __construct(array $orders = array())
    {
        $this
            ->setOrders($orders);
    }
    /**
     * Get orders value
     * @return \SubregSDK\Prod\StructType\Update_Contact_Order[]|null
     */
    public function getOrders()
    {
        return $this->orders;
    }
    /**
     * This method is responsible for validating the values passed to the setOrders method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOrders method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOrdersForArrayConstraintsFromSetOrders(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $update_Contact_DataOrdersItem) {
            // validation for constraint: itemType
            if (!$update_Contact_DataOrdersItem instanceof \SubregSDK\Prod\StructType\Update_Contact_Order) {
                $invalidValues[] = is_object($update_Contact_DataOrdersItem) ? get_class($update_Contact_DataOrdersItem) : sprintf('%s(%s)', gettype($update_Contact_DataOrdersItem), var_export($update_Contact_DataOrdersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The orders property can only contain items of type \SubregSDK\Prod\StructType\Update_Contact_Order, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set orders value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Update_Contact_Order[] $orders
     * @return \SubregSDK\Prod\StructType\Update_Contact_Data
     */
    public function setOrders(array $orders = array())
    {
        // validation for constraint: array
        if ('' !== ($ordersArrayErrorMessage = self::validateOrdersForArrayConstraintsFromSetOrders($orders))) {
            throw new \InvalidArgumentException($ordersArrayErrorMessage, __LINE__);
        }
        $this->orders = $orders;
        return $this;
    }
    /**
     * Add item to orders value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Update_Contact_Order $item
     * @return \SubregSDK\Prod\StructType\Update_Contact_Data
     */
    public function addToOrders(\SubregSDK\Prod\StructType\Update_Contact_Order $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Update_Contact_Order) {
            throw new \InvalidArgumentException(sprintf('The orders property can only contain items of type \SubregSDK\Prod\StructType\Update_Contact_Order, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->orders[] = $item;
        return $this;
    }
}
