<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Prices_Data StructType
 * @subpackage Structs
 */
class Set_Prices_Data extends AbstractStructBase
{
}
