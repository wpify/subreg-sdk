<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Accountings_Data StructType
 * @subpackage Structs
 */
class Get_Accountings_Data extends AbstractStructBase
{
    /**
     * The count
     * @var int
     */
    public $count;
    /**
     * The from
     * @var string
     */
    public $from;
    /**
     * The to
     * @var string
     */
    public $to;
    /**
     * The accounting
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Get_Accountings_Accounting[]
     */
    public $accounting;
    /**
     * Constructor method for Get_Accountings_Data
     * @uses Get_Accountings_Data::setCount()
     * @uses Get_Accountings_Data::setFrom()
     * @uses Get_Accountings_Data::setTo()
     * @uses Get_Accountings_Data::setAccounting()
     * @param int $count
     * @param string $from
     * @param string $to
     * @param \SubregSDK\Prod\StructType\Get_Accountings_Accounting[] $accounting
     */
    public function __construct($count = null, $from = null, $to = null, array $accounting = array())
    {
        $this
            ->setCount($count)
            ->setFrom($from)
            ->setTo($to)
            ->setAccounting($accounting);
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Data
     */
    public function setCount($count = null)
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        return $this;
    }
    /**
     * Get from value
     * @return string|null
     */
    public function getFrom()
    {
        return $this->from;
    }
    /**
     * Set from value
     * @param string $from
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Data
     */
    public function setFrom($from = null)
    {
        // validation for constraint: string
        if (!is_null($from) && !is_string($from)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($from, true), gettype($from)), __LINE__);
        }
        $this->from = $from;
        return $this;
    }
    /**
     * Get to value
     * @return string|null
     */
    public function getTo()
    {
        return $this->to;
    }
    /**
     * Set to value
     * @param string $to
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Data
     */
    public function setTo($to = null)
    {
        // validation for constraint: string
        if (!is_null($to) && !is_string($to)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($to, true), gettype($to)), __LINE__);
        }
        $this->to = $to;
        return $this;
    }
    /**
     * Get accounting value
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting[]|null
     */
    public function getAccounting()
    {
        return $this->accounting;
    }
    /**
     * This method is responsible for validating the values passed to the setAccounting method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAccounting method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAccountingForArrayConstraintsFromSetAccounting(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_Accountings_DataAccountingItem) {
            // validation for constraint: itemType
            if (!$get_Accountings_DataAccountingItem instanceof \SubregSDK\Prod\StructType\Get_Accountings_Accounting) {
                $invalidValues[] = is_object($get_Accountings_DataAccountingItem) ? get_class($get_Accountings_DataAccountingItem) : sprintf('%s(%s)', gettype($get_Accountings_DataAccountingItem), var_export($get_Accountings_DataAccountingItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The accounting property can only contain items of type \SubregSDK\Prod\StructType\Get_Accountings_Accounting, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set accounting value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Accountings_Accounting[] $accounting
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Data
     */
    public function setAccounting(array $accounting = array())
    {
        // validation for constraint: array
        if ('' !== ($accountingArrayErrorMessage = self::validateAccountingForArrayConstraintsFromSetAccounting($accounting))) {
            throw new \InvalidArgumentException($accountingArrayErrorMessage, __LINE__);
        }
        $this->accounting = $accounting;
        return $this;
    }
    /**
     * Add item to accounting value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Accountings_Accounting $item
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Data
     */
    public function addToAccounting(\SubregSDK\Prod\StructType\Get_Accountings_Accounting $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Get_Accountings_Accounting) {
            throw new \InvalidArgumentException(sprintf('The accounting property can only contain items of type \SubregSDK\Prod\StructType\Get_Accountings_Accounting, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->accounting[] = $item;
        return $this;
    }
}
