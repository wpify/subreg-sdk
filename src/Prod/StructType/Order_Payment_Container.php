<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Order_Payment_Container StructType
 * @subpackage Structs
 */
class Order_Payment_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Order_Payment_Response
     */
    public $response;
    /**
     * Constructor method for Order_Payment_Container
     * @uses Order_Payment_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Order_Payment_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Order_Payment_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Order_Payment_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Order_Payment_Response $response
     * @return \SubregSDK\Prod\StructType\Order_Payment_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Order_Payment_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
