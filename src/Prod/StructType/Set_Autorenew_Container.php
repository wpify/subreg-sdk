<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Autorenew_Container StructType
 * @subpackage Structs
 */
class Set_Autorenew_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Set_Autorenew_Response
     */
    public $response;
    /**
     * Constructor method for Set_Autorenew_Container
     * @uses Set_Autorenew_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Set_Autorenew_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Set_Autorenew_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Set_Autorenew_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Set_Autorenew_Response $response
     * @return \SubregSDK\Prod\StructType\Set_Autorenew_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Set_Autorenew_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
