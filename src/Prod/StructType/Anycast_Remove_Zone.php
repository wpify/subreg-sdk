<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_Remove_Zone StructType
 * @subpackage Structs
 */
class Anycast_Remove_Zone extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The server
     * @var int
     */
    public $server;
    /**
     * Constructor method for Anycast_Remove_Zone
     * @uses Anycast_Remove_Zone::setSsid()
     * @uses Anycast_Remove_Zone::setDomain()
     * @uses Anycast_Remove_Zone::setServer()
     * @param string $ssid
     * @param string $domain
     * @param int $server
     */
    public function __construct($ssid = null, $domain = null, $server = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setServer($server);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Anycast_Remove_Zone
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Anycast_Remove_Zone
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get server value
     * @return int|null
     */
    public function getServer()
    {
        return $this->server;
    }
    /**
     * Set server value
     * @param int $server
     * @return \SubregSDK\Prod\StructType\Anycast_Remove_Zone
     */
    public function setServer($server = null)
    {
        // validation for constraint: int
        if (!is_null($server) && !(is_int($server) || ctype_digit($server))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($server, true), gettype($server)), __LINE__);
        }
        $this->server = $server;
        return $this;
    }
}
