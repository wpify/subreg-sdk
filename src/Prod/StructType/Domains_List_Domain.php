<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Domains_List_Domain StructType
 * @subpackage Structs
 */
class Domains_List_Domain extends AbstractStructBase
{
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The expire
     * @var string
     */
    public $expire;
    /**
     * The autorenew
     * @var int
     */
    public $autorenew;
    /**
     * Constructor method for Domains_List_Domain
     * @uses Domains_List_Domain::setName()
     * @uses Domains_List_Domain::setExpire()
     * @uses Domains_List_Domain::setAutorenew()
     * @param string $name
     * @param string $expire
     * @param int $autorenew
     */
    public function __construct($name = null, $expire = null, $autorenew = null)
    {
        $this
            ->setName($name)
            ->setExpire($expire)
            ->setAutorenew($autorenew);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\Domains_List_Domain
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get expire value
     * @return string|null
     */
    public function getExpire()
    {
        return $this->expire;
    }
    /**
     * Set expire value
     * @param string $expire
     * @return \SubregSDK\Prod\StructType\Domains_List_Domain
     */
    public function setExpire($expire = null)
    {
        // validation for constraint: string
        if (!is_null($expire) && !is_string($expire)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expire, true), gettype($expire)), __LINE__);
        }
        $this->expire = $expire;
        return $this;
    }
    /**
     * Get autorenew value
     * @return int|null
     */
    public function getAutorenew()
    {
        return $this->autorenew;
    }
    /**
     * Set autorenew value
     * @param int $autorenew
     * @return \SubregSDK\Prod\StructType\Domains_List_Domain
     */
    public function setAutorenew($autorenew = null)
    {
        // validation for constraint: int
        if (!is_null($autorenew) && !(is_int($autorenew) || ctype_digit($autorenew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($autorenew, true), gettype($autorenew)), __LINE__);
        }
        $this->autorenew = $autorenew;
        return $this;
    }
}
