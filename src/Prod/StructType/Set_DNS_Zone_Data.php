<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_DNS_Zone_Data StructType
 * @subpackage Structs
 */
class Set_DNS_Zone_Data extends AbstractStructBase
{
}
