<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Client_Payment_Data StructType
 * @subpackage Structs
 */
class Client_Payment_Data extends AbstractStructBase
{
}
