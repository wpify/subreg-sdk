<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OIB_Search_Domain StructType
 * @subpackage Structs
 */
class OIB_Search_Domain extends AbstractStructBase
{
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The type
     * @var int
     */
    public $type;
    /**
     * The typedesc
     * @var string
     */
    public $typedesc;
    /**
     * Constructor method for OIB_Search_Domain
     * @uses OIB_Search_Domain::setName()
     * @uses OIB_Search_Domain::setType()
     * @uses OIB_Search_Domain::setTypedesc()
     * @param string $name
     * @param int $type
     * @param string $typedesc
     */
    public function __construct($name = null, $type = null, $typedesc = null)
    {
        $this
            ->setName($name)
            ->setType($type)
            ->setTypedesc($typedesc);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\OIB_Search_Domain
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get type value
     * @return int|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param int $type
     * @return \SubregSDK\Prod\StructType\OIB_Search_Domain
     */
    public function setType($type = null)
    {
        // validation for constraint: int
        if (!is_null($type) && !(is_int($type) || ctype_digit($type))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get typedesc value
     * @return string|null
     */
    public function getTypedesc()
    {
        return $this->typedesc;
    }
    /**
     * Set typedesc value
     * @param string $typedesc
     * @return \SubregSDK\Prod\StructType\OIB_Search_Domain
     */
    public function setTypedesc($typedesc = null)
    {
        // validation for constraint: string
        if (!is_null($typedesc) && !is_string($typedesc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typedesc, true), gettype($typedesc)), __LINE__);
        }
        $this->typedesc = $typedesc;
        return $this;
    }
}
