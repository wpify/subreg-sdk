<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Delete_DNS_Record_Data StructType
 * @subpackage Structs
 */
class Delete_DNS_Record_Data extends AbstractStructBase
{
}
