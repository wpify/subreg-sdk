<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Pricelist_Pricelist StructType
 * @subpackage Structs
 */
class Get_Pricelist_Pricelist extends AbstractStructBase
{
    /**
     * The tld
     * @var string
     */
    public $tld;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The prices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Get_Pricelist_Price[]
     */
    public $prices;
    /**
     * Constructor method for Get_Pricelist_Pricelist
     * @uses Get_Pricelist_Pricelist::setTld()
     * @uses Get_Pricelist_Pricelist::setCurrency()
     * @uses Get_Pricelist_Pricelist::setPrices()
     * @param string $tld
     * @param string $currency
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Price[] $prices
     */
    public function __construct($tld = null, $currency = null, array $prices = array())
    {
        $this
            ->setTld($tld)
            ->setCurrency($currency)
            ->setPrices($prices);
    }
    /**
     * Get tld value
     * @return string|null
     */
    public function getTld()
    {
        return $this->tld;
    }
    /**
     * Set tld value
     * @param string $tld
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist
     */
    public function setTld($tld = null)
    {
        // validation for constraint: string
        if (!is_null($tld) && !is_string($tld)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tld, true), gettype($tld)), __LINE__);
        }
        $this->tld = $tld;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get prices value
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Price[]|null
     */
    public function getPrices()
    {
        return $this->prices;
    }
    /**
     * This method is responsible for validating the values passed to the setPrices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePricesForArrayConstraintsFromSetPrices(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_Pricelist_PricelistPricesItem) {
            // validation for constraint: itemType
            if (!$get_Pricelist_PricelistPricesItem instanceof \SubregSDK\Prod\StructType\Get_Pricelist_Price) {
                $invalidValues[] = is_object($get_Pricelist_PricelistPricesItem) ? get_class($get_Pricelist_PricelistPricesItem) : sprintf('%s(%s)', gettype($get_Pricelist_PricelistPricesItem), var_export($get_Pricelist_PricelistPricesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The prices property can only contain items of type \SubregSDK\Prod\StructType\Get_Pricelist_Price, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Price[] $prices
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist
     */
    public function setPrices(array $prices = array())
    {
        // validation for constraint: array
        if ('' !== ($pricesArrayErrorMessage = self::validatePricesForArrayConstraintsFromSetPrices($prices))) {
            throw new \InvalidArgumentException($pricesArrayErrorMessage, __LINE__);
        }
        $this->prices = $prices;
        return $this;
    }
    /**
     * Add item to prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Price $item
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist
     */
    public function addToPrices(\SubregSDK\Prod\StructType\Get_Pricelist_Price $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Get_Pricelist_Price) {
            throw new \InvalidArgumentException(sprintf('The prices property can only contain items of type \SubregSDK\Prod\StructType\Get_Pricelist_Price, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->prices[] = $item;
        return $this;
    }
}
