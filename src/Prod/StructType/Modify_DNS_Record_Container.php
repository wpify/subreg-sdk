<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Modify_DNS_Record_Container StructType
 * @subpackage Structs
 */
class Modify_DNS_Record_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Modify_DNS_Record_Response
     */
    public $response;
    /**
     * Constructor method for Modify_DNS_Record_Container
     * @uses Modify_DNS_Record_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Modify_DNS_Record_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Modify_DNS_Record_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Modify_DNS_Record_Response $response
     * @return \SubregSDK\Prod\StructType\Modify_DNS_Record_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Modify_DNS_Record_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
