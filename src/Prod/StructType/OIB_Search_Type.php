<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OIB_Search_Type StructType
 * @subpackage Structs
 */
class OIB_Search_Type extends AbstractStructBase
{
    /**
     * The type
     * @var int
     */
    public $type;
    /**
     * The typedesc
     * @var string
     */
    public $typedesc;
    /**
     * The used
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $used;
    /**
     * The maximum
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $maximum;
    /**
     * Constructor method for OIB_Search_Type
     * @uses OIB_Search_Type::setType()
     * @uses OIB_Search_Type::setTypedesc()
     * @uses OIB_Search_Type::setUsed()
     * @uses OIB_Search_Type::setMaximum()
     * @param int $type
     * @param string $typedesc
     * @param int $used
     * @param int $maximum
     */
    public function __construct($type = null, $typedesc = null, $used = null, $maximum = null)
    {
        $this
            ->setType($type)
            ->setTypedesc($typedesc)
            ->setUsed($used)
            ->setMaximum($maximum);
    }
    /**
     * Get type value
     * @return int|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param int $type
     * @return \SubregSDK\Prod\StructType\OIB_Search_Type
     */
    public function setType($type = null)
    {
        // validation for constraint: int
        if (!is_null($type) && !(is_int($type) || ctype_digit($type))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get typedesc value
     * @return string|null
     */
    public function getTypedesc()
    {
        return $this->typedesc;
    }
    /**
     * Set typedesc value
     * @param string $typedesc
     * @return \SubregSDK\Prod\StructType\OIB_Search_Type
     */
    public function setTypedesc($typedesc = null)
    {
        // validation for constraint: string
        if (!is_null($typedesc) && !is_string($typedesc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($typedesc, true), gettype($typedesc)), __LINE__);
        }
        $this->typedesc = $typedesc;
        return $this;
    }
    /**
     * Get used value
     * @return int|null
     */
    public function getUsed()
    {
        return $this->used;
    }
    /**
     * Set used value
     * @param int $used
     * @return \SubregSDK\Prod\StructType\OIB_Search_Type
     */
    public function setUsed($used = null)
    {
        // validation for constraint: int
        if (!is_null($used) && !(is_int($used) || ctype_digit($used))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($used, true), gettype($used)), __LINE__);
        }
        $this->used = $used;
        return $this;
    }
    /**
     * Get maximum value
     * @return int|null
     */
    public function getMaximum()
    {
        return $this->maximum;
    }
    /**
     * Set maximum value
     * @param int $maximum
     * @return \SubregSDK\Prod\StructType\OIB_Search_Type
     */
    public function setMaximum($maximum = null)
    {
        // validation for constraint: int
        if (!is_null($maximum) && !(is_int($maximum) || ctype_digit($maximum))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maximum, true), gettype($maximum)), __LINE__);
        }
        $this->maximum = $maximum;
        return $this;
    }
}
