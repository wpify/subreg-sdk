<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Delete_DNS_Record_Record StructType
 * @subpackage Structs
 */
class Delete_DNS_Record_Record extends AbstractStructBase
{
    /**
     * The id
     * @var int
     */
    public $id;
    /**
     * Constructor method for Delete_DNS_Record_Record
     * @uses Delete_DNS_Record_Record::setId()
     * @param int $id
     */
    public function __construct($id = null)
    {
        $this
            ->setId($id);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \SubregSDK\Prod\StructType\Delete_DNS_Record_Record
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
}
