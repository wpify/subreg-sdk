<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Prices StructType
 * @subpackage Structs
 */
class Set_Prices extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The pricelist
     * @var string
     */
    public $pricelist;
    /**
     * The tld
     * @var string
     */
    public $tld;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The prices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Set_Prices_Price[]
     */
    public $prices;
    /**
     * Constructor method for Set_Prices
     * @uses Set_Prices::setSsid()
     * @uses Set_Prices::setPricelist()
     * @uses Set_Prices::setTld()
     * @uses Set_Prices::setCurrency()
     * @uses Set_Prices::setPrices()
     * @param string $ssid
     * @param string $pricelist
     * @param string $tld
     * @param string $currency
     * @param \SubregSDK\Prod\StructType\Set_Prices_Price[] $prices
     */
    public function __construct($ssid = null, $pricelist = null, $tld = null, $currency = null, array $prices = array())
    {
        $this
            ->setSsid($ssid)
            ->setPricelist($pricelist)
            ->setTld($tld)
            ->setCurrency($currency)
            ->setPrices($prices);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get pricelist value
     * @return string|null
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }
    /**
     * Set pricelist value
     * @param string $pricelist
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function setPricelist($pricelist = null)
    {
        // validation for constraint: string
        if (!is_null($pricelist) && !is_string($pricelist)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pricelist, true), gettype($pricelist)), __LINE__);
        }
        $this->pricelist = $pricelist;
        return $this;
    }
    /**
     * Get tld value
     * @return string|null
     */
    public function getTld()
    {
        return $this->tld;
    }
    /**
     * Set tld value
     * @param string $tld
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function setTld($tld = null)
    {
        // validation for constraint: string
        if (!is_null($tld) && !is_string($tld)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tld, true), gettype($tld)), __LINE__);
        }
        $this->tld = $tld;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get prices value
     * @return \SubregSDK\Prod\StructType\Set_Prices_Price[]|null
     */
    public function getPrices()
    {
        return $this->prices;
    }
    /**
     * This method is responsible for validating the values passed to the setPrices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePricesForArrayConstraintsFromSetPrices(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $set_PricesPricesItem) {
            // validation for constraint: itemType
            if (!$set_PricesPricesItem instanceof \SubregSDK\Prod\StructType\Set_Prices_Price) {
                $invalidValues[] = is_object($set_PricesPricesItem) ? get_class($set_PricesPricesItem) : sprintf('%s(%s)', gettype($set_PricesPricesItem), var_export($set_PricesPricesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The prices property can only contain items of type \SubregSDK\Prod\StructType\Set_Prices_Price, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Set_Prices_Price[] $prices
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function setPrices(array $prices = array())
    {
        // validation for constraint: array
        if ('' !== ($pricesArrayErrorMessage = self::validatePricesForArrayConstraintsFromSetPrices($prices))) {
            throw new \InvalidArgumentException($pricesArrayErrorMessage, __LINE__);
        }
        $this->prices = $prices;
        return $this;
    }
    /**
     * Add item to prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Set_Prices_Price $item
     * @return \SubregSDK\Prod\StructType\Set_Prices
     */
    public function addToPrices(\SubregSDK\Prod\StructType\Set_Prices_Price $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Set_Prices_Price) {
            throw new \InvalidArgumentException(sprintf('The prices property can only contain items of type \SubregSDK\Prod\StructType\Set_Prices_Price, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->prices[] = $item;
        return $this;
    }
}
