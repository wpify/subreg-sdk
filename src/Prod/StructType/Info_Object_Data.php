<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Data StructType
 * @subpackage Structs
 */
class Info_Object_Data extends AbstractStructBase
{
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The contact
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Object_Contact
     */
    public $contact;
    /**
     * The nsset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Object_Nsset
     */
    public $nsset;
    /**
     * The keyset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Object_Keyset
     */
    public $keyset;
    /**
     * Constructor method for Info_Object_Data
     * @uses Info_Object_Data::setId()
     * @uses Info_Object_Data::setType()
     * @uses Info_Object_Data::setContact()
     * @uses Info_Object_Data::setNsset()
     * @uses Info_Object_Data::setKeyset()
     * @param string $id
     * @param string $type
     * @param \SubregSDK\Prod\StructType\Info_Object_Contact $contact
     * @param \SubregSDK\Prod\StructType\Info_Object_Nsset $nsset
     * @param \SubregSDK\Prod\StructType\Info_Object_Keyset $keyset
     */
    public function __construct($id = null, $type = null, \SubregSDK\Prod\StructType\Info_Object_Contact $contact = null, \SubregSDK\Prod\StructType\Info_Object_Nsset $nsset = null, \SubregSDK\Prod\StructType\Info_Object_Keyset $keyset = null)
    {
        $this
            ->setId($id)
            ->setType($type)
            ->setContact($contact)
            ->setNsset($nsset)
            ->setKeyset($keyset);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Prod\StructType\Info_Object_Data
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Info_Object_Data
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get contact value
     * @return \SubregSDK\Prod\StructType\Info_Object_Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }
    /**
     * Set contact value
     * @param \SubregSDK\Prod\StructType\Info_Object_Contact $contact
     * @return \SubregSDK\Prod\StructType\Info_Object_Data
     */
    public function setContact(\SubregSDK\Prod\StructType\Info_Object_Contact $contact = null)
    {
        $this->contact = $contact;
        return $this;
    }
    /**
     * Get nsset value
     * @return \SubregSDK\Prod\StructType\Info_Object_Nsset|null
     */
    public function getNsset()
    {
        return $this->nsset;
    }
    /**
     * Set nsset value
     * @param \SubregSDK\Prod\StructType\Info_Object_Nsset $nsset
     * @return \SubregSDK\Prod\StructType\Info_Object_Data
     */
    public function setNsset(\SubregSDK\Prod\StructType\Info_Object_Nsset $nsset = null)
    {
        $this->nsset = $nsset;
        return $this;
    }
    /**
     * Get keyset value
     * @return \SubregSDK\Prod\StructType\Info_Object_Keyset|null
     */
    public function getKeyset()
    {
        return $this->keyset;
    }
    /**
     * Set keyset value
     * @param \SubregSDK\Prod\StructType\Info_Object_Keyset $keyset
     * @return \SubregSDK\Prod\StructType\Info_Object_Data
     */
    public function setKeyset(\SubregSDK\Prod\StructType\Info_Object_Keyset $keyset = null)
    {
        $this->keyset = $keyset;
        return $this;
    }
}
