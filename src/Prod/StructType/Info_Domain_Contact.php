<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Domain_Contact StructType
 * @subpackage Structs
 */
class Info_Domain_Contact extends AbstractStructBase
{
    /**
     * The subregid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $subregid;
    /**
     * The registryid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $registryid;
    /**
     * Constructor method for Info_Domain_Contact
     * @uses Info_Domain_Contact::setSubregid()
     * @uses Info_Domain_Contact::setRegistryid()
     * @param string $subregid
     * @param string $registryid
     */
    public function __construct($subregid = null, $registryid = null)
    {
        $this
            ->setSubregid($subregid)
            ->setRegistryid($registryid);
    }
    /**
     * Get subregid value
     * @return string|null
     */
    public function getSubregid()
    {
        return $this->subregid;
    }
    /**
     * Set subregid value
     * @param string $subregid
     * @return \SubregSDK\Prod\StructType\Info_Domain_Contact
     */
    public function setSubregid($subregid = null)
    {
        // validation for constraint: string
        if (!is_null($subregid) && !is_string($subregid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($subregid, true), gettype($subregid)), __LINE__);
        }
        $this->subregid = $subregid;
        return $this;
    }
    /**
     * Get registryid value
     * @return string|null
     */
    public function getRegistryid()
    {
        return $this->registryid;
    }
    /**
     * Set registryid value
     * @param string $registryid
     * @return \SubregSDK\Prod\StructType\Info_Domain_Contact
     */
    public function setRegistryid($registryid = null)
    {
        // validation for constraint: string
        if (!is_null($registryid) && !is_string($registryid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($registryid, true), gettype($registryid)), __LINE__);
        }
        $this->registryid = $registryid;
        return $this;
    }
}
