<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Users_List_Container StructType
 * @subpackage Structs
 */
class Users_List_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Users_List_Response
     */
    public $response;
    /**
     * Constructor method for Users_List_Container
     * @uses Users_List_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Users_List_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Users_List_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Users_List_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Users_List_Response $response
     * @return \SubregSDK\Prod\StructType\Users_List_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Users_List_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
