<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for List_Documents_Data StructType
 * @subpackage Structs
 */
class List_Documents_Data extends AbstractStructBase
{
    /**
     * The documents
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\List_Documents_Document[]
     */
    public $documents;
    /**
     * Constructor method for List_Documents_Data
     * @uses List_Documents_Data::setDocuments()
     * @param \SubregSDK\Prod\StructType\List_Documents_Document[] $documents
     */
    public function __construct(array $documents = array())
    {
        $this
            ->setDocuments($documents);
    }
    /**
     * Get documents value
     * @return \SubregSDK\Prod\StructType\List_Documents_Document[]|null
     */
    public function getDocuments()
    {
        return $this->documents;
    }
    /**
     * This method is responsible for validating the values passed to the setDocuments method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDocuments method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDocumentsForArrayConstraintsFromSetDocuments(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $list_Documents_DataDocumentsItem) {
            // validation for constraint: itemType
            if (!$list_Documents_DataDocumentsItem instanceof \SubregSDK\Prod\StructType\List_Documents_Document) {
                $invalidValues[] = is_object($list_Documents_DataDocumentsItem) ? get_class($list_Documents_DataDocumentsItem) : sprintf('%s(%s)', gettype($list_Documents_DataDocumentsItem), var_export($list_Documents_DataDocumentsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The documents property can only contain items of type \SubregSDK\Prod\StructType\List_Documents_Document, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set documents value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\List_Documents_Document[] $documents
     * @return \SubregSDK\Prod\StructType\List_Documents_Data
     */
    public function setDocuments(array $documents = array())
    {
        // validation for constraint: array
        if ('' !== ($documentsArrayErrorMessage = self::validateDocumentsForArrayConstraintsFromSetDocuments($documents))) {
            throw new \InvalidArgumentException($documentsArrayErrorMessage, __LINE__);
        }
        $this->documents = $documents;
        return $this;
    }
    /**
     * Add item to documents value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\List_Documents_Document $item
     * @return \SubregSDK\Prod\StructType\List_Documents_Data
     */
    public function addToDocuments(\SubregSDK\Prod\StructType\List_Documents_Document $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\List_Documents_Document) {
            throw new \InvalidArgumentException(sprintf('The documents property can only contain items of type \SubregSDK\Prod\StructType\List_Documents_Document, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->documents[] = $item;
        return $this;
    }
}
