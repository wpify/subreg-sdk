<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Contacts_List_Data StructType
 * @subpackage Structs
 */
class Contacts_List_Data extends AbstractStructBase
{
    /**
     * The contacts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Contacts_List_Contact[]
     */
    public $contacts;
    /**
     * The count
     * @var int
     */
    public $count;
    /**
     * Constructor method for Contacts_List_Data
     * @uses Contacts_List_Data::setContacts()
     * @uses Contacts_List_Data::setCount()
     * @param \SubregSDK\Prod\StructType\Contacts_List_Contact[] $contacts
     * @param int $count
     */
    public function __construct(array $contacts = array(), $count = null)
    {
        $this
            ->setContacts($contacts)
            ->setCount($count);
    }
    /**
     * Get contacts value
     * @return \SubregSDK\Prod\StructType\Contacts_List_Contact[]|null
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    /**
     * This method is responsible for validating the values passed to the setContacts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setContacts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateContactsForArrayConstraintsFromSetContacts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $contacts_List_DataContactsItem) {
            // validation for constraint: itemType
            if (!$contacts_List_DataContactsItem instanceof \SubregSDK\Prod\StructType\Contacts_List_Contact) {
                $invalidValues[] = is_object($contacts_List_DataContactsItem) ? get_class($contacts_List_DataContactsItem) : sprintf('%s(%s)', gettype($contacts_List_DataContactsItem), var_export($contacts_List_DataContactsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The contacts property can only contain items of type \SubregSDK\Prod\StructType\Contacts_List_Contact, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set contacts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Contacts_List_Contact[] $contacts
     * @return \SubregSDK\Prod\StructType\Contacts_List_Data
     */
    public function setContacts(array $contacts = array())
    {
        // validation for constraint: array
        if ('' !== ($contactsArrayErrorMessage = self::validateContactsForArrayConstraintsFromSetContacts($contacts))) {
            throw new \InvalidArgumentException($contactsArrayErrorMessage, __LINE__);
        }
        $this->contacts = $contacts;
        return $this;
    }
    /**
     * Add item to contacts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Contacts_List_Contact $item
     * @return \SubregSDK\Prod\StructType\Contacts_List_Data
     */
    public function addToContacts(\SubregSDK\Prod\StructType\Contacts_List_Contact $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Contacts_List_Contact) {
            throw new \InvalidArgumentException(sprintf('The contacts property can only contain items of type \SubregSDK\Prod\StructType\Contacts_List_Contact, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->contacts[] = $item;
        return $this;
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \SubregSDK\Prod\StructType\Contacts_List_Data
     */
    public function setCount($count = null)
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        return $this;
    }
}
