<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Certificate_Data StructType
 * @subpackage Structs
 */
class Get_Certificate_Data extends AbstractStructBase
{
    /**
     * The certificate
     * @var string
     */
    public $certificate;
    /**
     * The expire
     * @var string
     */
    public $expire;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * Constructor method for Get_Certificate_Data
     * @uses Get_Certificate_Data::setCertificate()
     * @uses Get_Certificate_Data::setExpire()
     * @uses Get_Certificate_Data::setDomain()
     * @uses Get_Certificate_Data::setType()
     * @param string $certificate
     * @param string $expire
     * @param string $domain
     * @param string $type
     */
    public function __construct($certificate = null, $expire = null, $domain = null, $type = null)
    {
        $this
            ->setCertificate($certificate)
            ->setExpire($expire)
            ->setDomain($domain)
            ->setType($type);
    }
    /**
     * Get certificate value
     * @return string|null
     */
    public function getCertificate()
    {
        return $this->certificate;
    }
    /**
     * Set certificate value
     * @param string $certificate
     * @return \SubregSDK\Prod\StructType\Get_Certificate_Data
     */
    public function setCertificate($certificate = null)
    {
        // validation for constraint: string
        if (!is_null($certificate) && !is_string($certificate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($certificate, true), gettype($certificate)), __LINE__);
        }
        $this->certificate = $certificate;
        return $this;
    }
    /**
     * Get expire value
     * @return string|null
     */
    public function getExpire()
    {
        return $this->expire;
    }
    /**
     * Set expire value
     * @param string $expire
     * @return \SubregSDK\Prod\StructType\Get_Certificate_Data
     */
    public function setExpire($expire = null)
    {
        // validation for constraint: string
        if (!is_null($expire) && !is_string($expire)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($expire, true), gettype($expire)), __LINE__);
        }
        $this->expire = $expire;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Get_Certificate_Data
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Get_Certificate_Data
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
}
