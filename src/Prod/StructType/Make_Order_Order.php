<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Order StructType
 * @subpackage Structs
 */
class Make_Order_Order extends AbstractStructBase
{
    /**
     * The domain
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $domain;
    /**
     * The object
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $object;
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public $params;
    /**
     * Constructor method for Make_Order_Order
     * @uses Make_Order_Order::setDomain()
     * @uses Make_Order_Order::setObject()
     * @uses Make_Order_Order::setType()
     * @uses Make_Order_Order::setParams()
     * @param string $domain
     * @param string $object
     * @param string $type
     * @param \SubregSDK\Prod\StructType\Make_Order_Params $params
     */
    public function __construct($domain = null, $object = null, $type = null, \SubregSDK\Prod\StructType\Make_Order_Params $params = null)
    {
        $this
            ->setDomain($domain)
            ->setObject($object)
            ->setType($type)
            ->setParams($params);
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Make_Order_Order
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get object value
     * @return string|null
     */
    public function getObject()
    {
        return $this->object;
    }
    /**
     * Set object value
     * @param string $object
     * @return \SubregSDK\Prod\StructType\Make_Order_Order
     */
    public function setObject($object = null)
    {
        // validation for constraint: string
        if (!is_null($object) && !is_string($object)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($object, true), gettype($object)), __LINE__);
        }
        $this->object = $object;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Make_Order_Order
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Prod\StructType\Make_Order_Params|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * Set params value
     * @param \SubregSDK\Prod\StructType\Make_Order_Params $params
     * @return \SubregSDK\Prod\StructType\Make_Order_Order
     */
    public function setParams(\SubregSDK\Prod\StructType\Make_Order_Params $params = null)
    {
        $this->params = $params;
        return $this;
    }
}
