<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Domain_Price StructType
 * @subpackage Structs
 */
class Check_Domain_Price extends AbstractStructBase
{
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The amount_with_trustee
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $amount_with_trustee;
    /**
     * The premium
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $premium;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * Constructor method for Check_Domain_Price
     * @uses Check_Domain_Price::setAmount()
     * @uses Check_Domain_Price::setAmount_with_trustee()
     * @uses Check_Domain_Price::setPremium()
     * @uses Check_Domain_Price::setCurrency()
     * @param float $amount
     * @param float $amount_with_trustee
     * @param int $premium
     * @param string $currency
     */
    public function __construct($amount = null, $amount_with_trustee = null, $premium = null, $currency = null)
    {
        $this
            ->setAmount($amount)
            ->setAmount_with_trustee($amount_with_trustee)
            ->setPremium($premium)
            ->setCurrency($currency);
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Prod\StructType\Check_Domain_Price
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get amount_with_trustee value
     * @return float|null
     */
    public function getAmount_with_trustee()
    {
        return $this->amount_with_trustee;
    }
    /**
     * Set amount_with_trustee value
     * @param float $amount_with_trustee
     * @return \SubregSDK\Prod\StructType\Check_Domain_Price
     */
    public function setAmount_with_trustee($amount_with_trustee = null)
    {
        // validation for constraint: float
        if (!is_null($amount_with_trustee) && !(is_float($amount_with_trustee) || is_numeric($amount_with_trustee))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount_with_trustee, true), gettype($amount_with_trustee)), __LINE__);
        }
        $this->amount_with_trustee = $amount_with_trustee;
        return $this;
    }
    /**
     * Get premium value
     * @return int|null
     */
    public function getPremium()
    {
        return $this->premium;
    }
    /**
     * Set premium value
     * @param int $premium
     * @return \SubregSDK\Prod\StructType\Check_Domain_Price
     */
    public function setPremium($premium = null)
    {
        // validation for constraint: int
        if (!is_null($premium) && !(is_int($premium) || ctype_digit($premium))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($premium, true), gettype($premium)), __LINE__);
        }
        $this->premium = $premium;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Prod\StructType\Check_Domain_Price
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
}
