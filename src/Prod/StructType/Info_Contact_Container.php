<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Contact_Container StructType
 * @subpackage Structs
 */
class Info_Contact_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Info_Contact_Response
     */
    public $response;
    /**
     * Constructor method for Info_Contact_Container
     * @uses Info_Contact_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Info_Contact_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Info_Contact_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Info_Contact_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Info_Contact_Response $response
     * @return \SubregSDK\Prod\StructType\Info_Contact_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Info_Contact_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
