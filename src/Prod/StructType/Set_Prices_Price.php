<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Prices_Price StructType
 * @subpackage Structs
 */
class Set_Prices_Price extends AbstractStructBase
{
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The value
     * @var float
     */
    public $value;
    /**
     * Constructor method for Set_Prices_Price
     * @uses Set_Prices_Price::setType()
     * @uses Set_Prices_Price::setValue()
     * @param string $type
     * @param float $value
     */
    public function __construct($type = null, $value = null)
    {
        $this
            ->setType($type)
            ->setValue($value);
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Set_Prices_Price
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get value value
     * @return float|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param float $value
     * @return \SubregSDK\Prod\StructType\Set_Prices_Price
     */
    public function setValue($value = null)
    {
        // validation for constraint: float
        if (!is_null($value) && !(is_float($value) || is_numeric($value))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        return $this;
    }
}
