<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Certificate StructType
 * @subpackage Structs
 */
class Get_Certificate extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The orderid
     * @var int
     */
    public $orderid;
    /**
     * Constructor method for Get_Certificate
     * @uses Get_Certificate::setSsid()
     * @uses Get_Certificate::setOrderid()
     * @param string $ssid
     * @param int $orderid
     */
    public function __construct($ssid = null, $orderid = null)
    {
        $this
            ->setSsid($ssid)
            ->setOrderid($orderid);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Get_Certificate
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get orderid value
     * @return int|null
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
    /**
     * Set orderid value
     * @param int $orderid
     * @return \SubregSDK\Prod\StructType\Get_Certificate
     */
    public function setOrderid($orderid = null)
    {
        // validation for constraint: int
        if (!is_null($orderid) && !(is_int($orderid) || ctype_digit($orderid))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderid, true), gettype($orderid)), __LINE__);
        }
        $this->orderid = $orderid;
        return $this;
    }
}
