<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Object_Data StructType
 * @subpackage Structs
 */
class Check_Object_Data extends AbstractStructBase
{
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * The avail
     * @var int
     */
    public $avail;
    /**
     * Constructor method for Check_Object_Data
     * @uses Check_Object_Data::setId()
     * @uses Check_Object_Data::setAvail()
     * @param string $id
     * @param int $avail
     */
    public function __construct($id = null, $avail = null)
    {
        $this
            ->setId($id)
            ->setAvail($avail);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Prod\StructType\Check_Object_Data
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get avail value
     * @return int|null
     */
    public function getAvail()
    {
        return $this->avail;
    }
    /**
     * Set avail value
     * @param int $avail
     * @return \SubregSDK\Prod\StructType\Check_Object_Data
     */
    public function setAvail($avail = null)
    {
        // validation for constraint: int
        if (!is_null($avail) && !(is_int($avail) || ctype_digit($avail))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($avail, true), gettype($avail)), __LINE__);
        }
        $this->avail = $avail;
        return $this;
    }
}
