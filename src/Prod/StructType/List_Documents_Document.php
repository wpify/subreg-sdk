<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for List_Documents_Document StructType
 * @subpackage Structs
 */
class List_Documents_Document extends AbstractStructBase
{
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $type;
    /**
     * The filetype
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $filetype;
    /**
     * The account
     * @var string
     */
    public $account;
    /**
     * The orderid
     * @var int
     */
    public $orderid;
    /**
     * Constructor method for List_Documents_Document
     * @uses List_Documents_Document::setId()
     * @uses List_Documents_Document::setName()
     * @uses List_Documents_Document::setType()
     * @uses List_Documents_Document::setFiletype()
     * @uses List_Documents_Document::setAccount()
     * @uses List_Documents_Document::setOrderid()
     * @param string $id
     * @param string $name
     * @param string $type
     * @param string $filetype
     * @param string $account
     * @param int $orderid
     */
    public function __construct($id = null, $name = null, $type = null, $filetype = null, $account = null, $orderid = null)
    {
        $this
            ->setId($id)
            ->setName($name)
            ->setType($type)
            ->setFiletype($filetype)
            ->setAccount($account)
            ->setOrderid($orderid);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get filetype value
     * @return string|null
     */
    public function getFiletype()
    {
        return $this->filetype;
    }
    /**
     * Set filetype value
     * @param string $filetype
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setFiletype($filetype = null)
    {
        // validation for constraint: string
        if (!is_null($filetype) && !is_string($filetype)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filetype, true), gettype($filetype)), __LINE__);
        }
        $this->filetype = $filetype;
        return $this;
    }
    /**
     * Get account value
     * @return string|null
     */
    public function getAccount()
    {
        return $this->account;
    }
    /**
     * Set account value
     * @param string $account
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setAccount($account = null)
    {
        // validation for constraint: string
        if (!is_null($account) && !is_string($account)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($account, true), gettype($account)), __LINE__);
        }
        $this->account = $account;
        return $this;
    }
    /**
     * Get orderid value
     * @return int|null
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
    /**
     * Set orderid value
     * @param int $orderid
     * @return \SubregSDK\Prod\StructType\List_Documents_Document
     */
    public function setOrderid($orderid = null)
    {
        // validation for constraint: int
        if (!is_null($orderid) && !(is_int($orderid) || ctype_digit($orderid))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderid, true), gettype($orderid)), __LINE__);
        }
        $this->orderid = $orderid;
        return $this;
    }
}
