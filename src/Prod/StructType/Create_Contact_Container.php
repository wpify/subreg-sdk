<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Create_Contact_Container StructType
 * @subpackage Structs
 */
class Create_Contact_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Create_Contact_Response
     */
    public $response;
    /**
     * Constructor method for Create_Contact_Container
     * @uses Create_Contact_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Create_Contact_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Create_Contact_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Create_Contact_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Create_Contact_Response $response
     * @return \SubregSDK\Prod\StructType\Create_Contact_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Create_Contact_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
