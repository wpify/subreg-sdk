<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Pricelist_Data StructType
 * @subpackage Structs
 */
class Get_Pricelist_Data extends AbstractStructBase
{
    /**
     * The pricelist
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist[]
     */
    public $pricelist;
    /**
     * Constructor method for Get_Pricelist_Data
     * @uses Get_Pricelist_Data::setPricelist()
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist[] $pricelist
     */
    public function __construct(array $pricelist = array())
    {
        $this
            ->setPricelist($pricelist);
    }
    /**
     * Get pricelist value
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist[]|null
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }
    /**
     * This method is responsible for validating the values passed to the setPricelist method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPricelist method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePricelistForArrayConstraintsFromSetPricelist(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_Pricelist_DataPricelistItem) {
            // validation for constraint: itemType
            if (!$get_Pricelist_DataPricelistItem instanceof \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist) {
                $invalidValues[] = is_object($get_Pricelist_DataPricelistItem) ? get_class($get_Pricelist_DataPricelistItem) : sprintf('%s(%s)', gettype($get_Pricelist_DataPricelistItem), var_export($get_Pricelist_DataPricelistItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The pricelist property can only contain items of type \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set pricelist value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist[] $pricelist
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Data
     */
    public function setPricelist(array $pricelist = array())
    {
        // validation for constraint: array
        if ('' !== ($pricelistArrayErrorMessage = self::validatePricelistForArrayConstraintsFromSetPricelist($pricelist))) {
            throw new \InvalidArgumentException($pricelistArrayErrorMessage, __LINE__);
        }
        $this->pricelist = $pricelist;
        return $this;
    }
    /**
     * Add item to pricelist value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist $item
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Data
     */
    public function addToPricelist(\SubregSDK\Prod\StructType\Get_Pricelist_Pricelist $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist) {
            throw new \InvalidArgumentException(sprintf('The pricelist property can only contain items of type \SubregSDK\Prod\StructType\Get_Pricelist_Pricelist, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->pricelist[] = $item;
        return $this;
    }
}
