<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Accountings_Accounting StructType
 * @subpackage Structs
 */
class Get_Accountings_Accounting extends AbstractStructBase
{
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $date;
    /**
     * The text
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $text;
    /**
     * The order
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $order;
    /**
     * The sum
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $sum;
    /**
     * The credit
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $credit;
    /**
     * Constructor method for Get_Accountings_Accounting
     * @uses Get_Accountings_Accounting::setDate()
     * @uses Get_Accountings_Accounting::setText()
     * @uses Get_Accountings_Accounting::setOrder()
     * @uses Get_Accountings_Accounting::setSum()
     * @uses Get_Accountings_Accounting::setCredit()
     * @param string $date
     * @param string $text
     * @param int $order
     * @param float $sum
     * @param float $credit
     */
    public function __construct($date = null, $text = null, $order = null, $sum = null, $credit = null)
    {
        $this
            ->setDate($date)
            ->setText($text)
            ->setOrder($order)
            ->setSum($sum)
            ->setCredit($credit);
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting
     */
    public function setDate($date = null)
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        return $this;
    }
    /**
     * Get text value
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }
    /**
     * Set text value
     * @param string $text
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting
     */
    public function setText($text = null)
    {
        // validation for constraint: string
        if (!is_null($text) && !is_string($text)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($text, true), gettype($text)), __LINE__);
        }
        $this->text = $text;
        return $this;
    }
    /**
     * Get order value
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * Set order value
     * @param int $order
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting
     */
    public function setOrder($order = null)
    {
        // validation for constraint: int
        if (!is_null($order) && !(is_int($order) || ctype_digit($order))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($order, true), gettype($order)), __LINE__);
        }
        $this->order = $order;
        return $this;
    }
    /**
     * Get sum value
     * @return float|null
     */
    public function getSum()
    {
        return $this->sum;
    }
    /**
     * Set sum value
     * @param float $sum
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting
     */
    public function setSum($sum = null)
    {
        // validation for constraint: float
        if (!is_null($sum) && !(is_float($sum) || is_numeric($sum))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($sum, true), gettype($sum)), __LINE__);
        }
        $this->sum = $sum;
        return $this;
    }
    /**
     * Get credit value
     * @return float|null
     */
    public function getCredit()
    {
        return $this->credit;
    }
    /**
     * Set credit value
     * @param float $credit
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Accounting
     */
    public function setCredit($credit = null)
    {
        // validation for constraint: float
        if (!is_null($credit) && !(is_float($credit) || is_numeric($credit))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($credit, true), gettype($credit)), __LINE__);
        }
        $this->credit = $credit;
        return $this;
    }
}
