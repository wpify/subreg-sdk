<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Download_Document_Response StructType
 * @subpackage Structs
 */
class Download_Document_Response extends AbstractStructBase
{
    /**
     * The status
     * @var string
     */
    public $status;
    /**
     * The data
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Download_Document_Data
     */
    public $data;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Error_Info
     */
    public $error;
    /**
     * Constructor method for Download_Document_Response
     * @uses Download_Document_Response::setStatus()
     * @uses Download_Document_Response::setData()
     * @uses Download_Document_Response::setError()
     * @param string $status
     * @param \SubregSDK\Prod\StructType\Download_Document_Data $data
     * @param \SubregSDK\Prod\StructType\Error_Info $error
     */
    public function __construct($status = null, \SubregSDK\Prod\StructType\Download_Document_Data $data = null, \SubregSDK\Prod\StructType\Error_Info $error = null)
    {
        $this
            ->setStatus($status)
            ->setData($data)
            ->setError($error);
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \SubregSDK\Prod\StructType\Download_Document_Response
     */
    public function setStatus($status = null)
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        return $this;
    }
    /**
     * Get data value
     * @return \SubregSDK\Prod\StructType\Download_Document_Data|null
     */
    public function getData()
    {
        return $this->data;
    }
    /**
     * Set data value
     * @param \SubregSDK\Prod\StructType\Download_Document_Data $data
     * @return \SubregSDK\Prod\StructType\Download_Document_Response
     */
    public function setData(\SubregSDK\Prod\StructType\Download_Document_Data $data = null)
    {
        $this->data = $data;
        return $this;
    }
    /**
     * Get error value
     * @return \SubregSDK\Prod\StructType\Error_Info|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \SubregSDK\Prod\StructType\Error_Info $error
     * @return \SubregSDK\Prod\StructType\Download_Document_Response
     */
    public function setError(\SubregSDK\Prod\StructType\Error_Info $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
