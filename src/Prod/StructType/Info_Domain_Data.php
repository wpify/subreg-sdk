<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Domain_Data StructType
 * @subpackage Structs
 */
class Info_Domain_Data extends AbstractStructBase
{
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The contacts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Domain_Contacts
     */
    public $contacts;
    /**
     * The hosts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $hosts;
    /**
     * The registrant
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Domain_Contact
     */
    public $registrant;
    /**
     * The exDate
     * @var string
     */
    public $exDate;
    /**
     * The crDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $crDate;
    /**
     * The trDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $trDate;
    /**
     * The upDate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $upDate;
    /**
     * The authid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $authid;
    /**
     * The status
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $status;
    /**
     * The rgp
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $rgp;
    /**
     * The autorenew
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $autorenew;
    /**
     * The premium
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $premium;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $price;
    /**
     * The whoisproxy
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $whoisproxy;
    /**
     * The trustee
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $trustee;
    /**
     * The options
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Info_Domain_Options
     */
    public $options;
    /**
     * Constructor method for Info_Domain_Data
     * @uses Info_Domain_Data::setDomain()
     * @uses Info_Domain_Data::setContacts()
     * @uses Info_Domain_Data::setHosts()
     * @uses Info_Domain_Data::setRegistrant()
     * @uses Info_Domain_Data::setExDate()
     * @uses Info_Domain_Data::setCrDate()
     * @uses Info_Domain_Data::setTrDate()
     * @uses Info_Domain_Data::setUpDate()
     * @uses Info_Domain_Data::setAuthid()
     * @uses Info_Domain_Data::setStatus()
     * @uses Info_Domain_Data::setRgp()
     * @uses Info_Domain_Data::setAutorenew()
     * @uses Info_Domain_Data::setPremium()
     * @uses Info_Domain_Data::setPrice()
     * @uses Info_Domain_Data::setWhoisproxy()
     * @uses Info_Domain_Data::setTrustee()
     * @uses Info_Domain_Data::setOptions()
     * @param string $domain
     * @param \SubregSDK\Prod\StructType\Info_Domain_Contacts $contacts
     * @param string[] $hosts
     * @param \SubregSDK\Prod\StructType\Info_Domain_Contact $registrant
     * @param string $exDate
     * @param string $crDate
     * @param string $trDate
     * @param string $upDate
     * @param string $authid
     * @param string[] $status
     * @param string[] $rgp
     * @param int $autorenew
     * @param int $premium
     * @param float $price
     * @param int $whoisproxy
     * @param int $trustee
     * @param \SubregSDK\Prod\StructType\Info_Domain_Options $options
     */
    public function __construct($domain = null, \SubregSDK\Prod\StructType\Info_Domain_Contacts $contacts = null, array $hosts = array(), \SubregSDK\Prod\StructType\Info_Domain_Contact $registrant = null, $exDate = null, $crDate = null, $trDate = null, $upDate = null, $authid = null, array $status = array(), array $rgp = array(), $autorenew = null, $premium = null, $price = null, $whoisproxy = null, $trustee = null, \SubregSDK\Prod\StructType\Info_Domain_Options $options = null)
    {
        $this
            ->setDomain($domain)
            ->setContacts($contacts)
            ->setHosts($hosts)
            ->setRegistrant($registrant)
            ->setExDate($exDate)
            ->setCrDate($crDate)
            ->setTrDate($trDate)
            ->setUpDate($upDate)
            ->setAuthid($authid)
            ->setStatus($status)
            ->setRgp($rgp)
            ->setAutorenew($autorenew)
            ->setPremium($premium)
            ->setPrice($price)
            ->setWhoisproxy($whoisproxy)
            ->setTrustee($trustee)
            ->setOptions($options);
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get contacts value
     * @return \SubregSDK\Prod\StructType\Info_Domain_Contacts|null
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    /**
     * Set contacts value
     * @param \SubregSDK\Prod\StructType\Info_Domain_Contacts $contacts
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setContacts(\SubregSDK\Prod\StructType\Info_Domain_Contacts $contacts = null)
    {
        $this->contacts = $contacts;
        return $this;
    }
    /**
     * Get hosts value
     * @return string[]|null
     */
    public function getHosts()
    {
        return $this->hosts;
    }
    /**
     * This method is responsible for validating the values passed to the setHosts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHosts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHostsForArrayConstraintsFromSetHosts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_DataHostsItem) {
            // validation for constraint: itemType
            if (!is_string($info_Domain_DataHostsItem)) {
                $invalidValues[] = is_object($info_Domain_DataHostsItem) ? get_class($info_Domain_DataHostsItem) : sprintf('%s(%s)', gettype($info_Domain_DataHostsItem), var_export($info_Domain_DataHostsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The hosts property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set hosts value
     * @throws \InvalidArgumentException
     * @param string[] $hosts
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setHosts(array $hosts = array())
    {
        // validation for constraint: array
        if ('' !== ($hostsArrayErrorMessage = self::validateHostsForArrayConstraintsFromSetHosts($hosts))) {
            throw new \InvalidArgumentException($hostsArrayErrorMessage, __LINE__);
        }
        $this->hosts = $hosts;
        return $this;
    }
    /**
     * Add item to hosts value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function addToHosts($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The hosts property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->hosts[] = $item;
        return $this;
    }
    /**
     * Get registrant value
     * @return \SubregSDK\Prod\StructType\Info_Domain_Contact|null
     */
    public function getRegistrant()
    {
        return $this->registrant;
    }
    /**
     * Set registrant value
     * @param \SubregSDK\Prod\StructType\Info_Domain_Contact $registrant
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setRegistrant(\SubregSDK\Prod\StructType\Info_Domain_Contact $registrant = null)
    {
        $this->registrant = $registrant;
        return $this;
    }
    /**
     * Get exDate value
     * @return string|null
     */
    public function getExDate()
    {
        return $this->exDate;
    }
    /**
     * Set exDate value
     * @param string $exDate
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setExDate($exDate = null)
    {
        // validation for constraint: string
        if (!is_null($exDate) && !is_string($exDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($exDate, true), gettype($exDate)), __LINE__);
        }
        $this->exDate = $exDate;
        return $this;
    }
    /**
     * Get crDate value
     * @return string|null
     */
    public function getCrDate()
    {
        return $this->crDate;
    }
    /**
     * Set crDate value
     * @param string $crDate
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setCrDate($crDate = null)
    {
        // validation for constraint: string
        if (!is_null($crDate) && !is_string($crDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($crDate, true), gettype($crDate)), __LINE__);
        }
        $this->crDate = $crDate;
        return $this;
    }
    /**
     * Get trDate value
     * @return string|null
     */
    public function getTrDate()
    {
        return $this->trDate;
    }
    /**
     * Set trDate value
     * @param string $trDate
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setTrDate($trDate = null)
    {
        // validation for constraint: string
        if (!is_null($trDate) && !is_string($trDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trDate, true), gettype($trDate)), __LINE__);
        }
        $this->trDate = $trDate;
        return $this;
    }
    /**
     * Get upDate value
     * @return string|null
     */
    public function getUpDate()
    {
        return $this->upDate;
    }
    /**
     * Set upDate value
     * @param string $upDate
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setUpDate($upDate = null)
    {
        // validation for constraint: string
        if (!is_null($upDate) && !is_string($upDate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($upDate, true), gettype($upDate)), __LINE__);
        }
        $this->upDate = $upDate;
        return $this;
    }
    /**
     * Get authid value
     * @return string|null
     */
    public function getAuthid()
    {
        return $this->authid;
    }
    /**
     * Set authid value
     * @param string $authid
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setAuthid($authid = null)
    {
        // validation for constraint: string
        if (!is_null($authid) && !is_string($authid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authid, true), gettype($authid)), __LINE__);
        }
        $this->authid = $authid;
        return $this;
    }
    /**
     * Get status value
     * @return string[]|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * This method is responsible for validating the values passed to the setStatus method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatus method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusForArrayConstraintsFromSetStatus(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_DataStatusItem) {
            // validation for constraint: itemType
            if (!is_string($info_Domain_DataStatusItem)) {
                $invalidValues[] = is_object($info_Domain_DataStatusItem) ? get_class($info_Domain_DataStatusItem) : sprintf('%s(%s)', gettype($info_Domain_DataStatusItem), var_export($info_Domain_DataStatusItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The status property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set status value
     * @throws \InvalidArgumentException
     * @param string[] $status
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setStatus(array $status = array())
    {
        // validation for constraint: array
        if ('' !== ($statusArrayErrorMessage = self::validateStatusForArrayConstraintsFromSetStatus($status))) {
            throw new \InvalidArgumentException($statusArrayErrorMessage, __LINE__);
        }
        $this->status = $status;
        return $this;
    }
    /**
     * Add item to status value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function addToStatus($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The status property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->status[] = $item;
        return $this;
    }
    /**
     * Get rgp value
     * @return string[]|null
     */
    public function getRgp()
    {
        return $this->rgp;
    }
    /**
     * This method is responsible for validating the values passed to the setRgp method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRgp method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRgpForArrayConstraintsFromSetRgp(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_DataRgpItem) {
            // validation for constraint: itemType
            if (!is_string($info_Domain_DataRgpItem)) {
                $invalidValues[] = is_object($info_Domain_DataRgpItem) ? get_class($info_Domain_DataRgpItem) : sprintf('%s(%s)', gettype($info_Domain_DataRgpItem), var_export($info_Domain_DataRgpItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The rgp property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set rgp value
     * @throws \InvalidArgumentException
     * @param string[] $rgp
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setRgp(array $rgp = array())
    {
        // validation for constraint: array
        if ('' !== ($rgpArrayErrorMessage = self::validateRgpForArrayConstraintsFromSetRgp($rgp))) {
            throw new \InvalidArgumentException($rgpArrayErrorMessage, __LINE__);
        }
        $this->rgp = $rgp;
        return $this;
    }
    /**
     * Add item to rgp value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function addToRgp($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The rgp property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->rgp[] = $item;
        return $this;
    }
    /**
     * Get autorenew value
     * @return int|null
     */
    public function getAutorenew()
    {
        return $this->autorenew;
    }
    /**
     * Set autorenew value
     * @param int $autorenew
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setAutorenew($autorenew = null)
    {
        // validation for constraint: int
        if (!is_null($autorenew) && !(is_int($autorenew) || ctype_digit($autorenew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($autorenew, true), gettype($autorenew)), __LINE__);
        }
        $this->autorenew = $autorenew;
        return $this;
    }
    /**
     * Get premium value
     * @return int|null
     */
    public function getPremium()
    {
        return $this->premium;
    }
    /**
     * Set premium value
     * @param int $premium
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setPremium($premium = null)
    {
        // validation for constraint: int
        if (!is_null($premium) && !(is_int($premium) || ctype_digit($premium))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($premium, true), gettype($premium)), __LINE__);
        }
        $this->premium = $premium;
        return $this;
    }
    /**
     * Get price value
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param float $price
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setPrice($price = null)
    {
        // validation for constraint: float
        if (!is_null($price) && !(is_float($price) || is_numeric($price))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($price, true), gettype($price)), __LINE__);
        }
        $this->price = $price;
        return $this;
    }
    /**
     * Get whoisproxy value
     * @return int|null
     */
    public function getWhoisproxy()
    {
        return $this->whoisproxy;
    }
    /**
     * Set whoisproxy value
     * @param int $whoisproxy
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setWhoisproxy($whoisproxy = null)
    {
        // validation for constraint: int
        if (!is_null($whoisproxy) && !(is_int($whoisproxy) || ctype_digit($whoisproxy))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($whoisproxy, true), gettype($whoisproxy)), __LINE__);
        }
        $this->whoisproxy = $whoisproxy;
        return $this;
    }
    /**
     * Get trustee value
     * @return int|null
     */
    public function getTrustee()
    {
        return $this->trustee;
    }
    /**
     * Set trustee value
     * @param int $trustee
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setTrustee($trustee = null)
    {
        // validation for constraint: int
        if (!is_null($trustee) && !(is_int($trustee) || ctype_digit($trustee))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($trustee, true), gettype($trustee)), __LINE__);
        }
        $this->trustee = $trustee;
        return $this;
    }
    /**
     * Get options value
     * @return \SubregSDK\Prod\StructType\Info_Domain_Options|null
     */
    public function getOptions()
    {
        return $this->options;
    }
    /**
     * Set options value
     * @param \SubregSDK\Prod\StructType\Info_Domain_Options $options
     * @return \SubregSDK\Prod\StructType\Info_Domain_Data
     */
    public function setOptions(\SubregSDK\Prod\StructType\Info_Domain_Options $options = null)
    {
        $this->options = $options;
        return $this;
    }
}
