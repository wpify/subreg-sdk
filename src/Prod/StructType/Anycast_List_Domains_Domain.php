<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_List_Domains_Domain StructType
 * @subpackage Structs
 */
class Anycast_List_Domains_Domain extends AbstractStructBase
{
    /**
     * The domain
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $domain;
    /**
     * The day
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $day;
    /**
     * The month
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $month;
    /**
     * Constructor method for Anycast_List_Domains_Domain
     * @uses Anycast_List_Domains_Domain::setDomain()
     * @uses Anycast_List_Domains_Domain::setDay()
     * @uses Anycast_List_Domains_Domain::setMonth()
     * @param string $domain
     * @param int $day
     * @param int $month
     */
    public function __construct($domain = null, $day = null, $month = null)
    {
        $this
            ->setDomain($domain)
            ->setDay($day)
            ->setMonth($month);
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Anycast_List_Domains_Domain
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get day value
     * @return int|null
     */
    public function getDay()
    {
        return $this->day;
    }
    /**
     * Set day value
     * @param int $day
     * @return \SubregSDK\Prod\StructType\Anycast_List_Domains_Domain
     */
    public function setDay($day = null)
    {
        // validation for constraint: int
        if (!is_null($day) && !(is_int($day) || ctype_digit($day))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($day, true), gettype($day)), __LINE__);
        }
        $this->day = $day;
        return $this;
    }
    /**
     * Get month value
     * @return int|null
     */
    public function getMonth()
    {
        return $this->month;
    }
    /**
     * Set month value
     * @param int $month
     * @return \SubregSDK\Prod\StructType\Anycast_List_Domains_Domain
     */
    public function setMonth($month = null)
    {
        // validation for constraint: int
        if (!is_null($month) && !(is_int($month) || ctype_digit($month))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($month, true), gettype($month)), __LINE__);
        }
        $this->month = $month;
        return $this;
    }
}
