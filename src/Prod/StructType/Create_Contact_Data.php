<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Create_Contact_Data StructType
 * @subpackage Structs
 */
class Create_Contact_Data extends AbstractStructBase
{
    /**
     * The contactid
     * @var string
     */
    public $contactid;
    /**
     * Constructor method for Create_Contact_Data
     * @uses Create_Contact_Data::setContactid()
     * @param string $contactid
     */
    public function __construct($contactid = null)
    {
        $this
            ->setContactid($contactid);
    }
    /**
     * Get contactid value
     * @return string|null
     */
    public function getContactid()
    {
        return $this->contactid;
    }
    /**
     * Set contactid value
     * @param string $contactid
     * @return \SubregSDK\Prod\StructType\Create_Contact_Data
     */
    public function setContactid($contactid = null)
    {
        // validation for constraint: string
        if (!is_null($contactid) && !is_string($contactid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($contactid, true), gettype($contactid)), __LINE__);
        }
        $this->contactid = $contactid;
        return $this;
    }
}
