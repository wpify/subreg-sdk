<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Login_Container StructType
 * @subpackage Structs
 */
class Login_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Login_Response
     */
    public $response;
    /**
     * Constructor method for Login_Container
     * @uses Login_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Login_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Login_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Login_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Login_Response $response
     * @return \SubregSDK\Prod\StructType\Login_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Login_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
