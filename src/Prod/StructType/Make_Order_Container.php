<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Container StructType
 * @subpackage Structs
 */
class Make_Order_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Make_Order_Response
     */
    public $response;
    /**
     * Constructor method for Make_Order_Container
     * @uses Make_Order_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Make_Order_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Make_Order_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Make_Order_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Make_Order_Response $response
     * @return \SubregSDK\Prod\StructType\Make_Order_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Make_Order_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
