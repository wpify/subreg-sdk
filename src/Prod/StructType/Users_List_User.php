<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Users_List_User StructType
 * @subpackage Structs
 */
class Users_List_User extends AbstractStructBase
{
    /**
     * The id
     * @var int
     */
    public $id;
    /**
     * The username
     * @var string
     */
    public $username;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $name;
    /**
     * The credit
     * @var string
     */
    public $credit;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * The billing_name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $billing_name;
    /**
     * The billing_street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $billing_street;
    /**
     * The billing_city
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $billing_city;
    /**
     * The billing_pc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $billing_pc;
    /**
     * The billing_country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $billing_country;
    /**
     * The company_id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $company_id;
    /**
     * The company_vat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $company_vat;
    /**
     * The email
     * @var string
     */
    public $email;
    /**
     * The phone
     * @var string
     */
    public $phone;
    /**
     * The last_login
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $last_login;
    /**
     * Constructor method for Users_List_User
     * @uses Users_List_User::setId()
     * @uses Users_List_User::setUsername()
     * @uses Users_List_User::setName()
     * @uses Users_List_User::setCredit()
     * @uses Users_List_User::setCurrency()
     * @uses Users_List_User::setBilling_name()
     * @uses Users_List_User::setBilling_street()
     * @uses Users_List_User::setBilling_city()
     * @uses Users_List_User::setBilling_pc()
     * @uses Users_List_User::setBilling_country()
     * @uses Users_List_User::setCompany_id()
     * @uses Users_List_User::setCompany_vat()
     * @uses Users_List_User::setEmail()
     * @uses Users_List_User::setPhone()
     * @uses Users_List_User::setLast_login()
     * @param int $id
     * @param string $username
     * @param string $name
     * @param string $credit
     * @param string $currency
     * @param string $billing_name
     * @param string $billing_street
     * @param string $billing_city
     * @param string $billing_pc
     * @param string $billing_country
     * @param string $company_id
     * @param string $company_vat
     * @param string $email
     * @param string $phone
     * @param string $last_login
     */
    public function __construct($id = null, $username = null, $name = null, $credit = null, $currency = null, $billing_name = null, $billing_street = null, $billing_city = null, $billing_pc = null, $billing_country = null, $company_id = null, $company_vat = null, $email = null, $phone = null, $last_login = null)
    {
        $this
            ->setId($id)
            ->setUsername($username)
            ->setName($name)
            ->setCredit($credit)
            ->setCurrency($currency)
            ->setBilling_name($billing_name)
            ->setBilling_street($billing_street)
            ->setBilling_city($billing_city)
            ->setBilling_pc($billing_pc)
            ->setBilling_country($billing_country)
            ->setCompany_id($company_id)
            ->setCompany_vat($company_vat)
            ->setEmail($email)
            ->setPhone($phone)
            ->setLast_login($last_login);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set username value
     * @param string $username
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setUsername($username = null)
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        $this->username = $username;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get credit value
     * @return string|null
     */
    public function getCredit()
    {
        return $this->credit;
    }
    /**
     * Set credit value
     * @param string $credit
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setCredit($credit = null)
    {
        // validation for constraint: string
        if (!is_null($credit) && !is_string($credit)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($credit, true), gettype($credit)), __LINE__);
        }
        $this->credit = $credit;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
    /**
     * Get billing_name value
     * @return string|null
     */
    public function getBilling_name()
    {
        return $this->billing_name;
    }
    /**
     * Set billing_name value
     * @param string $billing_name
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setBilling_name($billing_name = null)
    {
        // validation for constraint: string
        if (!is_null($billing_name) && !is_string($billing_name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billing_name, true), gettype($billing_name)), __LINE__);
        }
        $this->billing_name = $billing_name;
        return $this;
    }
    /**
     * Get billing_street value
     * @return string|null
     */
    public function getBilling_street()
    {
        return $this->billing_street;
    }
    /**
     * Set billing_street value
     * @param string $billing_street
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setBilling_street($billing_street = null)
    {
        // validation for constraint: string
        if (!is_null($billing_street) && !is_string($billing_street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billing_street, true), gettype($billing_street)), __LINE__);
        }
        $this->billing_street = $billing_street;
        return $this;
    }
    /**
     * Get billing_city value
     * @return string|null
     */
    public function getBilling_city()
    {
        return $this->billing_city;
    }
    /**
     * Set billing_city value
     * @param string $billing_city
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setBilling_city($billing_city = null)
    {
        // validation for constraint: string
        if (!is_null($billing_city) && !is_string($billing_city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billing_city, true), gettype($billing_city)), __LINE__);
        }
        $this->billing_city = $billing_city;
        return $this;
    }
    /**
     * Get billing_pc value
     * @return string|null
     */
    public function getBilling_pc()
    {
        return $this->billing_pc;
    }
    /**
     * Set billing_pc value
     * @param string $billing_pc
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setBilling_pc($billing_pc = null)
    {
        // validation for constraint: string
        if (!is_null($billing_pc) && !is_string($billing_pc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billing_pc, true), gettype($billing_pc)), __LINE__);
        }
        $this->billing_pc = $billing_pc;
        return $this;
    }
    /**
     * Get billing_country value
     * @return string|null
     */
    public function getBilling_country()
    {
        return $this->billing_country;
    }
    /**
     * Set billing_country value
     * @param string $billing_country
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setBilling_country($billing_country = null)
    {
        // validation for constraint: string
        if (!is_null($billing_country) && !is_string($billing_country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($billing_country, true), gettype($billing_country)), __LINE__);
        }
        $this->billing_country = $billing_country;
        return $this;
    }
    /**
     * Get company_id value
     * @return string|null
     */
    public function getCompany_id()
    {
        return $this->company_id;
    }
    /**
     * Set company_id value
     * @param string $company_id
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setCompany_id($company_id = null)
    {
        // validation for constraint: string
        if (!is_null($company_id) && !is_string($company_id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company_id, true), gettype($company_id)), __LINE__);
        }
        $this->company_id = $company_id;
        return $this;
    }
    /**
     * Get company_vat value
     * @return string|null
     */
    public function getCompany_vat()
    {
        return $this->company_vat;
    }
    /**
     * Set company_vat value
     * @param string $company_vat
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setCompany_vat($company_vat = null)
    {
        // validation for constraint: string
        if (!is_null($company_vat) && !is_string($company_vat)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($company_vat, true), gettype($company_vat)), __LINE__);
        }
        $this->company_vat = $company_vat;
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
    /**
     * Get phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * Set phone value
     * @param string $phone
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        $this->phone = $phone;
        return $this;
    }
    /**
     * Get last_login value
     * @return string|null
     */
    public function getLast_login()
    {
        return $this->last_login;
    }
    /**
     * Set last_login value
     * @param string $last_login
     * @return \SubregSDK\Prod\StructType\Users_List_User
     */
    public function setLast_login($last_login = null)
    {
        // validation for constraint: string
        if (!is_null($last_login) && !is_string($last_login)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($last_login, true), gettype($last_login)), __LINE__);
        }
        $this->last_login = $last_login;
        return $this;
    }
}
