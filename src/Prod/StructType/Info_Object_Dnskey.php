<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Dnskey StructType
 * @subpackage Structs
 */
class Info_Object_Dnskey extends AbstractStructBase
{
    /**
     * The flags
     * @var string
     */
    public $flags;
    /**
     * The protocol
     * @var string
     */
    public $protocol;
    /**
     * The alg
     * @var string
     */
    public $alg;
    /**
     * The pubKey
     * @var string
     */
    public $pubKey;
    /**
     * Constructor method for Info_Object_Dnskey
     * @uses Info_Object_Dnskey::setFlags()
     * @uses Info_Object_Dnskey::setProtocol()
     * @uses Info_Object_Dnskey::setAlg()
     * @uses Info_Object_Dnskey::setPubKey()
     * @param string $flags
     * @param string $protocol
     * @param string $alg
     * @param string $pubKey
     */
    public function __construct($flags = null, $protocol = null, $alg = null, $pubKey = null)
    {
        $this
            ->setFlags($flags)
            ->setProtocol($protocol)
            ->setAlg($alg)
            ->setPubKey($pubKey);
    }
    /**
     * Get flags value
     * @return string|null
     */
    public function getFlags()
    {
        return $this->flags;
    }
    /**
     * Set flags value
     * @param string $flags
     * @return \SubregSDK\Prod\StructType\Info_Object_Dnskey
     */
    public function setFlags($flags = null)
    {
        // validation for constraint: string
        if (!is_null($flags) && !is_string($flags)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($flags, true), gettype($flags)), __LINE__);
        }
        $this->flags = $flags;
        return $this;
    }
    /**
     * Get protocol value
     * @return string|null
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    /**
     * Set protocol value
     * @param string $protocol
     * @return \SubregSDK\Prod\StructType\Info_Object_Dnskey
     */
    public function setProtocol($protocol = null)
    {
        // validation for constraint: string
        if (!is_null($protocol) && !is_string($protocol)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($protocol, true), gettype($protocol)), __LINE__);
        }
        $this->protocol = $protocol;
        return $this;
    }
    /**
     * Get alg value
     * @return string|null
     */
    public function getAlg()
    {
        return $this->alg;
    }
    /**
     * Set alg value
     * @param string $alg
     * @return \SubregSDK\Prod\StructType\Info_Object_Dnskey
     */
    public function setAlg($alg = null)
    {
        // validation for constraint: string
        if (!is_null($alg) && !is_string($alg)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($alg, true), gettype($alg)), __LINE__);
        }
        $this->alg = $alg;
        return $this;
    }
    /**
     * Get pubKey value
     * @return string|null
     */
    public function getPubKey()
    {
        return $this->pubKey;
    }
    /**
     * Set pubKey value
     * @param string $pubKey
     * @return \SubregSDK\Prod\StructType\Info_Object_Dnskey
     */
    public function setPubKey($pubKey = null)
    {
        // validation for constraint: string
        if (!is_null($pubKey) && !is_string($pubKey)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pubKey, true), gettype($pubKey)), __LINE__);
        }
        $this->pubKey = $pubKey;
        return $this;
    }
}
