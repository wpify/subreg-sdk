<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Pricelist_Container StructType
 * @subpackage Structs
 */
class Get_Pricelist_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Get_Pricelist_Response
     */
    public $response;
    /**
     * Constructor method for Get_Pricelist_Container
     * @uses Get_Pricelist_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Get_Pricelist_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Get_Pricelist_Response $response
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Get_Pricelist_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
