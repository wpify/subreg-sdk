<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Upload_Document StructType
 * @subpackage Structs
 */
class Upload_Document extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The document
     * @var string
     */
    public $document;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $type;
    /**
     * The filetype
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $filetype;
    /**
     * Constructor method for Upload_Document
     * @uses Upload_Document::setSsid()
     * @uses Upload_Document::setName()
     * @uses Upload_Document::setDocument()
     * @uses Upload_Document::setType()
     * @uses Upload_Document::setFiletype()
     * @param string $ssid
     * @param string $name
     * @param string $document
     * @param string $type
     * @param string $filetype
     */
    public function __construct($ssid = null, $name = null, $document = null, $type = null, $filetype = null)
    {
        $this
            ->setSsid($ssid)
            ->setName($name)
            ->setDocument($document)
            ->setType($type)
            ->setFiletype($filetype);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Upload_Document
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Prod\StructType\Upload_Document
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get document value
     * @return string|null
     */
    public function getDocument()
    {
        return $this->document;
    }
    /**
     * Set document value
     * @param string $document
     * @return \SubregSDK\Prod\StructType\Upload_Document
     */
    public function setDocument($document = null)
    {
        // validation for constraint: string
        if (!is_null($document) && !is_string($document)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($document, true), gettype($document)), __LINE__);
        }
        $this->document = $document;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Upload_Document
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get filetype value
     * @return string|null
     */
    public function getFiletype()
    {
        return $this->filetype;
    }
    /**
     * Set filetype value
     * @param string $filetype
     * @return \SubregSDK\Prod\StructType\Upload_Document
     */
    public function setFiletype($filetype = null)
    {
        // validation for constraint: string
        if (!is_null($filetype) && !is_string($filetype)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filetype, true), gettype($filetype)), __LINE__);
        }
        $this->filetype = $filetype;
        return $this;
    }
}
