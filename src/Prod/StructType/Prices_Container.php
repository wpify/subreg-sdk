<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Prices_Container StructType
 * @subpackage Structs
 */
class Prices_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Prices_Response
     */
    public $response;
    /**
     * Constructor method for Prices_Container
     * @uses Prices_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Prices_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Prices_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Prices_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Prices_Response $response
     * @return \SubregSDK\Prod\StructType\Prices_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Prices_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
