<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Object_Container StructType
 * @subpackage Structs
 */
class Check_Object_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Check_Object_Response
     */
    public $response;
    /**
     * Constructor method for Check_Object_Container
     * @uses Check_Object_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Check_Object_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Check_Object_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Check_Object_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Check_Object_Response $response
     * @return \SubregSDK\Prod\StructType\Check_Object_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Check_Object_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
