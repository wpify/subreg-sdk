<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_TLD_Info_Contact StructType
 * @subpackage Structs
 */
class Get_TLD_Info_Contact extends AbstractStructBase
{
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $type;
    /**
     * The cnt
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $cnt;
    /**
     * Constructor method for Get_TLD_Info_Contact
     * @uses Get_TLD_Info_Contact::setType()
     * @uses Get_TLD_Info_Contact::setCnt()
     * @param string $type
     * @param int $cnt
     */
    public function __construct($type = null, $cnt = null)
    {
        $this
            ->setType($type)
            ->setCnt($cnt);
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Get_TLD_Info_Contact
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get cnt value
     * @return int|null
     */
    public function getCnt()
    {
        return $this->cnt;
    }
    /**
     * Set cnt value
     * @param int $cnt
     * @return \SubregSDK\Prod\StructType\Get_TLD_Info_Contact
     */
    public function setCnt($cnt = null)
    {
        // validation for constraint: int
        if (!is_null($cnt) && !(is_int($cnt) || ctype_digit($cnt))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($cnt, true), gettype($cnt)), __LINE__);
        }
        $this->cnt = $cnt;
        return $this;
    }
}
