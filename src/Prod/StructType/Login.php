<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Login StructType
 * @subpackage Structs
 */
class Login extends AbstractStructBase
{
    /**
     * The login
     * @var string
     */
    public $login;
    /**
     * The password
     * @var string
     */
    public $password;
    /**
     * Constructor method for Login
     * @uses Login::setLogin()
     * @uses Login::setPassword()
     * @param string $login
     * @param string $password
     */
    public function __construct($login = null, $password = null)
    {
        $this
            ->setLogin($login)
            ->setPassword($password);
    }
    /**
     * Get login value
     * @return string|null
     */
    public function getLogin()
    {
        return $this->login;
    }
    /**
     * Set login value
     * @param string $login
     * @return \SubregSDK\Prod\StructType\Login
     */
    public function setLogin($login = null)
    {
        // validation for constraint: string
        if (!is_null($login) && !is_string($login)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($login, true), gettype($login)), __LINE__);
        }
        $this->login = $login;
        return $this;
    }
    /**
     * Get password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set password value
     * @param string $password
     * @return \SubregSDK\Prod\StructType\Login
     */
    public function setPassword($password = null)
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        $this->password = $password;
        return $this;
    }
}
