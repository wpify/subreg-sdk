<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_DNS_Zone_Data StructType
 * @subpackage Structs
 */
class Get_DNS_Zone_Data extends AbstractStructBase
{
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The records
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Get_DNS_Zone_Record[]
     */
    public $records;
    /**
     * Constructor method for Get_DNS_Zone_Data
     * @uses Get_DNS_Zone_Data::setDomain()
     * @uses Get_DNS_Zone_Data::setRecords()
     * @param string $domain
     * @param \SubregSDK\Prod\StructType\Get_DNS_Zone_Record[] $records
     */
    public function __construct($domain = null, array $records = array())
    {
        $this
            ->setDomain($domain)
            ->setRecords($records);
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Get_DNS_Zone_Data
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get records value
     * @return \SubregSDK\Prod\StructType\Get_DNS_Zone_Record[]|null
     */
    public function getRecords()
    {
        return $this->records;
    }
    /**
     * This method is responsible for validating the values passed to the setRecords method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRecords method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRecordsForArrayConstraintsFromSetRecords(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_DNS_Zone_DataRecordsItem) {
            // validation for constraint: itemType
            if (!$get_DNS_Zone_DataRecordsItem instanceof \SubregSDK\Prod\StructType\Get_DNS_Zone_Record) {
                $invalidValues[] = is_object($get_DNS_Zone_DataRecordsItem) ? get_class($get_DNS_Zone_DataRecordsItem) : sprintf('%s(%s)', gettype($get_DNS_Zone_DataRecordsItem), var_export($get_DNS_Zone_DataRecordsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The records property can only contain items of type \SubregSDK\Prod\StructType\Get_DNS_Zone_Record, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_DNS_Zone_Record[] $records
     * @return \SubregSDK\Prod\StructType\Get_DNS_Zone_Data
     */
    public function setRecords(array $records = array())
    {
        // validation for constraint: array
        if ('' !== ($recordsArrayErrorMessage = self::validateRecordsForArrayConstraintsFromSetRecords($records))) {
            throw new \InvalidArgumentException($recordsArrayErrorMessage, __LINE__);
        }
        $this->records = $records;
        return $this;
    }
    /**
     * Add item to records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Get_DNS_Zone_Record $item
     * @return \SubregSDK\Prod\StructType\Get_DNS_Zone_Data
     */
    public function addToRecords(\SubregSDK\Prod\StructType\Get_DNS_Zone_Record $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Get_DNS_Zone_Record) {
            throw new \InvalidArgumentException(sprintf('The records property can only contain items of type \SubregSDK\Prod\StructType\Get_DNS_Zone_Record, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->records[] = $item;
        return $this;
    }
}
