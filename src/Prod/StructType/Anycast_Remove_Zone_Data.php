<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_Remove_Zone_Data StructType
 * @subpackage Structs
 */
class Anycast_Remove_Zone_Data extends AbstractStructBase
{
}
