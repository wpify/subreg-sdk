<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Download_Document_Container StructType
 * @subpackage Structs
 */
class Download_Document_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Download_Document_Response
     */
    public $response;
    /**
     * Constructor method for Download_Document_Container
     * @uses Download_Document_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Download_Document_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Download_Document_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Download_Document_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Download_Document_Response $response
     * @return \SubregSDK\Prod\StructType\Download_Document_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Download_Document_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
