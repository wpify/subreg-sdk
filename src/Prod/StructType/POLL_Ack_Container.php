<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for POLL_Ack_Container StructType
 * @subpackage Structs
 */
class POLL_Ack_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\POLL_Ack_Response
     */
    public $response;
    /**
     * Constructor method for POLL_Ack_Container
     * @uses POLL_Ack_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\POLL_Ack_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\POLL_Ack_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\POLL_Ack_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\POLL_Ack_Response $response
     * @return \SubregSDK\Prod\StructType\POLL_Ack_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\POLL_Ack_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
