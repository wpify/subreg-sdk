<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Order_Order StructType
 * @subpackage Structs
 */
class Info_Order_Order extends AbstractStructBase
{
    /**
     * The id
     * @var int
     */
    public $id;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The status
     * @var string
     */
    public $status;
    /**
     * The errorcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $errorcode;
    /**
     * The lastupdate
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $lastupdate;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $message;
    /**
     * The payed
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $payed;
    /**
     * The amount
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $amount;
    /**
     * Constructor method for Info_Order_Order
     * @uses Info_Order_Order::setId()
     * @uses Info_Order_Order::setDomain()
     * @uses Info_Order_Order::setType()
     * @uses Info_Order_Order::setStatus()
     * @uses Info_Order_Order::setErrorcode()
     * @uses Info_Order_Order::setLastupdate()
     * @uses Info_Order_Order::setMessage()
     * @uses Info_Order_Order::setPayed()
     * @uses Info_Order_Order::setAmount()
     * @param int $id
     * @param string $domain
     * @param string $type
     * @param string $status
     * @param string $errorcode
     * @param string $lastupdate
     * @param string $message
     * @param string $payed
     * @param float $amount
     */
    public function __construct($id = null, $domain = null, $type = null, $status = null, $errorcode = null, $lastupdate = null, $message = null, $payed = null, $amount = null)
    {
        $this
            ->setId($id)
            ->setDomain($domain)
            ->setType($type)
            ->setStatus($status)
            ->setErrorcode($errorcode)
            ->setLastupdate($lastupdate)
            ->setMessage($message)
            ->setPayed($payed)
            ->setAmount($amount);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setStatus($status = null)
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        return $this;
    }
    /**
     * Get errorcode value
     * @return string|null
     */
    public function getErrorcode()
    {
        return $this->errorcode;
    }
    /**
     * Set errorcode value
     * @param string $errorcode
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setErrorcode($errorcode = null)
    {
        // validation for constraint: string
        if (!is_null($errorcode) && !is_string($errorcode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorcode, true), gettype($errorcode)), __LINE__);
        }
        $this->errorcode = $errorcode;
        return $this;
    }
    /**
     * Get lastupdate value
     * @return string|null
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }
    /**
     * Set lastupdate value
     * @param string $lastupdate
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setLastupdate($lastupdate = null)
    {
        // validation for constraint: string
        if (!is_null($lastupdate) && !is_string($lastupdate)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lastupdate, true), gettype($lastupdate)), __LINE__);
        }
        $this->lastupdate = $lastupdate;
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        return $this;
    }
    /**
     * Get payed value
     * @return string|null
     */
    public function getPayed()
    {
        return $this->payed;
    }
    /**
     * Set payed value
     * @param string $payed
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setPayed($payed = null)
    {
        // validation for constraint: string
        if (!is_null($payed) && !is_string($payed)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($payed, true), gettype($payed)), __LINE__);
        }
        $this->payed = $payed;
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Prod\StructType\Info_Order_Order
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
}
