<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Params StructType
 * @subpackage Structs
 */
class Make_Order_Params extends AbstractStructBase
{
    /**
     * The period
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $period;
    /**
     * The registrant
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contact
     */
    public $registrant;
    /**
     * The contacts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Contacts
     */
    public $contacts;
    /**
     * The ns
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Ns
     */
    public $ns;
    /**
     * The new
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_New
     */
    public $new;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $id;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $type;
    /**
     * The registry
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $registry;
    /**
     * The authid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $authid;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Prod\StructType\Make_Order_Param[]
     */
    public $params;
    /**
     * The newowner
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $newowner;
    /**
     * The reason
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $reason;
    /**
     * The nicd
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $nicd;
    /**
     * The password
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $password;
    /**
     * The hostname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $hostname;
    /**
     * The ipv4
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ipv4;
    /**
     * The ipv6
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ipv6;
    /**
     * The dnstemp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $dnstemp;
    /**
     * The statuses
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $statuses;
    /**
     * The autorenew
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $autorenew;
    /**
     * Constructor method for Make_Order_Params
     * @uses Make_Order_Params::setPeriod()
     * @uses Make_Order_Params::setRegistrant()
     * @uses Make_Order_Params::setContacts()
     * @uses Make_Order_Params::setNs()
     * @uses Make_Order_Params::setNew()
     * @uses Make_Order_Params::setId()
     * @uses Make_Order_Params::setType()
     * @uses Make_Order_Params::setRegistry()
     * @uses Make_Order_Params::setAuthid()
     * @uses Make_Order_Params::setParams()
     * @uses Make_Order_Params::setNewowner()
     * @uses Make_Order_Params::setReason()
     * @uses Make_Order_Params::setNicd()
     * @uses Make_Order_Params::setPassword()
     * @uses Make_Order_Params::setHostname()
     * @uses Make_Order_Params::setIpv4()
     * @uses Make_Order_Params::setIpv6()
     * @uses Make_Order_Params::setDnstemp()
     * @uses Make_Order_Params::setStatuses()
     * @uses Make_Order_Params::setAutorenew()
     * @param int $period
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $registrant
     * @param \SubregSDK\Prod\StructType\Make_Order_Contacts $contacts
     * @param \SubregSDK\Prod\StructType\Make_Order_Ns $ns
     * @param \SubregSDK\Prod\StructType\Make_Order_New $new
     * @param string $id
     * @param string $type
     * @param string $registry
     * @param string $authid
     * @param \SubregSDK\Prod\StructType\Make_Order_Param[] $params
     * @param string $newowner
     * @param string $reason
     * @param string $nicd
     * @param string $password
     * @param string $hostname
     * @param string $ipv4
     * @param string $ipv6
     * @param string $dnstemp
     * @param string[] $statuses
     * @param int $autorenew
     */
    public function __construct($period = null, \SubregSDK\Prod\StructType\Make_Order_Contact $registrant = null, \SubregSDK\Prod\StructType\Make_Order_Contacts $contacts = null, \SubregSDK\Prod\StructType\Make_Order_Ns $ns = null, \SubregSDK\Prod\StructType\Make_Order_New $new = null, $id = null, $type = null, $registry = null, $authid = null, array $params = array(), $newowner = null, $reason = null, $nicd = null, $password = null, $hostname = null, $ipv4 = null, $ipv6 = null, $dnstemp = null, array $statuses = array(), $autorenew = null)
    {
        $this
            ->setPeriod($period)
            ->setRegistrant($registrant)
            ->setContacts($contacts)
            ->setNs($ns)
            ->setNew($new)
            ->setId($id)
            ->setType($type)
            ->setRegistry($registry)
            ->setAuthid($authid)
            ->setParams($params)
            ->setNewowner($newowner)
            ->setReason($reason)
            ->setNicd($nicd)
            ->setPassword($password)
            ->setHostname($hostname)
            ->setIpv4($ipv4)
            ->setIpv6($ipv6)
            ->setDnstemp($dnstemp)
            ->setStatuses($statuses)
            ->setAutorenew($autorenew);
    }
    /**
     * Get period value
     * @return int|null
     */
    public function getPeriod()
    {
        return $this->period;
    }
    /**
     * Set period value
     * @param int $period
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setPeriod($period = null)
    {
        // validation for constraint: int
        if (!is_null($period) && !(is_int($period) || ctype_digit($period))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($period, true), gettype($period)), __LINE__);
        }
        $this->period = $period;
        return $this;
    }
    /**
     * Get registrant value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contact|null
     */
    public function getRegistrant()
    {
        return $this->registrant;
    }
    /**
     * Set registrant value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contact $registrant
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setRegistrant(\SubregSDK\Prod\StructType\Make_Order_Contact $registrant = null)
    {
        $this->registrant = $registrant;
        return $this;
    }
    /**
     * Get contacts value
     * @return \SubregSDK\Prod\StructType\Make_Order_Contacts|null
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    /**
     * Set contacts value
     * @param \SubregSDK\Prod\StructType\Make_Order_Contacts $contacts
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setContacts(\SubregSDK\Prod\StructType\Make_Order_Contacts $contacts = null)
    {
        $this->contacts = $contacts;
        return $this;
    }
    /**
     * Get ns value
     * @return \SubregSDK\Prod\StructType\Make_Order_Ns|null
     */
    public function getNs()
    {
        return $this->ns;
    }
    /**
     * Set ns value
     * @param \SubregSDK\Prod\StructType\Make_Order_Ns $ns
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setNs(\SubregSDK\Prod\StructType\Make_Order_Ns $ns = null)
    {
        $this->ns = $ns;
        return $this;
    }
    /**
     * Get new value
     * @return \SubregSDK\Prod\StructType\Make_Order_New|null
     */
    public function getNew()
    {
        return $this->new;
    }
    /**
     * Set new value
     * @param \SubregSDK\Prod\StructType\Make_Order_New $new
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setNew(\SubregSDK\Prod\StructType\Make_Order_New $new = null)
    {
        $this->new = $new;
        return $this;
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get registry value
     * @return string|null
     */
    public function getRegistry()
    {
        return $this->registry;
    }
    /**
     * Set registry value
     * @param string $registry
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setRegistry($registry = null)
    {
        // validation for constraint: string
        if (!is_null($registry) && !is_string($registry)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($registry, true), gettype($registry)), __LINE__);
        }
        $this->registry = $registry;
        return $this;
    }
    /**
     * Get authid value
     * @return string|null
     */
    public function getAuthid()
    {
        return $this->authid;
    }
    /**
     * Set authid value
     * @param string $authid
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setAuthid($authid = null)
    {
        // validation for constraint: string
        if (!is_null($authid) && !is_string($authid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($authid, true), gettype($authid)), __LINE__);
        }
        $this->authid = $authid;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Prod\StructType\Make_Order_Param[]|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * This method is responsible for validating the values passed to the setParams method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParams method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParamsForArrayConstraintsFromSetParams(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_ParamsParamsItem) {
            // validation for constraint: itemType
            if (!$make_Order_ParamsParamsItem instanceof \SubregSDK\Prod\StructType\Make_Order_Param) {
                $invalidValues[] = is_object($make_Order_ParamsParamsItem) ? get_class($make_Order_ParamsParamsItem) : sprintf('%s(%s)', gettype($make_Order_ParamsParamsItem), var_export($make_Order_ParamsParamsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The params property can only contain items of type \SubregSDK\Prod\StructType\Make_Order_Param, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Make_Order_Param[] $params
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setParams(array $params = array())
    {
        // validation for constraint: array
        if ('' !== ($paramsArrayErrorMessage = self::validateParamsForArrayConstraintsFromSetParams($params))) {
            throw new \InvalidArgumentException($paramsArrayErrorMessage, __LINE__);
        }
        $this->params = $params;
        return $this;
    }
    /**
     * Add item to params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Prod\StructType\Make_Order_Param $item
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function addToParams(\SubregSDK\Prod\StructType\Make_Order_Param $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Prod\StructType\Make_Order_Param) {
            throw new \InvalidArgumentException(sprintf('The params property can only contain items of type \SubregSDK\Prod\StructType\Make_Order_Param, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->params[] = $item;
        return $this;
    }
    /**
     * Get newowner value
     * @return string|null
     */
    public function getNewowner()
    {
        return $this->newowner;
    }
    /**
     * Set newowner value
     * @param string $newowner
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setNewowner($newowner = null)
    {
        // validation for constraint: string
        if (!is_null($newowner) && !is_string($newowner)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($newowner, true), gettype($newowner)), __LINE__);
        }
        $this->newowner = $newowner;
        return $this;
    }
    /**
     * Get reason value
     * @return string|null
     */
    public function getReason()
    {
        return $this->reason;
    }
    /**
     * Set reason value
     * @param string $reason
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setReason($reason = null)
    {
        // validation for constraint: string
        if (!is_null($reason) && !is_string($reason)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reason, true), gettype($reason)), __LINE__);
        }
        $this->reason = $reason;
        return $this;
    }
    /**
     * Get nicd value
     * @return string|null
     */
    public function getNicd()
    {
        return $this->nicd;
    }
    /**
     * Set nicd value
     * @param string $nicd
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setNicd($nicd = null)
    {
        // validation for constraint: string
        if (!is_null($nicd) && !is_string($nicd)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nicd, true), gettype($nicd)), __LINE__);
        }
        $this->nicd = $nicd;
        return $this;
    }
    /**
     * Get password value
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * Set password value
     * @param string $password
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setPassword($password = null)
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($password, true), gettype($password)), __LINE__);
        }
        $this->password = $password;
        return $this;
    }
    /**
     * Get hostname value
     * @return string|null
     */
    public function getHostname()
    {
        return $this->hostname;
    }
    /**
     * Set hostname value
     * @param string $hostname
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setHostname($hostname = null)
    {
        // validation for constraint: string
        if (!is_null($hostname) && !is_string($hostname)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($hostname, true), gettype($hostname)), __LINE__);
        }
        $this->hostname = $hostname;
        return $this;
    }
    /**
     * Get ipv4 value
     * @return string|null
     */
    public function getIpv4()
    {
        return $this->ipv4;
    }
    /**
     * Set ipv4 value
     * @param string $ipv4
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setIpv4($ipv4 = null)
    {
        // validation for constraint: string
        if (!is_null($ipv4) && !is_string($ipv4)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ipv4, true), gettype($ipv4)), __LINE__);
        }
        $this->ipv4 = $ipv4;
        return $this;
    }
    /**
     * Get ipv6 value
     * @return string|null
     */
    public function getIpv6()
    {
        return $this->ipv6;
    }
    /**
     * Set ipv6 value
     * @param string $ipv6
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setIpv6($ipv6 = null)
    {
        // validation for constraint: string
        if (!is_null($ipv6) && !is_string($ipv6)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ipv6, true), gettype($ipv6)), __LINE__);
        }
        $this->ipv6 = $ipv6;
        return $this;
    }
    /**
     * Get dnstemp value
     * @return string|null
     */
    public function getDnstemp()
    {
        return $this->dnstemp;
    }
    /**
     * Set dnstemp value
     * @param string $dnstemp
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setDnstemp($dnstemp = null)
    {
        // validation for constraint: string
        if (!is_null($dnstemp) && !is_string($dnstemp)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($dnstemp, true), gettype($dnstemp)), __LINE__);
        }
        $this->dnstemp = $dnstemp;
        return $this;
    }
    /**
     * Get statuses value
     * @return string[]|null
     */
    public function getStatuses()
    {
        return $this->statuses;
    }
    /**
     * This method is responsible for validating the values passed to the setStatuses method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatuses method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusesForArrayConstraintsFromSetStatuses(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_ParamsStatusesItem) {
            // validation for constraint: itemType
            if (!is_string($make_Order_ParamsStatusesItem)) {
                $invalidValues[] = is_object($make_Order_ParamsStatusesItem) ? get_class($make_Order_ParamsStatusesItem) : sprintf('%s(%s)', gettype($make_Order_ParamsStatusesItem), var_export($make_Order_ParamsStatusesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The statuses property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set statuses value
     * @throws \InvalidArgumentException
     * @param string[] $statuses
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setStatuses(array $statuses = array())
    {
        // validation for constraint: array
        if ('' !== ($statusesArrayErrorMessage = self::validateStatusesForArrayConstraintsFromSetStatuses($statuses))) {
            throw new \InvalidArgumentException($statusesArrayErrorMessage, __LINE__);
        }
        $this->statuses = $statuses;
        return $this;
    }
    /**
     * Add item to statuses value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function addToStatuses($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The statuses property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->statuses[] = $item;
        return $this;
    }
    /**
     * Get autorenew value
     * @return int|null
     */
    public function getAutorenew()
    {
        return $this->autorenew;
    }
    /**
     * Set autorenew value
     * @param int $autorenew
     * @return \SubregSDK\Prod\StructType\Make_Order_Params
     */
    public function setAutorenew($autorenew = null)
    {
        // validation for constraint: int
        if (!is_null($autorenew) && !(is_int($autorenew) || ctype_digit($autorenew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($autorenew, true), gettype($autorenew)), __LINE__);
        }
        $this->autorenew = $autorenew;
        return $this;
    }
}
