<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Delete_DNS_Zone_Container StructType
 * @subpackage Structs
 */
class Delete_DNS_Zone_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Prod\StructType\Delete_DNS_Zone_Response
     */
    public $response;
    /**
     * Constructor method for Delete_DNS_Zone_Container
     * @uses Delete_DNS_Zone_Container::setResponse()
     * @param \SubregSDK\Prod\StructType\Delete_DNS_Zone_Response $response
     */
    public function __construct(\SubregSDK\Prod\StructType\Delete_DNS_Zone_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Prod\StructType\Delete_DNS_Zone_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Prod\StructType\Delete_DNS_Zone_Response $response
     * @return \SubregSDK\Prod\StructType\Delete_DNS_Zone_Container
     */
    public function setResponse(\SubregSDK\Prod\StructType\Delete_DNS_Zone_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
