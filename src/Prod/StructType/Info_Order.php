<?php

namespace SubregSDK\Prod\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Order StructType
 * @subpackage Structs
 */
class Info_Order extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The order
     * @var int
     */
    public $order;
    /**
     * Constructor method for Info_Order
     * @uses Info_Order::setSsid()
     * @uses Info_Order::setOrder()
     * @param string $ssid
     * @param int $order
     */
    public function __construct($ssid = null, $order = null)
    {
        $this
            ->setSsid($ssid)
            ->setOrder($order);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Prod\StructType\Info_Order
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get order value
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * Set order value
     * @param int $order
     * @return \SubregSDK\Prod\StructType\Info_Order
     */
    public function setOrder($order = null)
    {
        // validation for constraint: int
        if (!is_null($order) && !(is_int($order) || ctype_digit($order))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($order, true), gettype($order)), __LINE__);
        }
        $this->order = $order;
        return $this;
    }
}
