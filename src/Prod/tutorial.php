<?php
/**
 * This file aims to show you how to use this generated package.
 * In addition, the goal is to show which methods are available and the first needed parameter(s)
 * You have to use an associative array such as:
 * - the key must be a constant beginning with WSDL_ from AbstractSoapClientBase class (each generated ServiceType class extends this class)
 * - the value must be the corresponding key value (each option matches a {@link http://www.php.net/manual/en/soapclient.soapclient.php} option)
 * $options = array(
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://subreg.cz/wsdl',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_TRACE => true,
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_LOGIN => 'you_secret_login',
 * \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_PASSWORD => 'you_secret_password',
 * );
 * etc...
 */
require_once __DIR__ . '/vendor/autoload.php';
/**
 * Minimal options
 */
$options = array(
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_URL => 'https://subreg.cz/wsdl',
    \WsdlToPhp\PackageBase\AbstractSoapClientBase::WSDL_CLASSMAP => \SubregSDK\Prod\ClassMap::get(),
);
/**
 * Samples for Login ServiceType
 */
$login = new \SubregSDK\Prod\ServiceType\Login($options);
/**
 * Sample call for Login operation/method
 */
if ($login->Login(new \SubregSDK\Prod\StructType\Login()) !== false) {
    print_r($login->getResult());
} else {
    print_r($login->getLastError());
}
/**
 * Samples for Check ServiceType
 */
$check = new \SubregSDK\Prod\ServiceType\Check($options);
/**
 * Sample call for Check_Domain operation/method
 */
if ($check->Check_Domain(new \SubregSDK\Prod\StructType\Check_Domain()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Sample call for Check_Object operation/method
 */
if ($check->Check_Object(new \SubregSDK\Prod\StructType\Check_Object()) !== false) {
    print_r($check->getResult());
} else {
    print_r($check->getLastError());
}
/**
 * Samples for Info ServiceType
 */
$info = new \SubregSDK\Prod\ServiceType\Info($options);
/**
 * Sample call for Info_Domain operation/method
 */
if ($info->Info_Domain(new \SubregSDK\Prod\StructType\Info_Domain()) !== false) {
    print_r($info->getResult());
} else {
    print_r($info->getLastError());
}
/**
 * Sample call for Info_Domain_CZ operation/method
 */
if ($info->Info_Domain_CZ(new \SubregSDK\Prod\StructType\Info_Domain_CZ()) !== false) {
    print_r($info->getResult());
} else {
    print_r($info->getLastError());
}
/**
 * Sample call for Info_Contact operation/method
 */
if ($info->Info_Contact(new \SubregSDK\Prod\StructType\Info_Contact()) !== false) {
    print_r($info->getResult());
} else {
    print_r($info->getLastError());
}
/**
 * Sample call for Info_Object operation/method
 */
if ($info->Info_Object(new \SubregSDK\Prod\StructType\Info_Object()) !== false) {
    print_r($info->getResult());
} else {
    print_r($info->getLastError());
}
/**
 * Sample call for Info_Order operation/method
 */
if ($info->Info_Order(new \SubregSDK\Prod\StructType\Info_Order()) !== false) {
    print_r($info->getResult());
} else {
    print_r($info->getLastError());
}
/**
 * Samples for Domains ServiceType
 */
$domains = new \SubregSDK\Prod\ServiceType\Domains($options);
/**
 * Sample call for Domains_List operation/method
 */
if ($domains->Domains_List(new \SubregSDK\Prod\StructType\Domains_List()) !== false) {
    print_r($domains->getResult());
} else {
    print_r($domains->getLastError());
}
/**
 * Samples for Set ServiceType
 */
$set = new \SubregSDK\Prod\ServiceType\Set($options);
/**
 * Sample call for Set_Autorenew operation/method
 */
if ($set->Set_Autorenew(new \SubregSDK\Prod\StructType\Set_Autorenew()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Sample call for Set_Prices operation/method
 */
if ($set->Set_Prices(new \SubregSDK\Prod\StructType\Set_Prices()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Sample call for Set_DNS_Zone operation/method
 */
if ($set->Set_DNS_Zone(new \SubregSDK\Prod\StructType\Set_DNS_Zone()) !== false) {
    print_r($set->getResult());
} else {
    print_r($set->getLastError());
}
/**
 * Samples for Create ServiceType
 */
$create = new \SubregSDK\Prod\ServiceType\Create($options);
/**
 * Sample call for Create_Contact operation/method
 */
if ($create->Create_Contact(new \SubregSDK\Prod\StructType\Create_Contact()) !== false) {
    print_r($create->getResult());
} else {
    print_r($create->getLastError());
}
/**
 * Samples for Update ServiceType
 */
$update = new \SubregSDK\Prod\ServiceType\Update($options);
/**
 * Sample call for Update_Contact operation/method
 */
if ($update->Update_Contact(new \SubregSDK\Prod\StructType\Update_Contact()) !== false) {
    print_r($update->getResult());
} else {
    print_r($update->getLastError());
}
/**
 * Samples for Contacts ServiceType
 */
$contacts = new \SubregSDK\Prod\ServiceType\Contacts($options);
/**
 * Sample call for Contacts_List operation/method
 */
if ($contacts->Contacts_List(new \SubregSDK\Prod\StructType\Contacts_List()) !== false) {
    print_r($contacts->getResult());
} else {
    print_r($contacts->getLastError());
}
/**
 * Samples for Make ServiceType
 */
$make = new \SubregSDK\Prod\ServiceType\Make($options);
/**
 * Sample call for Make_Order operation/method
 */
if ($make->Make_Order(new \SubregSDK\Prod\StructType\Make_Order()) !== false) {
    print_r($make->getResult());
} else {
    print_r($make->getLastError());
}
/**
 * Samples for Get ServiceType
 */
$get = new \SubregSDK\Prod\ServiceType\Get($options);
/**
 * Sample call for Get_Credit operation/method
 */
if ($get->Get_Credit(new \SubregSDK\Prod\StructType\Get_Credit()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_Accountings operation/method
 */
if ($get->Get_Accountings(new \SubregSDK\Prod\StructType\Get_Accountings()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_Pricelist operation/method
 */
if ($get->Get_Pricelist(new \SubregSDK\Prod\StructType\Get_Pricelist()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_DNS_Zone operation/method
 */
if ($get->Get_DNS_Zone(new \SubregSDK\Prod\StructType\Get_DNS_Zone()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_Certificate operation/method
 */
if ($get->Get_Certificate(new \SubregSDK\Prod\StructType\Get_Certificate()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_Redirects operation/method
 */
if ($get->Get_Redirects(new \SubregSDK\Prod\StructType\Get_Redirects()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_DNS_Info operation/method
 */
if ($get->Get_DNS_Info(new \SubregSDK\Prod\StructType\Get_DNS_Info()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Sample call for Get_TLD_Info operation/method
 */
if ($get->Get_TLD_Info(new \SubregSDK\Prod\StructType\Get_TLD_Info()) !== false) {
    print_r($get->getResult());
} else {
    print_r($get->getLastError());
}
/**
 * Samples for Client ServiceType
 */
$client = new \SubregSDK\Prod\ServiceType\Client($options);
/**
 * Sample call for Client_Payment operation/method
 */
if ($client->Client_Payment(new \SubregSDK\Prod\StructType\Client_Payment()) !== false) {
    print_r($client->getResult());
} else {
    print_r($client->getLastError());
}
/**
 * Samples for Order ServiceType
 */
$order = new \SubregSDK\Prod\ServiceType\Order($options);
/**
 * Sample call for Order_Payment operation/method
 */
if ($order->Order_Payment(new \SubregSDK\Prod\StructType\Order_Payment()) !== false) {
    print_r($order->getResult());
} else {
    print_r($order->getLastError());
}
/**
 * Samples for Credit ServiceType
 */
$credit = new \SubregSDK\Prod\ServiceType\Credit($options);
/**
 * Sample call for Credit_Correction operation/method
 */
if ($credit->Credit_Correction(new \SubregSDK\Prod\StructType\Credit_Correction()) !== false) {
    print_r($credit->getResult());
} else {
    print_r($credit->getLastError());
}
/**
 * Samples for Pricelist ServiceType
 */
$pricelist = new \SubregSDK\Prod\ServiceType\Pricelist($options);
/**
 * Sample call for Pricelist operation/method
 */
if ($pricelist->Pricelist(new \SubregSDK\Prod\StructType\Pricelist()) !== false) {
    print_r($pricelist->getResult());
} else {
    print_r($pricelist->getLastError());
}
/**
 * Samples for Prices ServiceType
 */
$prices = new \SubregSDK\Prod\ServiceType\Prices($options);
/**
 * Sample call for Prices operation/method
 */
if ($prices->Prices(new \SubregSDK\Prod\StructType\Prices()) !== false) {
    print_r($prices->getResult());
} else {
    print_r($prices->getLastError());
}
/**
 * Samples for Download ServiceType
 */
$download = new \SubregSDK\Prod\ServiceType\Download($options);
/**
 * Sample call for Download_Document operation/method
 */
if ($download->Download_Document(new \SubregSDK\Prod\StructType\Download_Document()) !== false) {
    print_r($download->getResult());
} else {
    print_r($download->getLastError());
}
/**
 * Samples for Upload ServiceType
 */
$upload = new \SubregSDK\Prod\ServiceType\Upload($options);
/**
 * Sample call for Upload_Document operation/method
 */
if ($upload->Upload_Document(new \SubregSDK\Prod\StructType\Upload_Document()) !== false) {
    print_r($upload->getResult());
} else {
    print_r($upload->getLastError());
}
/**
 * Samples for List ServiceType
 */
$list = new \SubregSDK\Prod\ServiceType\_List($options);
/**
 * Sample call for List_Documents operation/method
 */
if ($list->List_Documents(new \SubregSDK\Prod\StructType\List_Documents()) !== false) {
    print_r($list->getResult());
} else {
    print_r($list->getLastError());
}
/**
 * Samples for Users ServiceType
 */
$users = new \SubregSDK\Prod\ServiceType\Users($options);
/**
 * Sample call for Users_List operation/method
 */
if ($users->Users_List(new \SubregSDK\Prod\StructType\Users_List()) !== false) {
    print_r($users->getResult());
} else {
    print_r($users->getLastError());
}
/**
 * Samples for Anycast ServiceType
 */
$anycast = new \SubregSDK\Prod\ServiceType\Anycast($options);
/**
 * Sample call for Anycast_ADD_Zone operation/method
 */
if ($anycast->Anycast_ADD_Zone(new \SubregSDK\Prod\StructType\Anycast_ADD_Zone()) !== false) {
    print_r($anycast->getResult());
} else {
    print_r($anycast->getLastError());
}
/**
 * Sample call for Anycast_Remove_Zone operation/method
 */
if ($anycast->Anycast_Remove_Zone(new \SubregSDK\Prod\StructType\Anycast_Remove_Zone()) !== false) {
    print_r($anycast->getResult());
} else {
    print_r($anycast->getLastError());
}
/**
 * Sample call for Anycast_List_Domains operation/method
 */
if ($anycast->Anycast_List_Domains(new \SubregSDK\Prod\StructType\Anycast_List_Domains()) !== false) {
    print_r($anycast->getResult());
} else {
    print_r($anycast->getLastError());
}
/**
 * Samples for Add ServiceType
 */
$add = new \SubregSDK\Prod\ServiceType\Add($options);
/**
 * Sample call for Add_DNS_Zone operation/method
 */
if ($add->Add_DNS_Zone(new \SubregSDK\Prod\StructType\Add_DNS_Zone()) !== false) {
    print_r($add->getResult());
} else {
    print_r($add->getLastError());
}
/**
 * Sample call for Add_DNS_Record operation/method
 */
if ($add->Add_DNS_Record(new \SubregSDK\Prod\StructType\Add_DNS_Record()) !== false) {
    print_r($add->getResult());
} else {
    print_r($add->getLastError());
}
/**
 * Samples for Delete ServiceType
 */
$delete = new \SubregSDK\Prod\ServiceType\Delete($options);
/**
 * Sample call for Delete_DNS_Zone operation/method
 */
if ($delete->Delete_DNS_Zone(new \SubregSDK\Prod\StructType\Delete_DNS_Zone()) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Sample call for Delete_DNS_Record operation/method
 */
if ($delete->Delete_DNS_Record(new \SubregSDK\Prod\StructType\Delete_DNS_Record()) !== false) {
    print_r($delete->getResult());
} else {
    print_r($delete->getLastError());
}
/**
 * Samples for Modify ServiceType
 */
$modify = new \SubregSDK\Prod\ServiceType\Modify($options);
/**
 * Sample call for Modify_DNS_Record operation/method
 */
if ($modify->Modify_DNS_Record(new \SubregSDK\Prod\StructType\Modify_DNS_Record()) !== false) {
    print_r($modify->getResult());
} else {
    print_r($modify->getLastError());
}
/**
 * Samples for POLLG ServiceType
 */
$pOLLG = new \SubregSDK\Prod\ServiceType\POLLG($options);
/**
 * Sample call for POLL_Get operation/method
 */
if ($pOLLG->POLL_Get(new \SubregSDK\Prod\StructType\POLL_Get()) !== false) {
    print_r($pOLLG->getResult());
} else {
    print_r($pOLLG->getLastError());
}
/**
 * Samples for POLLA ServiceType
 */
$pOLLA = new \SubregSDK\Prod\ServiceType\POLLA($options);
/**
 * Sample call for POLL_Ack operation/method
 */
if ($pOLLA->POLL_Ack(new \SubregSDK\Prod\StructType\POLL_Ack()) !== false) {
    print_r($pOLLA->getResult());
} else {
    print_r($pOLLA->getLastError());
}
/**
 * Samples for OIBS ServiceType
 */
$oIBS = new \SubregSDK\Prod\ServiceType\OIBS($options);
/**
 * Sample call for OIB_Search operation/method
 */
if ($oIBS->OIB_Search(new \SubregSDK\Prod\StructType\OIB_Search()) !== false) {
    print_r($oIBS->getResult());
} else {
    print_r($oIBS->getLastError());
}
/**
 * Samples for In ServiceType
 */
$in = new \SubregSDK\Prod\ServiceType\In($options);
/**
 * Sample call for In_Subreg operation/method
 */
if ($in->In_Subreg(new \SubregSDK\Prod\StructType\In_Subreg()) !== false) {
    print_r($in->getResult());
} else {
    print_r($in->getLastError());
}
/**
 * Samples for Sign ServiceType
 */
$sign = new \SubregSDK\Prod\ServiceType\Sign($options);
/**
 * Sample call for Sign_DNS_Zone operation/method
 */
if ($sign->Sign_DNS_Zone(new \SubregSDK\Prod\StructType\Sign_DNS_Zone()) !== false) {
    print_r($sign->getResult());
} else {
    print_r($sign->getLastError());
}
/**
 * Samples for Unsign ServiceType
 */
$unsign = new \SubregSDK\Prod\ServiceType\Unsign($options);
/**
 * Sample call for Unsign_DNS_Zone operation/method
 */
if ($unsign->Unsign_DNS_Zone(new \SubregSDK\Prod\StructType\Unsign_DNS_Zone()) !== false) {
    print_r($unsign->getResult());
} else {
    print_r($unsign->getLastError());
}
/**
 * Samples for Special ServiceType
 */
$special = new \SubregSDK\Prod\ServiceType\Special($options);
/**
 * Sample call for Special_Pricelist operation/method
 */
if ($special->Special_Pricelist(new \SubregSDK\Prod\StructType\Special_Pricelist()) !== false) {
    print_r($special->getResult());
} else {
    print_r($special->getLastError());
}
