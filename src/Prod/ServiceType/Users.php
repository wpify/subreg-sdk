<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Users ServiceType
 * @subpackage Services
 */
class Users extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Users_List
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Users_List $parameters
     * @return \SubregSDK\Prod\StructType\Users_List_Container|bool
     */
    public function Users_List(\SubregSDK\Prod\StructType\Users_List $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Users_List', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Users_List_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
