<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Unsign ServiceType
 * @subpackage Services
 */
class Unsign extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Unsign_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Unsign_DNS_Zone $parameters
     * @return \SubregSDK\Prod\StructType\Unsign_DNS_Zone_Container|bool
     */
    public function Unsign_DNS_Zone(\SubregSDK\Prod\StructType\Unsign_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Unsign_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Unsign_DNS_Zone_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
