<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for POLLG ServiceType
 * @subpackage Services
 */
class POLLG extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named POLL_Get
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\POLL_Get $parameters
     * @return \SubregSDK\Prod\StructType\POLL_Get_Container|bool
     */
    public function POLL_Get(\SubregSDK\Prod\StructType\POLL_Get $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('POLL_Get', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\POLL_Get_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
