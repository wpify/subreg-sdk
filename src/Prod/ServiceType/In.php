<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for In ServiceType
 * @subpackage Services
 */
class In extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named In_Subreg
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\In_Subreg $parameters
     * @return \SubregSDK\Prod\StructType\In_Subreg_Container|bool
     */
    public function In_Subreg(\SubregSDK\Prod\StructType\In_Subreg $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('In_Subreg', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\In_Subreg_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
