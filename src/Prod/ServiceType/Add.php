<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Add ServiceType
 * @subpackage Services
 */
class Add extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Add_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Add_DNS_Zone $parameters
     * @return \SubregSDK\Prod\StructType\Add_DNS_Zone_Container|bool
     */
    public function Add_DNS_Zone(\SubregSDK\Prod\StructType\Add_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Add_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Add_DNS_Record
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Add_DNS_Record $parameters
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Container|bool
     */
    public function Add_DNS_Record(\SubregSDK\Prod\StructType\Add_DNS_Record $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Add_DNS_Record', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Add_DNS_Record_Container|\SubregSDK\Prod\StructType\Add_DNS_Zone_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
