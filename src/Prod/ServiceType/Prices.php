<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Prices ServiceType
 * @subpackage Services
 */
class Prices extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Prices
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Prices $parameters
     * @return \SubregSDK\Prod\StructType\Prices_Container|bool
     */
    public function Prices(\SubregSDK\Prod\StructType\Prices $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Prices', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Prices_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
