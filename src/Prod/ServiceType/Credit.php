<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Credit ServiceType
 * @subpackage Services
 */
class Credit extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Credit_Correction
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Credit_Correction $parameters
     * @return \SubregSDK\Prod\StructType\Credit_Correction_Container|bool
     */
    public function Credit_Correction(\SubregSDK\Prod\StructType\Credit_Correction $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Credit_Correction', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Credit_Correction_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
