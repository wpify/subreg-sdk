<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Get ServiceType
 * @subpackage Services
 */
class Get extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Get_Credit
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_Credit $parameters
     * @return \SubregSDK\Prod\StructType\Get_Credit_Container|bool
     */
    public function Get_Credit(\SubregSDK\Prod\StructType\Get_Credit $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_Credit', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_Accountings
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_Accountings $parameters
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Container|bool
     */
    public function Get_Accountings(\SubregSDK\Prod\StructType\Get_Accountings $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_Accountings', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_Pricelist
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_Pricelist $parameters
     * @return \SubregSDK\Prod\StructType\Get_Pricelist_Container|bool
     */
    public function Get_Pricelist(\SubregSDK\Prod\StructType\Get_Pricelist $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_Pricelist', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_DNS_Zone $parameters
     * @return \SubregSDK\Prod\StructType\Get_DNS_Zone_Container|bool
     */
    public function Get_DNS_Zone(\SubregSDK\Prod\StructType\Get_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_Certificate
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_Certificate $parameters
     * @return \SubregSDK\Prod\StructType\Get_Certificate_Container|bool
     */
    public function Get_Certificate(\SubregSDK\Prod\StructType\Get_Certificate $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_Certificate', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_Redirects
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_Redirects $parameters
     * @return \SubregSDK\Prod\StructType\Get_Redirects_Container|bool
     */
    public function Get_Redirects(\SubregSDK\Prod\StructType\Get_Redirects $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_Redirects', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_DNS_Info
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_DNS_Info $parameters
     * @return \SubregSDK\Prod\StructType\Get_DNS_Info_Container|bool
     */
    public function Get_DNS_Info(\SubregSDK\Prod\StructType\Get_DNS_Info $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_DNS_Info', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Get_TLD_Info
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Get_TLD_Info $parameters
     * @return \SubregSDK\Prod\StructType\Get_TLD_Info_Container|bool
     */
    public function Get_TLD_Info(\SubregSDK\Prod\StructType\Get_TLD_Info $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Get_TLD_Info', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Get_Accountings_Container|\SubregSDK\Prod\StructType\Get_Certificate_Container|\SubregSDK\Prod\StructType\Get_Credit_Container|\SubregSDK\Prod\StructType\Get_DNS_Info_Container|\SubregSDK\Prod\StructType\Get_DNS_Zone_Container|\SubregSDK\Prod\StructType\Get_Pricelist_Container|\SubregSDK\Prod\StructType\Get_Redirects_Container|\SubregSDK\Prod\StructType\Get_TLD_Info_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
