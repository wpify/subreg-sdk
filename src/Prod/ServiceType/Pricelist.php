<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Pricelist ServiceType
 * @subpackage Services
 */
class Pricelist extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Pricelist
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Pricelist $parameters
     * @return \SubregSDK\Prod\StructType\Pricelist_Container|bool
     */
    public function Pricelist(\SubregSDK\Prod\StructType\Pricelist $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Pricelist', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Pricelist_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
