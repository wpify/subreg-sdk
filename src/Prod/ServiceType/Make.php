<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Make ServiceType
 * @subpackage Services
 */
class Make extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Make_Order
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Make_Order $parameters
     * @return \SubregSDK\Prod\StructType\Make_Order_Container|bool
     */
    public function Make_Order(\SubregSDK\Prod\StructType\Make_Order $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Make_Order', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Make_Order_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
