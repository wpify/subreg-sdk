<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Client ServiceType
 * @subpackage Services
 */
class Client extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Client_Payment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Client_Payment $parameters
     * @return \SubregSDK\Prod\StructType\Client_Payment_Container|bool
     */
    public function Client_Payment(\SubregSDK\Prod\StructType\Client_Payment $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Client_Payment', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Client_Payment_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
