<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Special ServiceType
 * @subpackage Services
 */
class Special extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Special_Pricelist
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Special_Pricelist $parameters
     * @return \SubregSDK\Prod\StructType\Special_Pricelist_Container|bool
     */
    public function Special_Pricelist(\SubregSDK\Prod\StructType\Special_Pricelist $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Special_Pricelist', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Special_Pricelist_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
