<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for POLLA ServiceType
 * @subpackage Services
 */
class POLLA extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named POLL_Ack
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\POLL_Ack $parameters
     * @return \SubregSDK\Prod\StructType\POLL_Ack_Container|bool
     */
    public function POLL_Ack(\SubregSDK\Prod\StructType\POLL_Ack $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('POLL_Ack', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\POLL_Ack_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
