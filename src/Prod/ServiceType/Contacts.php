<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Contacts ServiceType
 * @subpackage Services
 */
class Contacts extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Contacts_List
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\Contacts_List $parameters
     * @return \SubregSDK\Prod\StructType\Contacts_List_Container|bool
     */
    public function Contacts_List(\SubregSDK\Prod\StructType\Contacts_List $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Contacts_List', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\Contacts_List_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
