<?php

namespace SubregSDK\Prod\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for List ServiceType
 * @subpackage Services
 */
class _List extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named List_Documents
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Prod\StructType\List_Documents $parameters
     * @return \SubregSDK\Prod\StructType\List_Documents_Container|bool
     */
    public function List_Documents(\SubregSDK\Prod\StructType\List_Documents $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('List_Documents', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Prod\StructType\List_Documents_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
