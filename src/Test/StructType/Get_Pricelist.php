<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Pricelist StructType
 * @subpackage Structs
 */
class Get_Pricelist extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The pricelist
     * @var string
     */
    public $pricelist;
    /**
     * Constructor method for Get_Pricelist
     * @uses Get_Pricelist::setSsid()
     * @uses Get_Pricelist::setPricelist()
     * @param string $ssid
     * @param string $pricelist
     */
    public function __construct($ssid = null, $pricelist = null)
    {
        $this
            ->setSsid($ssid)
            ->setPricelist($pricelist);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Get_Pricelist
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get pricelist value
     * @return string|null
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }
    /**
     * Set pricelist value
     * @param string $pricelist
     * @return \SubregSDK\Test\StructType\Get_Pricelist
     */
    public function setPricelist($pricelist = null)
    {
        // validation for constraint: string
        if (!is_null($pricelist) && !is_string($pricelist)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pricelist, true), gettype($pricelist)), __LINE__);
        }
        $this->pricelist = $pricelist;
        return $this;
    }
}
