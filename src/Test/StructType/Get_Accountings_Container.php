<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Accountings_Container StructType
 * @subpackage Structs
 */
class Get_Accountings_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Get_Accountings_Response
     */
    public $response;
    /**
     * Constructor method for Get_Accountings_Container
     * @uses Get_Accountings_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Get_Accountings_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Get_Accountings_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Get_Accountings_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Get_Accountings_Response $response
     * @return \SubregSDK\Test\StructType\Get_Accountings_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Get_Accountings_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
