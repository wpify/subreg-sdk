<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Record StructType
 * @subpackage Structs
 */
class Make_Order_Record extends AbstractStructBase
{
    /**
     * The tag
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $tag;
    /**
     * The flag
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $flag;
    /**
     * The protocol
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $protocol;
    /**
     * The alg
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $alg;
    /**
     * The key
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $key;
    /**
     * Constructor method for Make_Order_Record
     * @uses Make_Order_Record::setTag()
     * @uses Make_Order_Record::setFlag()
     * @uses Make_Order_Record::setProtocol()
     * @uses Make_Order_Record::setAlg()
     * @uses Make_Order_Record::setKey()
     * @param string $tag
     * @param string $flag
     * @param string $protocol
     * @param string $alg
     * @param string $key
     */
    public function __construct($tag = null, $flag = null, $protocol = null, $alg = null, $key = null)
    {
        $this
            ->setTag($tag)
            ->setFlag($flag)
            ->setProtocol($protocol)
            ->setAlg($alg)
            ->setKey($key);
    }
    /**
     * Get tag value
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }
    /**
     * Set tag value
     * @param string $tag
     * @return \SubregSDK\Test\StructType\Make_Order_Record
     */
    public function setTag($tag = null)
    {
        // validation for constraint: string
        if (!is_null($tag) && !is_string($tag)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tag, true), gettype($tag)), __LINE__);
        }
        $this->tag = $tag;
        return $this;
    }
    /**
     * Get flag value
     * @return string|null
     */
    public function getFlag()
    {
        return $this->flag;
    }
    /**
     * Set flag value
     * @param string $flag
     * @return \SubregSDK\Test\StructType\Make_Order_Record
     */
    public function setFlag($flag = null)
    {
        // validation for constraint: string
        if (!is_null($flag) && !is_string($flag)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($flag, true), gettype($flag)), __LINE__);
        }
        $this->flag = $flag;
        return $this;
    }
    /**
     * Get protocol value
     * @return string|null
     */
    public function getProtocol()
    {
        return $this->protocol;
    }
    /**
     * Set protocol value
     * @param string $protocol
     * @return \SubregSDK\Test\StructType\Make_Order_Record
     */
    public function setProtocol($protocol = null)
    {
        // validation for constraint: string
        if (!is_null($protocol) && !is_string($protocol)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($protocol, true), gettype($protocol)), __LINE__);
        }
        $this->protocol = $protocol;
        return $this;
    }
    /**
     * Get alg value
     * @return string|null
     */
    public function getAlg()
    {
        return $this->alg;
    }
    /**
     * Set alg value
     * @param string $alg
     * @return \SubregSDK\Test\StructType\Make_Order_Record
     */
    public function setAlg($alg = null)
    {
        // validation for constraint: string
        if (!is_null($alg) && !is_string($alg)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($alg, true), gettype($alg)), __LINE__);
        }
        $this->alg = $alg;
        return $this;
    }
    /**
     * Get key value
     * @return string|null
     */
    public function getKey()
    {
        return $this->key;
    }
    /**
     * Set key value
     * @param string $key
     * @return \SubregSDK\Test\StructType\Make_Order_Record
     */
    public function setKey($key = null)
    {
        // validation for constraint: string
        if (!is_null($key) && !is_string($key)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($key, true), gettype($key)), __LINE__);
        }
        $this->key = $key;
        return $this;
    }
}
