<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Pricelist_Value StructType
 * @subpackage Structs
 */
class Pricelist_Value extends AbstractStructBase
{
    /**
     * The value
     * @var string
     */
    public $value;
    /**
     * The description
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $description;
    /**
     * Constructor method for Pricelist_Value
     * @uses Pricelist_Value::setValue()
     * @uses Pricelist_Value::setDescription()
     * @param string $value
     * @param string $description
     */
    public function __construct($value = null, $description = null)
    {
        $this
            ->setValue($value)
            ->setDescription($description);
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \SubregSDK\Test\StructType\Pricelist_Value
     */
    public function setValue($value = null)
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        return $this;
    }
    /**
     * Get description value
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Set description value
     * @param string $description
     * @return \SubregSDK\Test\StructType\Pricelist_Value
     */
    public function setDescription($description = null)
    {
        // validation for constraint: string
        if (!is_null($description) && !is_string($description)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($description, true), gettype($description)), __LINE__);
        }
        $this->description = $description;
        return $this;
    }
}
