<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_DNS_Info_Anydata StructType
 * @subpackage Structs
 */
class Get_DNS_Info_Anydata extends AbstractStructBase
{
    /**
     * The type
     * @var string
     */
    public $type;
    /**
     * The data
     * @var string
     */
    public $data;
    /**
     * Constructor method for Get_DNS_Info_Anydata
     * @uses Get_DNS_Info_Anydata::setType()
     * @uses Get_DNS_Info_Anydata::setData()
     * @param string $type
     * @param string $data
     */
    public function __construct($type = null, $data = null)
    {
        $this
            ->setType($type)
            ->setData($data);
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Anydata
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get data value
     * @return string|null
     */
    public function getData()
    {
        return $this->data;
    }
    /**
     * Set data value
     * @param string $data
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Anydata
     */
    public function setData($data = null)
    {
        // validation for constraint: string
        if (!is_null($data) && !is_string($data)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($data, true), gettype($data)), __LINE__);
        }
        $this->data = $data;
        return $this;
    }
}
