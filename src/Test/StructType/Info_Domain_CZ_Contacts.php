<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Domain_CZ_Contacts StructType
 * @subpackage Structs
 */
class Info_Domain_CZ_Contacts extends AbstractStructBase
{
    /**
     * The admin
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]
     */
    public $admin;
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]
     */
    public $tech;
    /**
     * The bill
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]
     */
    public $bill;
    /**
     * Constructor method for Info_Domain_CZ_Contacts
     * @uses Info_Domain_CZ_Contacts::setAdmin()
     * @uses Info_Domain_CZ_Contacts::setTech()
     * @uses Info_Domain_CZ_Contacts::setBill()
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $admin
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $tech
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $bill
     */
    public function __construct(array $admin = array(), array $tech = array(), array $bill = array())
    {
        $this
            ->setAdmin($admin)
            ->setTech($tech)
            ->setBill($bill);
    }
    /**
     * Get admin value
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]|null
     */
    public function getAdmin()
    {
        return $this->admin;
    }
    /**
     * This method is responsible for validating the values passed to the setAdmin method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAdmin method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAdminForArrayConstraintsFromSetAdmin(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_CZ_ContactsAdminItem) {
            // validation for constraint: itemType
            if (!$info_Domain_CZ_ContactsAdminItem instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
                $invalidValues[] = is_object($info_Domain_CZ_ContactsAdminItem) ? get_class($info_Domain_CZ_ContactsAdminItem) : sprintf('%s(%s)', gettype($info_Domain_CZ_ContactsAdminItem), var_export($info_Domain_CZ_ContactsAdminItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The admin property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set admin value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $admin
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function setAdmin(array $admin = array())
    {
        // validation for constraint: array
        if ('' !== ($adminArrayErrorMessage = self::validateAdminForArrayConstraintsFromSetAdmin($admin))) {
            throw new \InvalidArgumentException($adminArrayErrorMessage, __LINE__);
        }
        $this->admin = $admin;
        return $this;
    }
    /**
     * Add item to admin value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function addToAdmin(\SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
            throw new \InvalidArgumentException(sprintf('The admin property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->admin[] = $item;
        return $this;
    }
    /**
     * Get tech value
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * This method is responsible for validating the values passed to the setTech method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTech method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTechForArrayConstraintsFromSetTech(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_CZ_ContactsTechItem) {
            // validation for constraint: itemType
            if (!$info_Domain_CZ_ContactsTechItem instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
                $invalidValues[] = is_object($info_Domain_CZ_ContactsTechItem) ? get_class($info_Domain_CZ_ContactsTechItem) : sprintf('%s(%s)', gettype($info_Domain_CZ_ContactsTechItem), var_export($info_Domain_CZ_ContactsTechItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The tech property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set tech value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $tech
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function setTech(array $tech = array())
    {
        // validation for constraint: array
        if ('' !== ($techArrayErrorMessage = self::validateTechForArrayConstraintsFromSetTech($tech))) {
            throw new \InvalidArgumentException($techArrayErrorMessage, __LINE__);
        }
        $this->tech = $tech;
        return $this;
    }
    /**
     * Add item to tech value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function addToTech(\SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
            throw new \InvalidArgumentException(sprintf('The tech property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->tech[] = $item;
        return $this;
    }
    /**
     * Get bill value
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[]|null
     */
    public function getBill()
    {
        return $this->bill;
    }
    /**
     * This method is responsible for validating the values passed to the setBill method
     * This method is willingly generated in order to preserve the one-line inline validation within the setBill method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateBillForArrayConstraintsFromSetBill(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_CZ_ContactsBillItem) {
            // validation for constraint: itemType
            if (!$info_Domain_CZ_ContactsBillItem instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
                $invalidValues[] = is_object($info_Domain_CZ_ContactsBillItem) ? get_class($info_Domain_CZ_ContactsBillItem) : sprintf('%s(%s)', gettype($info_Domain_CZ_ContactsBillItem), var_export($info_Domain_CZ_ContactsBillItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The bill property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set bill value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact[] $bill
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function setBill(array $bill = array())
    {
        // validation for constraint: array
        if ('' !== ($billArrayErrorMessage = self::validateBillForArrayConstraintsFromSetBill($bill))) {
            throw new \InvalidArgumentException($billArrayErrorMessage, __LINE__);
        }
        $this->bill = $bill;
        return $this;
    }
    /**
     * Add item to bill value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Contacts
     */
    public function addToBill(\SubregSDK\Test\StructType\Info_Domain_CZ_Contact $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Domain_CZ_Contact) {
            throw new \InvalidArgumentException(sprintf('The bill property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_CZ_Contact, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->bill[] = $item;
        return $this;
    }
}
