<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Autorenew StructType
 * @subpackage Structs
 */
class Set_Autorenew extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The autorenew
     * @var string
     */
    public $autorenew;
    /**
     * Constructor method for Set_Autorenew
     * @uses Set_Autorenew::setSsid()
     * @uses Set_Autorenew::setDomain()
     * @uses Set_Autorenew::setAutorenew()
     * @param string $ssid
     * @param string $domain
     * @param string $autorenew
     */
    public function __construct($ssid = null, $domain = null, $autorenew = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setAutorenew($autorenew);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Set_Autorenew
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Test\StructType\Set_Autorenew
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get autorenew value
     * @return string|null
     */
    public function getAutorenew()
    {
        return $this->autorenew;
    }
    /**
     * Set autorenew value
     * @param string $autorenew
     * @return \SubregSDK\Test\StructType\Set_Autorenew
     */
    public function setAutorenew($autorenew = null)
    {
        // validation for constraint: string
        if (!is_null($autorenew) && !is_string($autorenew)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($autorenew, true), gettype($autorenew)), __LINE__);
        }
        $this->autorenew = $autorenew;
        return $this;
    }
}
