<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Error_Info StructType
 * @subpackage Structs
 */
class Error_Info extends AbstractStructBase
{
    /**
     * The errormsg
     * @var string
     */
    public $errormsg;
    /**
     * The errorcode
     * @var \SubregSDK\Test\StructType\Error_Codes
     */
    public $errorcode;
    /**
     * Constructor method for Error_Info
     * @uses Error_Info::setErrormsg()
     * @uses Error_Info::setErrorcode()
     * @param string $errormsg
     * @param \SubregSDK\Test\StructType\Error_Codes $errorcode
     */
    public function __construct($errormsg = null, \SubregSDK\Test\StructType\Error_Codes $errorcode = null)
    {
        $this
            ->setErrormsg($errormsg)
            ->setErrorcode($errorcode);
    }
    /**
     * Get errormsg value
     * @return string|null
     */
    public function getErrormsg()
    {
        return $this->errormsg;
    }
    /**
     * Set errormsg value
     * @param string $errormsg
     * @return \SubregSDK\Test\StructType\Error_Info
     */
    public function setErrormsg($errormsg = null)
    {
        // validation for constraint: string
        if (!is_null($errormsg) && !is_string($errormsg)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errormsg, true), gettype($errormsg)), __LINE__);
        }
        $this->errormsg = $errormsg;
        return $this;
    }
    /**
     * Get errorcode value
     * @return \SubregSDK\Test\StructType\Error_Codes|null
     */
    public function getErrorcode()
    {
        return $this->errorcode;
    }
    /**
     * Set errorcode value
     * @param \SubregSDK\Test\StructType\Error_Codes $errorcode
     * @return \SubregSDK\Test\StructType\Error_Info
     */
    public function setErrorcode(\SubregSDK\Test\StructType\Error_Codes $errorcode = null)
    {
        $this->errorcode = $errorcode;
        return $this;
    }
}
