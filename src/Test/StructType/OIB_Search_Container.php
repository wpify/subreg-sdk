<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OIB_Search_Container StructType
 * @subpackage Structs
 */
class OIB_Search_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\OIB_Search_Response
     */
    public $response;
    /**
     * Constructor method for OIB_Search_Container
     * @uses OIB_Search_Container::setResponse()
     * @param \SubregSDK\Test\StructType\OIB_Search_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\OIB_Search_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\OIB_Search_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\OIB_Search_Response $response
     * @return \SubregSDK\Test\StructType\OIB_Search_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\OIB_Search_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
