<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_List_Domains_Container StructType
 * @subpackage Structs
 */
class Anycast_List_Domains_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Anycast_List_Domains_Response
     */
    public $response;
    /**
     * Constructor method for Anycast_List_Domains_Container
     * @uses Anycast_List_Domains_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Anycast_List_Domains_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains_Response $response
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Anycast_List_Domains_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
