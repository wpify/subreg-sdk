<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Error_Codes StructType
 * @subpackage Structs
 */
class Error_Codes extends AbstractStructBase
{
    /**
     * The major
     * @var int
     */
    public $major;
    /**
     * The minor
     * @var int
     */
    public $minor;
    /**
     * Constructor method for Error_Codes
     * @uses Error_Codes::setMajor()
     * @uses Error_Codes::setMinor()
     * @param int $major
     * @param int $minor
     */
    public function __construct($major = null, $minor = null)
    {
        $this
            ->setMajor($major)
            ->setMinor($minor);
    }
    /**
     * Get major value
     * @return int|null
     */
    public function getMajor()
    {
        return $this->major;
    }
    /**
     * Set major value
     * @param int $major
     * @return \SubregSDK\Test\StructType\Error_Codes
     */
    public function setMajor($major = null)
    {
        // validation for constraint: int
        if (!is_null($major) && !(is_int($major) || ctype_digit($major))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($major, true), gettype($major)), __LINE__);
        }
        $this->major = $major;
        return $this;
    }
    /**
     * Get minor value
     * @return int|null
     */
    public function getMinor()
    {
        return $this->minor;
    }
    /**
     * Set minor value
     * @param int $minor
     * @return \SubregSDK\Test\StructType\Error_Codes
     */
    public function setMinor($minor = null)
    {
        // validation for constraint: int
        if (!is_null($minor) && !(is_int($minor) || ctype_digit($minor))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minor, true), gettype($minor)), __LINE__);
        }
        $this->minor = $minor;
        return $this;
    }
}
