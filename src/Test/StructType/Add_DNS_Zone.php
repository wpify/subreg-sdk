<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Add_DNS_Zone StructType
 * @subpackage Structs
 */
class Add_DNS_Zone extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The template
     * @var string
     */
    public $template;
    /**
     * Constructor method for Add_DNS_Zone
     * @uses Add_DNS_Zone::setSsid()
     * @uses Add_DNS_Zone::setDomain()
     * @uses Add_DNS_Zone::setTemplate()
     * @param string $ssid
     * @param string $domain
     * @param string $template
     */
    public function __construct($ssid = null, $domain = null, $template = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setTemplate($template);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Add_DNS_Zone
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Test\StructType\Add_DNS_Zone
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get template value
     * @return string|null
     */
    public function getTemplate()
    {
        return $this->template;
    }
    /**
     * Set template value
     * @param string $template
     * @return \SubregSDK\Test\StructType\Add_DNS_Zone
     */
    public function setTemplate($template = null)
    {
        // validation for constraint: string
        if (!is_null($template) && !is_string($template)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($template, true), gettype($template)), __LINE__);
        }
        $this->template = $template;
        return $this;
    }
}
