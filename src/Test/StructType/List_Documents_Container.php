<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for List_Documents_Container StructType
 * @subpackage Structs
 */
class List_Documents_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\List_Documents_Response
     */
    public $response;
    /**
     * Constructor method for List_Documents_Container
     * @uses List_Documents_Container::setResponse()
     * @param \SubregSDK\Test\StructType\List_Documents_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\List_Documents_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\List_Documents_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\List_Documents_Response $response
     * @return \SubregSDK\Test\StructType\List_Documents_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\List_Documents_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
