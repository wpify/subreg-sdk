<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Order_Payment_Data StructType
 * @subpackage Structs
 */
class Order_Payment_Data extends AbstractStructBase
{
}
