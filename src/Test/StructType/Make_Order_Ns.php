<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Ns StructType
 * @subpackage Structs
 */
class Make_Order_Ns extends AbstractStructBase
{
    /**
     * The hosts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Host[]
     */
    public $hosts;
    /**
     * The nsset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $nsset;
    /**
     * Constructor method for Make_Order_Ns
     * @uses Make_Order_Ns::setHosts()
     * @uses Make_Order_Ns::setNsset()
     * @param \SubregSDK\Test\StructType\Make_Order_Host[] $hosts
     * @param string $nsset
     */
    public function __construct(array $hosts = array(), $nsset = null)
    {
        $this
            ->setHosts($hosts)
            ->setNsset($nsset);
    }
    /**
     * Get hosts value
     * @return \SubregSDK\Test\StructType\Make_Order_Host[]|null
     */
    public function getHosts()
    {
        return $this->hosts;
    }
    /**
     * This method is responsible for validating the values passed to the setHosts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHosts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHostsForArrayConstraintsFromSetHosts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_NsHostsItem) {
            // validation for constraint: itemType
            if (!$make_Order_NsHostsItem instanceof \SubregSDK\Test\StructType\Make_Order_Host) {
                $invalidValues[] = is_object($make_Order_NsHostsItem) ? get_class($make_Order_NsHostsItem) : sprintf('%s(%s)', gettype($make_Order_NsHostsItem), var_export($make_Order_NsHostsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The hosts property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Host, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set hosts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Host[] $hosts
     * @return \SubregSDK\Test\StructType\Make_Order_Ns
     */
    public function setHosts(array $hosts = array())
    {
        // validation for constraint: array
        if ('' !== ($hostsArrayErrorMessage = self::validateHostsForArrayConstraintsFromSetHosts($hosts))) {
            throw new \InvalidArgumentException($hostsArrayErrorMessage, __LINE__);
        }
        $this->hosts = $hosts;
        return $this;
    }
    /**
     * Add item to hosts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Host $item
     * @return \SubregSDK\Test\StructType\Make_Order_Ns
     */
    public function addToHosts(\SubregSDK\Test\StructType\Make_Order_Host $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Make_Order_Host) {
            throw new \InvalidArgumentException(sprintf('The hosts property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Host, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->hosts[] = $item;
        return $this;
    }
    /**
     * Get nsset value
     * @return string|null
     */
    public function getNsset()
    {
        return $this->nsset;
    }
    /**
     * Set nsset value
     * @param string $nsset
     * @return \SubregSDK\Test\StructType\Make_Order_Ns
     */
    public function setNsset($nsset = null)
    {
        // validation for constraint: string
        if (!is_null($nsset) && !is_string($nsset)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nsset, true), gettype($nsset)), __LINE__);
        }
        $this->nsset = $nsset;
        return $this;
    }
}
