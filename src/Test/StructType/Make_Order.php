<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order StructType
 * @subpackage Structs
 */
class Make_Order extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The order
     * @var \SubregSDK\Test\StructType\Make_Order_Order
     */
    public $order;
    /**
     * Constructor method for Make_Order
     * @uses Make_Order::setSsid()
     * @uses Make_Order::setOrder()
     * @param string $ssid
     * @param \SubregSDK\Test\StructType\Make_Order_Order $order
     */
    public function __construct($ssid = null, \SubregSDK\Test\StructType\Make_Order_Order $order = null)
    {
        $this
            ->setSsid($ssid)
            ->setOrder($order);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Make_Order
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get order value
     * @return \SubregSDK\Test\StructType\Make_Order_Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * Set order value
     * @param \SubregSDK\Test\StructType\Make_Order_Order $order
     * @return \SubregSDK\Test\StructType\Make_Order
     */
    public function setOrder(\SubregSDK\Test\StructType\Make_Order_Order $order = null)
    {
        $this->order = $order;
        return $this;
    }
}
