<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Credit_Correction_Container StructType
 * @subpackage Structs
 */
class Credit_Correction_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Credit_Correction_Response
     */
    public $response;
    /**
     * Constructor method for Credit_Correction_Container
     * @uses Credit_Correction_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Credit_Correction_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Credit_Correction_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Credit_Correction_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Credit_Correction_Response $response
     * @return \SubregSDK\Test\StructType\Credit_Correction_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Credit_Correction_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
