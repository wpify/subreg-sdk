<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Redirects_Container StructType
 * @subpackage Structs
 */
class Get_Redirects_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Get_Redirects_Response
     */
    public $response;
    /**
     * Constructor method for Get_Redirects_Container
     * @uses Get_Redirects_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Get_Redirects_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Get_Redirects_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Get_Redirects_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Get_Redirects_Response $response
     * @return \SubregSDK\Test\StructType\Get_Redirects_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Get_Redirects_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
