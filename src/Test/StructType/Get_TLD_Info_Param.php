<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_TLD_Info_Param StructType
 * @subpackage Structs
 */
class Get_TLD_Info_Param extends AbstractStructBase
{
    /**
     * The param
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $param;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $name;
    /**
     * The desc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $desc;
    /**
     * The required
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $required;
    /**
     * The options
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Get_TLD_Info_Option[]
     */
    public $options;
    /**
     * Constructor method for Get_TLD_Info_Param
     * @uses Get_TLD_Info_Param::setParam()
     * @uses Get_TLD_Info_Param::setName()
     * @uses Get_TLD_Info_Param::setDesc()
     * @uses Get_TLD_Info_Param::setRequired()
     * @uses Get_TLD_Info_Param::setOptions()
     * @param string $param
     * @param string $name
     * @param string $desc
     * @param string $required
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Option[] $options
     */
    public function __construct($param = null, $name = null, $desc = null, $required = null, array $options = array())
    {
        $this
            ->setParam($param)
            ->setName($name)
            ->setDesc($desc)
            ->setRequired($required)
            ->setOptions($options);
    }
    /**
     * Get param value
     * @return string|null
     */
    public function getParam()
    {
        return $this->param;
    }
    /**
     * Set param value
     * @param string $param
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function setParam($param = null)
    {
        // validation for constraint: string
        if (!is_null($param) && !is_string($param)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($param, true), gettype($param)), __LINE__);
        }
        $this->param = $param;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get desc value
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }
    /**
     * Set desc value
     * @param string $desc
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function setDesc($desc = null)
    {
        // validation for constraint: string
        if (!is_null($desc) && !is_string($desc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($desc, true), gettype($desc)), __LINE__);
        }
        $this->desc = $desc;
        return $this;
    }
    /**
     * Get required value
     * @return string|null
     */
    public function getRequired()
    {
        return $this->required;
    }
    /**
     * Set required value
     * @param string $required
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function setRequired($required = null)
    {
        // validation for constraint: string
        if (!is_null($required) && !is_string($required)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($required, true), gettype($required)), __LINE__);
        }
        $this->required = $required;
        return $this;
    }
    /**
     * Get options value
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Option[]|null
     */
    public function getOptions()
    {
        return $this->options;
    }
    /**
     * This method is responsible for validating the values passed to the setOptions method
     * This method is willingly generated in order to preserve the one-line inline validation within the setOptions method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateOptionsForArrayConstraintsFromSetOptions(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_TLD_Info_ParamOptionsItem) {
            // validation for constraint: itemType
            if (!$get_TLD_Info_ParamOptionsItem instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Option) {
                $invalidValues[] = is_object($get_TLD_Info_ParamOptionsItem) ? get_class($get_TLD_Info_ParamOptionsItem) : sprintf('%s(%s)', gettype($get_TLD_Info_ParamOptionsItem), var_export($get_TLD_Info_ParamOptionsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The options property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Option, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set options value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Option[] $options
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function setOptions(array $options = array())
    {
        // validation for constraint: array
        if ('' !== ($optionsArrayErrorMessage = self::validateOptionsForArrayConstraintsFromSetOptions($options))) {
            throw new \InvalidArgumentException($optionsArrayErrorMessage, __LINE__);
        }
        $this->options = $options;
        return $this;
    }
    /**
     * Add item to options value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Option $item
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param
     */
    public function addToOptions(\SubregSDK\Test\StructType\Get_TLD_Info_Option $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Option) {
            throw new \InvalidArgumentException(sprintf('The options property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Option, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->options[] = $item;
        return $this;
    }
}
