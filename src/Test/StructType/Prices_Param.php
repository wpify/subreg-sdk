<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Prices_Param StructType
 * @subpackage Structs
 */
class Prices_Param extends AbstractStructBase
{
    /**
     * The param
     * @var string
     */
    public $param;
    /**
     * The desc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $desc;
    /**
     * The required
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $required;
    /**
     * The error_code
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $error_code;
    /**
     * The values
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Prices_Value[]
     */
    public $values;
    /**
     * Constructor method for Prices_Param
     * @uses Prices_Param::setParam()
     * @uses Prices_Param::setDesc()
     * @uses Prices_Param::setRequired()
     * @uses Prices_Param::setError_code()
     * @uses Prices_Param::setValues()
     * @param string $param
     * @param string $desc
     * @param int $required
     * @param int $error_code
     * @param \SubregSDK\Test\StructType\Prices_Value[] $values
     */
    public function __construct($param = null, $desc = null, $required = null, $error_code = null, array $values = array())
    {
        $this
            ->setParam($param)
            ->setDesc($desc)
            ->setRequired($required)
            ->setError_code($error_code)
            ->setValues($values);
    }
    /**
     * Get param value
     * @return string|null
     */
    public function getParam()
    {
        return $this->param;
    }
    /**
     * Set param value
     * @param string $param
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function setParam($param = null)
    {
        // validation for constraint: string
        if (!is_null($param) && !is_string($param)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($param, true), gettype($param)), __LINE__);
        }
        $this->param = $param;
        return $this;
    }
    /**
     * Get desc value
     * @return string|null
     */
    public function getDesc()
    {
        return $this->desc;
    }
    /**
     * Set desc value
     * @param string $desc
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function setDesc($desc = null)
    {
        // validation for constraint: string
        if (!is_null($desc) && !is_string($desc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($desc, true), gettype($desc)), __LINE__);
        }
        $this->desc = $desc;
        return $this;
    }
    /**
     * Get required value
     * @return int|null
     */
    public function getRequired()
    {
        return $this->required;
    }
    /**
     * Set required value
     * @param int $required
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function setRequired($required = null)
    {
        // validation for constraint: int
        if (!is_null($required) && !(is_int($required) || ctype_digit($required))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($required, true), gettype($required)), __LINE__);
        }
        $this->required = $required;
        return $this;
    }
    /**
     * Get error_code value
     * @return int|null
     */
    public function getError_code()
    {
        return $this->error_code;
    }
    /**
     * Set error_code value
     * @param int $error_code
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function setError_code($error_code = null)
    {
        // validation for constraint: int
        if (!is_null($error_code) && !(is_int($error_code) || ctype_digit($error_code))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($error_code, true), gettype($error_code)), __LINE__);
        }
        $this->error_code = $error_code;
        return $this;
    }
    /**
     * Get values value
     * @return \SubregSDK\Test\StructType\Prices_Value[]|null
     */
    public function getValues()
    {
        return $this->values;
    }
    /**
     * This method is responsible for validating the values passed to the setValues method
     * This method is willingly generated in order to preserve the one-line inline validation within the setValues method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateValuesForArrayConstraintsFromSetValues(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $prices_ParamValuesItem) {
            // validation for constraint: itemType
            if (!$prices_ParamValuesItem instanceof \SubregSDK\Test\StructType\Prices_Value) {
                $invalidValues[] = is_object($prices_ParamValuesItem) ? get_class($prices_ParamValuesItem) : sprintf('%s(%s)', gettype($prices_ParamValuesItem), var_export($prices_ParamValuesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The values property can only contain items of type \SubregSDK\Test\StructType\Prices_Value, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set values value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Value[] $values
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function setValues(array $values = array())
    {
        // validation for constraint: array
        if ('' !== ($valuesArrayErrorMessage = self::validateValuesForArrayConstraintsFromSetValues($values))) {
            throw new \InvalidArgumentException($valuesArrayErrorMessage, __LINE__);
        }
        $this->values = $values;
        return $this;
    }
    /**
     * Add item to values value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Value $item
     * @return \SubregSDK\Test\StructType\Prices_Param
     */
    public function addToValues(\SubregSDK\Test\StructType\Prices_Value $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Prices_Value) {
            throw new \InvalidArgumentException(sprintf('The values property can only contain items of type \SubregSDK\Test\StructType\Prices_Value, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->values[] = $item;
        return $this;
    }
}
