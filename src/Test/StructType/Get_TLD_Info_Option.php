<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_TLD_Info_Option StructType
 * @subpackage Structs
 */
class Get_TLD_Info_Option extends AbstractStructBase
{
    /**
     * The value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $value;
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $name;
    /**
     * Constructor method for Get_TLD_Info_Option
     * @uses Get_TLD_Info_Option::setValue()
     * @uses Get_TLD_Info_Option::setName()
     * @param string $value
     * @param string $name
     */
    public function __construct($value = null, $name = null)
    {
        $this
            ->setValue($value)
            ->setName($name);
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Option
     */
    public function setValue($value = null)
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Option
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
}
