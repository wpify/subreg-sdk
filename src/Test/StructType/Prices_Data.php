<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Prices_Data StructType
 * @subpackage Structs
 */
class Prices_Data extends AbstractStructBase
{
    /**
     * The tld
     * @var string
     */
    public $tld;
    /**
     * The country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $country;
    /**
     * The continent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $continent;
    /**
     * The minyear
     * @var int
     */
    public $minyear;
    /**
     * The maxyear
     * @var int
     */
    public $maxyear;
    /**
     * The local_presence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $local_presence;
    /**
     * The prices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Prices_Price[]
     */
    public $prices;
    /**
     * The statuses
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $statuses;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Prices_Param[]
     */
    public $params;
    /**
     * Constructor method for Prices_Data
     * @uses Prices_Data::setTld()
     * @uses Prices_Data::setCountry()
     * @uses Prices_Data::setContinent()
     * @uses Prices_Data::setMinyear()
     * @uses Prices_Data::setMaxyear()
     * @uses Prices_Data::setLocal_presence()
     * @uses Prices_Data::setPrices()
     * @uses Prices_Data::setStatuses()
     * @uses Prices_Data::setParams()
     * @param string $tld
     * @param string $country
     * @param string $continent
     * @param int $minyear
     * @param int $maxyear
     * @param int $local_presence
     * @param \SubregSDK\Test\StructType\Prices_Price[] $prices
     * @param string[] $statuses
     * @param \SubregSDK\Test\StructType\Prices_Param[] $params
     */
    public function __construct($tld = null, $country = null, $continent = null, $minyear = null, $maxyear = null, $local_presence = null, array $prices = array(), array $statuses = array(), array $params = array())
    {
        $this
            ->setTld($tld)
            ->setCountry($country)
            ->setContinent($continent)
            ->setMinyear($minyear)
            ->setMaxyear($maxyear)
            ->setLocal_presence($local_presence)
            ->setPrices($prices)
            ->setStatuses($statuses)
            ->setParams($params);
    }
    /**
     * Get tld value
     * @return string|null
     */
    public function getTld()
    {
        return $this->tld;
    }
    /**
     * Set tld value
     * @param string $tld
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setTld($tld = null)
    {
        // validation for constraint: string
        if (!is_null($tld) && !is_string($tld)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tld, true), gettype($tld)), __LINE__);
        }
        $this->tld = $tld;
        return $this;
    }
    /**
     * Get country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * Set country value
     * @param string $country
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setCountry($country = null)
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        $this->country = $country;
        return $this;
    }
    /**
     * Get continent value
     * @return string|null
     */
    public function getContinent()
    {
        return $this->continent;
    }
    /**
     * Set continent value
     * @param string $continent
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setContinent($continent = null)
    {
        // validation for constraint: string
        if (!is_null($continent) && !is_string($continent)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($continent, true), gettype($continent)), __LINE__);
        }
        $this->continent = $continent;
        return $this;
    }
    /**
     * Get minyear value
     * @return int|null
     */
    public function getMinyear()
    {
        return $this->minyear;
    }
    /**
     * Set minyear value
     * @param int $minyear
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setMinyear($minyear = null)
    {
        // validation for constraint: int
        if (!is_null($minyear) && !(is_int($minyear) || ctype_digit($minyear))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minyear, true), gettype($minyear)), __LINE__);
        }
        $this->minyear = $minyear;
        return $this;
    }
    /**
     * Get maxyear value
     * @return int|null
     */
    public function getMaxyear()
    {
        return $this->maxyear;
    }
    /**
     * Set maxyear value
     * @param int $maxyear
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setMaxyear($maxyear = null)
    {
        // validation for constraint: int
        if (!is_null($maxyear) && !(is_int($maxyear) || ctype_digit($maxyear))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxyear, true), gettype($maxyear)), __LINE__);
        }
        $this->maxyear = $maxyear;
        return $this;
    }
    /**
     * Get local_presence value
     * @return int|null
     */
    public function getLocal_presence()
    {
        return $this->local_presence;
    }
    /**
     * Set local_presence value
     * @param int $local_presence
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setLocal_presence($local_presence = null)
    {
        // validation for constraint: int
        if (!is_null($local_presence) && !(is_int($local_presence) || ctype_digit($local_presence))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($local_presence, true), gettype($local_presence)), __LINE__);
        }
        $this->local_presence = $local_presence;
        return $this;
    }
    /**
     * Get prices value
     * @return \SubregSDK\Test\StructType\Prices_Price[]|null
     */
    public function getPrices()
    {
        return $this->prices;
    }
    /**
     * This method is responsible for validating the values passed to the setPrices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePricesForArrayConstraintsFromSetPrices(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $prices_DataPricesItem) {
            // validation for constraint: itemType
            if (!$prices_DataPricesItem instanceof \SubregSDK\Test\StructType\Prices_Price) {
                $invalidValues[] = is_object($prices_DataPricesItem) ? get_class($prices_DataPricesItem) : sprintf('%s(%s)', gettype($prices_DataPricesItem), var_export($prices_DataPricesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The prices property can only contain items of type \SubregSDK\Test\StructType\Prices_Price, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Price[] $prices
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setPrices(array $prices = array())
    {
        // validation for constraint: array
        if ('' !== ($pricesArrayErrorMessage = self::validatePricesForArrayConstraintsFromSetPrices($prices))) {
            throw new \InvalidArgumentException($pricesArrayErrorMessage, __LINE__);
        }
        $this->prices = $prices;
        return $this;
    }
    /**
     * Add item to prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Price $item
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function addToPrices(\SubregSDK\Test\StructType\Prices_Price $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Prices_Price) {
            throw new \InvalidArgumentException(sprintf('The prices property can only contain items of type \SubregSDK\Test\StructType\Prices_Price, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->prices[] = $item;
        return $this;
    }
    /**
     * Get statuses value
     * @return string[]|null
     */
    public function getStatuses()
    {
        return $this->statuses;
    }
    /**
     * This method is responsible for validating the values passed to the setStatuses method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatuses method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusesForArrayConstraintsFromSetStatuses(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $prices_DataStatusesItem) {
            // validation for constraint: itemType
            if (!is_string($prices_DataStatusesItem)) {
                $invalidValues[] = is_object($prices_DataStatusesItem) ? get_class($prices_DataStatusesItem) : sprintf('%s(%s)', gettype($prices_DataStatusesItem), var_export($prices_DataStatusesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The statuses property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set statuses value
     * @throws \InvalidArgumentException
     * @param string[] $statuses
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setStatuses(array $statuses = array())
    {
        // validation for constraint: array
        if ('' !== ($statusesArrayErrorMessage = self::validateStatusesForArrayConstraintsFromSetStatuses($statuses))) {
            throw new \InvalidArgumentException($statusesArrayErrorMessage, __LINE__);
        }
        $this->statuses = $statuses;
        return $this;
    }
    /**
     * Add item to statuses value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function addToStatuses($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The statuses property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->statuses[] = $item;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Test\StructType\Prices_Param[]|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * This method is responsible for validating the values passed to the setParams method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParams method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParamsForArrayConstraintsFromSetParams(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $prices_DataParamsItem) {
            // validation for constraint: itemType
            if (!$prices_DataParamsItem instanceof \SubregSDK\Test\StructType\Prices_Param) {
                $invalidValues[] = is_object($prices_DataParamsItem) ? get_class($prices_DataParamsItem) : sprintf('%s(%s)', gettype($prices_DataParamsItem), var_export($prices_DataParamsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Prices_Param, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Param[] $params
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function setParams(array $params = array())
    {
        // validation for constraint: array
        if ('' !== ($paramsArrayErrorMessage = self::validateParamsForArrayConstraintsFromSetParams($params))) {
            throw new \InvalidArgumentException($paramsArrayErrorMessage, __LINE__);
        }
        $this->params = $params;
        return $this;
    }
    /**
     * Add item to params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Prices_Param $item
     * @return \SubregSDK\Test\StructType\Prices_Data
     */
    public function addToParams(\SubregSDK\Test\StructType\Prices_Param $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Prices_Param) {
            throw new \InvalidArgumentException(sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Prices_Param, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->params[] = $item;
        return $this;
    }
}
