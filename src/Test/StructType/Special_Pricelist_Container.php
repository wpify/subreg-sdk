<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Special_Pricelist_Container StructType
 * @subpackage Structs
 */
class Special_Pricelist_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Special_Pricelist_Response
     */
    public $response;
    /**
     * Constructor method for Special_Pricelist_Container
     * @uses Special_Pricelist_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Special_Pricelist_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Special_Pricelist_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Special_Pricelist_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Special_Pricelist_Response $response
     * @return \SubregSDK\Test\StructType\Special_Pricelist_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Special_Pricelist_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
