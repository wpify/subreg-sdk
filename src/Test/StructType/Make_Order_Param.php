<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Param StructType
 * @subpackage Structs
 */
class Make_Order_Param extends AbstractStructBase
{
    /**
     * The dsdata
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Dsdata[]
     */
    public $dsdata;
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Contact
     */
    public $tech;
    /**
     * The hosts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Host[]
     */
    public $hosts;
    /**
     * The records
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Record[]
     */
    public $records;
    /**
     * The param
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $param;
    /**
     * The value
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $value;
    /**
     * Constructor method for Make_Order_Param
     * @uses Make_Order_Param::setDsdata()
     * @uses Make_Order_Param::setTech()
     * @uses Make_Order_Param::setHosts()
     * @uses Make_Order_Param::setRecords()
     * @uses Make_Order_Param::setParam()
     * @uses Make_Order_Param::setValue()
     * @param \SubregSDK\Test\StructType\Make_Order_Dsdata[] $dsdata
     * @param \SubregSDK\Test\StructType\Make_Order_Contact $tech
     * @param \SubregSDK\Test\StructType\Make_Order_Host[] $hosts
     * @param \SubregSDK\Test\StructType\Make_Order_Record[] $records
     * @param string $param
     * @param string $value
     */
    public function __construct(array $dsdata = array(), \SubregSDK\Test\StructType\Make_Order_Contact $tech = null, array $hosts = array(), array $records = array(), $param = null, $value = null)
    {
        $this
            ->setDsdata($dsdata)
            ->setTech($tech)
            ->setHosts($hosts)
            ->setRecords($records)
            ->setParam($param)
            ->setValue($value);
    }
    /**
     * Get dsdata value
     * @return \SubregSDK\Test\StructType\Make_Order_Dsdata[]|null
     */
    public function getDsdata()
    {
        return $this->dsdata;
    }
    /**
     * This method is responsible for validating the values passed to the setDsdata method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDsdata method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDsdataForArrayConstraintsFromSetDsdata(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_ParamDsdataItem) {
            // validation for constraint: itemType
            if (!$make_Order_ParamDsdataItem instanceof \SubregSDK\Test\StructType\Make_Order_Dsdata) {
                $invalidValues[] = is_object($make_Order_ParamDsdataItem) ? get_class($make_Order_ParamDsdataItem) : sprintf('%s(%s)', gettype($make_Order_ParamDsdataItem), var_export($make_Order_ParamDsdataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dsdata property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Dsdata, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set dsdata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Dsdata[] $dsdata
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setDsdata(array $dsdata = array())
    {
        // validation for constraint: array
        if ('' !== ($dsdataArrayErrorMessage = self::validateDsdataForArrayConstraintsFromSetDsdata($dsdata))) {
            throw new \InvalidArgumentException($dsdataArrayErrorMessage, __LINE__);
        }
        $this->dsdata = $dsdata;
        return $this;
    }
    /**
     * Add item to dsdata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Dsdata $item
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function addToDsdata(\SubregSDK\Test\StructType\Make_Order_Dsdata $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Make_Order_Dsdata) {
            throw new \InvalidArgumentException(sprintf('The dsdata property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Dsdata, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dsdata[] = $item;
        return $this;
    }
    /**
     * Get tech value
     * @return \SubregSDK\Test\StructType\Make_Order_Contact|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * Set tech value
     * @param \SubregSDK\Test\StructType\Make_Order_Contact $tech
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setTech(\SubregSDK\Test\StructType\Make_Order_Contact $tech = null)
    {
        $this->tech = $tech;
        return $this;
    }
    /**
     * Get hosts value
     * @return \SubregSDK\Test\StructType\Make_Order_Host[]|null
     */
    public function getHosts()
    {
        return $this->hosts;
    }
    /**
     * This method is responsible for validating the values passed to the setHosts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHosts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHostsForArrayConstraintsFromSetHosts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_ParamHostsItem) {
            // validation for constraint: itemType
            if (!$make_Order_ParamHostsItem instanceof \SubregSDK\Test\StructType\Make_Order_Host) {
                $invalidValues[] = is_object($make_Order_ParamHostsItem) ? get_class($make_Order_ParamHostsItem) : sprintf('%s(%s)', gettype($make_Order_ParamHostsItem), var_export($make_Order_ParamHostsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The hosts property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Host, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set hosts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Host[] $hosts
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setHosts(array $hosts = array())
    {
        // validation for constraint: array
        if ('' !== ($hostsArrayErrorMessage = self::validateHostsForArrayConstraintsFromSetHosts($hosts))) {
            throw new \InvalidArgumentException($hostsArrayErrorMessage, __LINE__);
        }
        $this->hosts = $hosts;
        return $this;
    }
    /**
     * Add item to hosts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Host $item
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function addToHosts(\SubregSDK\Test\StructType\Make_Order_Host $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Make_Order_Host) {
            throw new \InvalidArgumentException(sprintf('The hosts property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Host, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->hosts[] = $item;
        return $this;
    }
    /**
     * Get records value
     * @return \SubregSDK\Test\StructType\Make_Order_Record[]|null
     */
    public function getRecords()
    {
        return $this->records;
    }
    /**
     * This method is responsible for validating the values passed to the setRecords method
     * This method is willingly generated in order to preserve the one-line inline validation within the setRecords method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateRecordsForArrayConstraintsFromSetRecords(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $make_Order_ParamRecordsItem) {
            // validation for constraint: itemType
            if (!$make_Order_ParamRecordsItem instanceof \SubregSDK\Test\StructType\Make_Order_Record) {
                $invalidValues[] = is_object($make_Order_ParamRecordsItem) ? get_class($make_Order_ParamRecordsItem) : sprintf('%s(%s)', gettype($make_Order_ParamRecordsItem), var_export($make_Order_ParamRecordsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The records property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Record, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Record[] $records
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setRecords(array $records = array())
    {
        // validation for constraint: array
        if ('' !== ($recordsArrayErrorMessage = self::validateRecordsForArrayConstraintsFromSetRecords($records))) {
            throw new \InvalidArgumentException($recordsArrayErrorMessage, __LINE__);
        }
        $this->records = $records;
        return $this;
    }
    /**
     * Add item to records value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Make_Order_Record $item
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function addToRecords(\SubregSDK\Test\StructType\Make_Order_Record $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Make_Order_Record) {
            throw new \InvalidArgumentException(sprintf('The records property can only contain items of type \SubregSDK\Test\StructType\Make_Order_Record, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->records[] = $item;
        return $this;
    }
    /**
     * Get param value
     * @return string|null
     */
    public function getParam()
    {
        return $this->param;
    }
    /**
     * Set param value
     * @param string $param
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setParam($param = null)
    {
        // validation for constraint: string
        if (!is_null($param) && !is_string($param)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($param, true), gettype($param)), __LINE__);
        }
        $this->param = $param;
        return $this;
    }
    /**
     * Get value value
     * @return string|null
     */
    public function getValue()
    {
        return $this->value;
    }
    /**
     * Set value value
     * @param string $value
     * @return \SubregSDK\Test\StructType\Make_Order_Param
     */
    public function setValue($value = null)
    {
        // validation for constraint: string
        if (!is_null($value) && !is_string($value)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($value, true), gettype($value)), __LINE__);
        }
        $this->value = $value;
        return $this;
    }
}
