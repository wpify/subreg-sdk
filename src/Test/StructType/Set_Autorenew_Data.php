<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Autorenew_Data StructType
 * @subpackage Structs
 */
class Set_Autorenew_Data extends AbstractStructBase
{
}
