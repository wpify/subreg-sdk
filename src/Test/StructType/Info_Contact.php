<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Contact StructType
 * @subpackage Structs
 */
class Info_Contact extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The contact
     * @var \SubregSDK\Test\StructType\Info_Contact_Contact
     */
    public $contact;
    /**
     * Constructor method for Info_Contact
     * @uses Info_Contact::setSsid()
     * @uses Info_Contact::setContact()
     * @param string $ssid
     * @param \SubregSDK\Test\StructType\Info_Contact_Contact $contact
     */
    public function __construct($ssid = null, \SubregSDK\Test\StructType\Info_Contact_Contact $contact = null)
    {
        $this
            ->setSsid($ssid)
            ->setContact($contact);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Info_Contact
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get contact value
     * @return \SubregSDK\Test\StructType\Info_Contact_Contact|null
     */
    public function getContact()
    {
        return $this->contact;
    }
    /**
     * Set contact value
     * @param \SubregSDK\Test\StructType\Info_Contact_Contact $contact
     * @return \SubregSDK\Test\StructType\Info_Contact
     */
    public function setContact(\SubregSDK\Test\StructType\Info_Contact_Contact $contact = null)
    {
        $this->contact = $contact;
        return $this;
    }
}
