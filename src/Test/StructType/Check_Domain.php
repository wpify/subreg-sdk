<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Domain StructType
 * @subpackage Structs
 */
class Check_Domain extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The domain
     * @var string
     */
    public $domain;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Check_Domain_Params
     */
    public $params;
    /**
     * Constructor method for Check_Domain
     * @uses Check_Domain::setSsid()
     * @uses Check_Domain::setDomain()
     * @uses Check_Domain::setParams()
     * @param string $ssid
     * @param string $domain
     * @param \SubregSDK\Test\StructType\Check_Domain_Params $params
     */
    public function __construct($ssid = null, $domain = null, \SubregSDK\Test\StructType\Check_Domain_Params $params = null)
    {
        $this
            ->setSsid($ssid)
            ->setDomain($domain)
            ->setParams($params);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Check_Domain
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get domain value
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }
    /**
     * Set domain value
     * @param string $domain
     * @return \SubregSDK\Test\StructType\Check_Domain
     */
    public function setDomain($domain = null)
    {
        // validation for constraint: string
        if (!is_null($domain) && !is_string($domain)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($domain, true), gettype($domain)), __LINE__);
        }
        $this->domain = $domain;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Test\StructType\Check_Domain_Params|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * Set params value
     * @param \SubregSDK\Test\StructType\Check_Domain_Params $params
     * @return \SubregSDK\Test\StructType\Check_Domain
     */
    public function setParams(\SubregSDK\Test\StructType\Check_Domain_Params $params = null)
    {
        $this->params = $params;
        return $this;
    }
}
