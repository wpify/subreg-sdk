<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Sign_DNS_Zone_Data StructType
 * @subpackage Structs
 */
class Sign_DNS_Zone_Data extends AbstractStructBase
{
}
