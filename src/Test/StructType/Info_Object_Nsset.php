<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Nsset StructType
 * @subpackage Structs
 */
class Info_Object_Nsset extends AbstractStructBase
{
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $tech;
    /**
     * The ns
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Object_Ns[]
     */
    public $ns;
    /**
     * The clID
     * @var string
     */
    public $clID;
    /**
     * Constructor method for Info_Object_Nsset
     * @uses Info_Object_Nsset::setTech()
     * @uses Info_Object_Nsset::setNs()
     * @uses Info_Object_Nsset::setClID()
     * @param string $tech
     * @param \SubregSDK\Test\StructType\Info_Object_Ns[] $ns
     * @param string $clID
     */
    public function __construct($tech = null, array $ns = array(), $clID = null)
    {
        $this
            ->setTech($tech)
            ->setNs($ns)
            ->setClID($clID);
    }
    /**
     * Get tech value
     * @return string|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * Set tech value
     * @param string $tech
     * @return \SubregSDK\Test\StructType\Info_Object_Nsset
     */
    public function setTech($tech = null)
    {
        // validation for constraint: string
        if (!is_null($tech) && !is_string($tech)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tech, true), gettype($tech)), __LINE__);
        }
        $this->tech = $tech;
        return $this;
    }
    /**
     * Get ns value
     * @return \SubregSDK\Test\StructType\Info_Object_Ns[]|null
     */
    public function getNs()
    {
        return $this->ns;
    }
    /**
     * This method is responsible for validating the values passed to the setNs method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNs method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNsForArrayConstraintsFromSetNs(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Object_NssetNsItem) {
            // validation for constraint: itemType
            if (!$info_Object_NssetNsItem instanceof \SubregSDK\Test\StructType\Info_Object_Ns) {
                $invalidValues[] = is_object($info_Object_NssetNsItem) ? get_class($info_Object_NssetNsItem) : sprintf('%s(%s)', gettype($info_Object_NssetNsItem), var_export($info_Object_NssetNsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The ns property can only contain items of type \SubregSDK\Test\StructType\Info_Object_Ns, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set ns value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Object_Ns[] $ns
     * @return \SubregSDK\Test\StructType\Info_Object_Nsset
     */
    public function setNs(array $ns = array())
    {
        // validation for constraint: array
        if ('' !== ($nsArrayErrorMessage = self::validateNsForArrayConstraintsFromSetNs($ns))) {
            throw new \InvalidArgumentException($nsArrayErrorMessage, __LINE__);
        }
        $this->ns = $ns;
        return $this;
    }
    /**
     * Add item to ns value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Object_Ns $item
     * @return \SubregSDK\Test\StructType\Info_Object_Nsset
     */
    public function addToNs(\SubregSDK\Test\StructType\Info_Object_Ns $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Object_Ns) {
            throw new \InvalidArgumentException(sprintf('The ns property can only contain items of type \SubregSDK\Test\StructType\Info_Object_Ns, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->ns[] = $item;
        return $this;
    }
    /**
     * Get clID value
     * @return string|null
     */
    public function getClID()
    {
        return $this->clID;
    }
    /**
     * Set clID value
     * @param string $clID
     * @return \SubregSDK\Test\StructType\Info_Object_Nsset
     */
    public function setClID($clID = null)
    {
        // validation for constraint: string
        if (!is_null($clID) && !is_string($clID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clID, true), gettype($clID)), __LINE__);
        }
        $this->clID = $clID;
        return $this;
    }
}
