<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OIB_Search_Data StructType
 * @subpackage Structs
 */
class OIB_Search_Data extends AbstractStructBase
{
    /**
     * The domains
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\OIB_Search_Domain[]
     */
    public $domains;
    /**
     * The types
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\OIB_Search_Type[]
     */
    public $types;
    /**
     * Constructor method for OIB_Search_Data
     * @uses OIB_Search_Data::setDomains()
     * @uses OIB_Search_Data::setTypes()
     * @param \SubregSDK\Test\StructType\OIB_Search_Domain[] $domains
     * @param \SubregSDK\Test\StructType\OIB_Search_Type[] $types
     */
    public function __construct(array $domains = array(), array $types = array())
    {
        $this
            ->setDomains($domains)
            ->setTypes($types);
    }
    /**
     * Get domains value
     * @return \SubregSDK\Test\StructType\OIB_Search_Domain[]|null
     */
    public function getDomains()
    {
        return $this->domains;
    }
    /**
     * This method is responsible for validating the values passed to the setDomains method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDomains method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDomainsForArrayConstraintsFromSetDomains(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $oIB_Search_DataDomainsItem) {
            // validation for constraint: itemType
            if (!$oIB_Search_DataDomainsItem instanceof \SubregSDK\Test\StructType\OIB_Search_Domain) {
                $invalidValues[] = is_object($oIB_Search_DataDomainsItem) ? get_class($oIB_Search_DataDomainsItem) : sprintf('%s(%s)', gettype($oIB_Search_DataDomainsItem), var_export($oIB_Search_DataDomainsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\OIB_Search_Domain, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\OIB_Search_Domain[] $domains
     * @return \SubregSDK\Test\StructType\OIB_Search_Data
     */
    public function setDomains(array $domains = array())
    {
        // validation for constraint: array
        if ('' !== ($domainsArrayErrorMessage = self::validateDomainsForArrayConstraintsFromSetDomains($domains))) {
            throw new \InvalidArgumentException($domainsArrayErrorMessage, __LINE__);
        }
        $this->domains = $domains;
        return $this;
    }
    /**
     * Add item to domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\OIB_Search_Domain $item
     * @return \SubregSDK\Test\StructType\OIB_Search_Data
     */
    public function addToDomains(\SubregSDK\Test\StructType\OIB_Search_Domain $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\OIB_Search_Domain) {
            throw new \InvalidArgumentException(sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\OIB_Search_Domain, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->domains[] = $item;
        return $this;
    }
    /**
     * Get types value
     * @return \SubregSDK\Test\StructType\OIB_Search_Type[]|null
     */
    public function getTypes()
    {
        return $this->types;
    }
    /**
     * This method is responsible for validating the values passed to the setTypes method
     * This method is willingly generated in order to preserve the one-line inline validation within the setTypes method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateTypesForArrayConstraintsFromSetTypes(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $oIB_Search_DataTypesItem) {
            // validation for constraint: itemType
            if (!$oIB_Search_DataTypesItem instanceof \SubregSDK\Test\StructType\OIB_Search_Type) {
                $invalidValues[] = is_object($oIB_Search_DataTypesItem) ? get_class($oIB_Search_DataTypesItem) : sprintf('%s(%s)', gettype($oIB_Search_DataTypesItem), var_export($oIB_Search_DataTypesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The types property can only contain items of type \SubregSDK\Test\StructType\OIB_Search_Type, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set types value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\OIB_Search_Type[] $types
     * @return \SubregSDK\Test\StructType\OIB_Search_Data
     */
    public function setTypes(array $types = array())
    {
        // validation for constraint: array
        if ('' !== ($typesArrayErrorMessage = self::validateTypesForArrayConstraintsFromSetTypes($types))) {
            throw new \InvalidArgumentException($typesArrayErrorMessage, __LINE__);
        }
        $this->types = $types;
        return $this;
    }
    /**
     * Add item to types value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\OIB_Search_Type $item
     * @return \SubregSDK\Test\StructType\OIB_Search_Data
     */
    public function addToTypes(\SubregSDK\Test\StructType\OIB_Search_Type $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\OIB_Search_Type) {
            throw new \InvalidArgumentException(sprintf('The types property can only contain items of type \SubregSDK\Test\StructType\OIB_Search_Type, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->types[] = $item;
        return $this;
    }
}
