<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Contact StructType
 * @subpackage Structs
 */
class Info_Object_Contact extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $name;
    /**
     * The org
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $org;
    /**
     * The street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $street;
    /**
     * The city
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $city;
    /**
     * The pc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $pc;
    /**
     * The sp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $sp;
    /**
     * The cc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $cc;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $email;
    /**
     * The phone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $phone;
    /**
     * The fax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $fax;
    /**
     * The vat
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $vat;
    /**
     * The notify_email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $notify_email;
    /**
     * The ident_type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ident_type;
    /**
     * The ident_number
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ident_number;
    /**
     * The clID
     * @var string
     */
    public $clID;
    /**
     * The hidden
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $hidden;
    /**
     * The statuses
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $statuses;
    /**
     * Constructor method for Info_Object_Contact
     * @uses Info_Object_Contact::setName()
     * @uses Info_Object_Contact::setOrg()
     * @uses Info_Object_Contact::setStreet()
     * @uses Info_Object_Contact::setCity()
     * @uses Info_Object_Contact::setPc()
     * @uses Info_Object_Contact::setSp()
     * @uses Info_Object_Contact::setCc()
     * @uses Info_Object_Contact::setEmail()
     * @uses Info_Object_Contact::setPhone()
     * @uses Info_Object_Contact::setFax()
     * @uses Info_Object_Contact::setVat()
     * @uses Info_Object_Contact::setNotify_email()
     * @uses Info_Object_Contact::setIdent_type()
     * @uses Info_Object_Contact::setIdent_number()
     * @uses Info_Object_Contact::setClID()
     * @uses Info_Object_Contact::setHidden()
     * @uses Info_Object_Contact::setStatuses()
     * @param string $name
     * @param string $org
     * @param string $street
     * @param string $city
     * @param string $pc
     * @param string $sp
     * @param string $cc
     * @param string $email
     * @param string $phone
     * @param string $fax
     * @param string $vat
     * @param string $notify_email
     * @param string $ident_type
     * @param string $ident_number
     * @param string $clID
     * @param string[] $hidden
     * @param string[] $statuses
     */
    public function __construct($name = null, $org = null, $street = null, $city = null, $pc = null, $sp = null, $cc = null, $email = null, $phone = null, $fax = null, $vat = null, $notify_email = null, $ident_type = null, $ident_number = null, $clID = null, array $hidden = array(), array $statuses = array())
    {
        $this
            ->setName($name)
            ->setOrg($org)
            ->setStreet($street)
            ->setCity($city)
            ->setPc($pc)
            ->setSp($sp)
            ->setCc($cc)
            ->setEmail($email)
            ->setPhone($phone)
            ->setFax($fax)
            ->setVat($vat)
            ->setNotify_email($notify_email)
            ->setIdent_type($ident_type)
            ->setIdent_number($ident_number)
            ->setClID($clID)
            ->setHidden($hidden)
            ->setStatuses($statuses);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get org value
     * @return string|null
     */
    public function getOrg()
    {
        return $this->org;
    }
    /**
     * Set org value
     * @param string $org
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setOrg($org = null)
    {
        // validation for constraint: string
        if (!is_null($org) && !is_string($org)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($org, true), gettype($org)), __LINE__);
        }
        $this->org = $org;
        return $this;
    }
    /**
     * Get street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * Set street value
     * @param string $street
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        $this->street = $street;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get pc value
     * @return string|null
     */
    public function getPc()
    {
        return $this->pc;
    }
    /**
     * Set pc value
     * @param string $pc
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setPc($pc = null)
    {
        // validation for constraint: string
        if (!is_null($pc) && !is_string($pc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pc, true), gettype($pc)), __LINE__);
        }
        $this->pc = $pc;
        return $this;
    }
    /**
     * Get sp value
     * @return string|null
     */
    public function getSp()
    {
        return $this->sp;
    }
    /**
     * Set sp value
     * @param string $sp
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setSp($sp = null)
    {
        // validation for constraint: string
        if (!is_null($sp) && !is_string($sp)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sp, true), gettype($sp)), __LINE__);
        }
        $this->sp = $sp;
        return $this;
    }
    /**
     * Get cc value
     * @return string|null
     */
    public function getCc()
    {
        return $this->cc;
    }
    /**
     * Set cc value
     * @param string $cc
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setCc($cc = null)
    {
        // validation for constraint: string
        if (!is_null($cc) && !is_string($cc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cc, true), gettype($cc)), __LINE__);
        }
        $this->cc = $cc;
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
    /**
     * Get phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * Set phone value
     * @param string $phone
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        $this->phone = $phone;
        return $this;
    }
    /**
     * Get fax value
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }
    /**
     * Set fax value
     * @param string $fax
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setFax($fax = null)
    {
        // validation for constraint: string
        if (!is_null($fax) && !is_string($fax)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fax, true), gettype($fax)), __LINE__);
        }
        $this->fax = $fax;
        return $this;
    }
    /**
     * Get vat value
     * @return string|null
     */
    public function getVat()
    {
        return $this->vat;
    }
    /**
     * Set vat value
     * @param string $vat
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setVat($vat = null)
    {
        // validation for constraint: string
        if (!is_null($vat) && !is_string($vat)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($vat, true), gettype($vat)), __LINE__);
        }
        $this->vat = $vat;
        return $this;
    }
    /**
     * Get notify_email value
     * @return string|null
     */
    public function getNotify_email()
    {
        return $this->notify_email;
    }
    /**
     * Set notify_email value
     * @param string $notify_email
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setNotify_email($notify_email = null)
    {
        // validation for constraint: string
        if (!is_null($notify_email) && !is_string($notify_email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($notify_email, true), gettype($notify_email)), __LINE__);
        }
        $this->notify_email = $notify_email;
        return $this;
    }
    /**
     * Get ident_type value
     * @return string|null
     */
    public function getIdent_type()
    {
        return $this->ident_type;
    }
    /**
     * Set ident_type value
     * @param string $ident_type
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setIdent_type($ident_type = null)
    {
        // validation for constraint: string
        if (!is_null($ident_type) && !is_string($ident_type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ident_type, true), gettype($ident_type)), __LINE__);
        }
        $this->ident_type = $ident_type;
        return $this;
    }
    /**
     * Get ident_number value
     * @return string|null
     */
    public function getIdent_number()
    {
        return $this->ident_number;
    }
    /**
     * Set ident_number value
     * @param string $ident_number
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setIdent_number($ident_number = null)
    {
        // validation for constraint: string
        if (!is_null($ident_number) && !is_string($ident_number)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ident_number, true), gettype($ident_number)), __LINE__);
        }
        $this->ident_number = $ident_number;
        return $this;
    }
    /**
     * Get clID value
     * @return string|null
     */
    public function getClID()
    {
        return $this->clID;
    }
    /**
     * Set clID value
     * @param string $clID
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setClID($clID = null)
    {
        // validation for constraint: string
        if (!is_null($clID) && !is_string($clID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clID, true), gettype($clID)), __LINE__);
        }
        $this->clID = $clID;
        return $this;
    }
    /**
     * Get hidden value
     * @return string[]|null
     */
    public function getHidden()
    {
        return $this->hidden;
    }
    /**
     * This method is responsible for validating the values passed to the setHidden method
     * This method is willingly generated in order to preserve the one-line inline validation within the setHidden method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateHiddenForArrayConstraintsFromSetHidden(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Object_ContactHiddenItem) {
            // validation for constraint: itemType
            if (!is_string($info_Object_ContactHiddenItem)) {
                $invalidValues[] = is_object($info_Object_ContactHiddenItem) ? get_class($info_Object_ContactHiddenItem) : sprintf('%s(%s)', gettype($info_Object_ContactHiddenItem), var_export($info_Object_ContactHiddenItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The hidden property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set hidden value
     * @throws \InvalidArgumentException
     * @param string[] $hidden
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setHidden(array $hidden = array())
    {
        // validation for constraint: array
        if ('' !== ($hiddenArrayErrorMessage = self::validateHiddenForArrayConstraintsFromSetHidden($hidden))) {
            throw new \InvalidArgumentException($hiddenArrayErrorMessage, __LINE__);
        }
        $this->hidden = $hidden;
        return $this;
    }
    /**
     * Add item to hidden value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function addToHidden($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The hidden property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->hidden[] = $item;
        return $this;
    }
    /**
     * Get statuses value
     * @return string[]|null
     */
    public function getStatuses()
    {
        return $this->statuses;
    }
    /**
     * This method is responsible for validating the values passed to the setStatuses method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatuses method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusesForArrayConstraintsFromSetStatuses(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Object_ContactStatusesItem) {
            // validation for constraint: itemType
            if (!is_string($info_Object_ContactStatusesItem)) {
                $invalidValues[] = is_object($info_Object_ContactStatusesItem) ? get_class($info_Object_ContactStatusesItem) : sprintf('%s(%s)', gettype($info_Object_ContactStatusesItem), var_export($info_Object_ContactStatusesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The statuses property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set statuses value
     * @throws \InvalidArgumentException
     * @param string[] $statuses
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function setStatuses(array $statuses = array())
    {
        // validation for constraint: array
        if ('' !== ($statusesArrayErrorMessage = self::validateStatusesForArrayConstraintsFromSetStatuses($statuses))) {
            throw new \InvalidArgumentException($statusesArrayErrorMessage, __LINE__);
        }
        $this->statuses = $statuses;
        return $this;
    }
    /**
     * Add item to statuses value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Info_Object_Contact
     */
    public function addToStatuses($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The statuses property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->statuses[] = $item;
        return $this;
    }
}
