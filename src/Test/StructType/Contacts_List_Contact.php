<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Contacts_List_Contact StructType
 * @subpackage Structs
 */
class Contacts_List_Contact extends AbstractStructBase
{
    /**
     * The name
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $name;
    /**
     * The surname
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $surname;
    /**
     * The org
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $org;
    /**
     * The street
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $street;
    /**
     * The city
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $city;
    /**
     * The pc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $pc;
    /**
     * The sp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $sp;
    /**
     * The cc
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $cc;
    /**
     * The email
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $email;
    /**
     * The phone
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $phone;
    /**
     * The fax
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $fax;
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * Constructor method for Contacts_List_Contact
     * @uses Contacts_List_Contact::setName()
     * @uses Contacts_List_Contact::setSurname()
     * @uses Contacts_List_Contact::setOrg()
     * @uses Contacts_List_Contact::setStreet()
     * @uses Contacts_List_Contact::setCity()
     * @uses Contacts_List_Contact::setPc()
     * @uses Contacts_List_Contact::setSp()
     * @uses Contacts_List_Contact::setCc()
     * @uses Contacts_List_Contact::setEmail()
     * @uses Contacts_List_Contact::setPhone()
     * @uses Contacts_List_Contact::setFax()
     * @uses Contacts_List_Contact::setId()
     * @param string $name
     * @param string $surname
     * @param string $org
     * @param string $street
     * @param string $city
     * @param string $pc
     * @param string $sp
     * @param string $cc
     * @param string $email
     * @param string $phone
     * @param string $fax
     * @param string $id
     */
    public function __construct($name = null, $surname = null, $org = null, $street = null, $city = null, $pc = null, $sp = null, $cc = null, $email = null, $phone = null, $fax = null, $id = null)
    {
        $this
            ->setName($name)
            ->setSurname($surname)
            ->setOrg($org)
            ->setStreet($street)
            ->setCity($city)
            ->setPc($pc)
            ->setSp($sp)
            ->setCc($cc)
            ->setEmail($email)
            ->setPhone($phone)
            ->setFax($fax)
            ->setId($id);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get surname value
     * @return string|null
     */
    public function getSurname()
    {
        return $this->surname;
    }
    /**
     * Set surname value
     * @param string $surname
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setSurname($surname = null)
    {
        // validation for constraint: string
        if (!is_null($surname) && !is_string($surname)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($surname, true), gettype($surname)), __LINE__);
        }
        $this->surname = $surname;
        return $this;
    }
    /**
     * Get org value
     * @return string|null
     */
    public function getOrg()
    {
        return $this->org;
    }
    /**
     * Set org value
     * @param string $org
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setOrg($org = null)
    {
        // validation for constraint: string
        if (!is_null($org) && !is_string($org)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($org, true), gettype($org)), __LINE__);
        }
        $this->org = $org;
        return $this;
    }
    /**
     * Get street value
     * @return string|null
     */
    public function getStreet()
    {
        return $this->street;
    }
    /**
     * Set street value
     * @param string $street
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setStreet($street = null)
    {
        // validation for constraint: string
        if (!is_null($street) && !is_string($street)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($street, true), gettype($street)), __LINE__);
        }
        $this->street = $street;
        return $this;
    }
    /**
     * Get city value
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }
    /**
     * Set city value
     * @param string $city
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setCity($city = null)
    {
        // validation for constraint: string
        if (!is_null($city) && !is_string($city)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($city, true), gettype($city)), __LINE__);
        }
        $this->city = $city;
        return $this;
    }
    /**
     * Get pc value
     * @return string|null
     */
    public function getPc()
    {
        return $this->pc;
    }
    /**
     * Set pc value
     * @param string $pc
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setPc($pc = null)
    {
        // validation for constraint: string
        if (!is_null($pc) && !is_string($pc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($pc, true), gettype($pc)), __LINE__);
        }
        $this->pc = $pc;
        return $this;
    }
    /**
     * Get sp value
     * @return string|null
     */
    public function getSp()
    {
        return $this->sp;
    }
    /**
     * Set sp value
     * @param string $sp
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setSp($sp = null)
    {
        // validation for constraint: string
        if (!is_null($sp) && !is_string($sp)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($sp, true), gettype($sp)), __LINE__);
        }
        $this->sp = $sp;
        return $this;
    }
    /**
     * Get cc value
     * @return string|null
     */
    public function getCc()
    {
        return $this->cc;
    }
    /**
     * Set cc value
     * @param string $cc
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setCc($cc = null)
    {
        // validation for constraint: string
        if (!is_null($cc) && !is_string($cc)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($cc, true), gettype($cc)), __LINE__);
        }
        $this->cc = $cc;
        return $this;
    }
    /**
     * Get email value
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * Set email value
     * @param string $email
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setEmail($email = null)
    {
        // validation for constraint: string
        if (!is_null($email) && !is_string($email)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($email, true), gettype($email)), __LINE__);
        }
        $this->email = $email;
        return $this;
    }
    /**
     * Get phone value
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }
    /**
     * Set phone value
     * @param string $phone
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setPhone($phone = null)
    {
        // validation for constraint: string
        if (!is_null($phone) && !is_string($phone)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($phone, true), gettype($phone)), __LINE__);
        }
        $this->phone = $phone;
        return $this;
    }
    /**
     * Get fax value
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }
    /**
     * Set fax value
     * @param string $fax
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setFax($fax = null)
    {
        // validation for constraint: string
        if (!is_null($fax) && !is_string($fax)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($fax, true), gettype($fax)), __LINE__);
        }
        $this->fax = $fax;
        return $this;
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Test\StructType\Contacts_List_Contact
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
}
