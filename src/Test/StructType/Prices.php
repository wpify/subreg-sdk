<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Prices StructType
 * @subpackage Structs
 */
class Prices extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The tld
     * @var string
     */
    public $tld;
    /**
     * Constructor method for Prices
     * @uses Prices::setSsid()
     * @uses Prices::setTld()
     * @param string $ssid
     * @param string $tld
     */
    public function __construct($ssid = null, $tld = null)
    {
        $this
            ->setSsid($ssid)
            ->setTld($tld);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Prices
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get tld value
     * @return string|null
     */
    public function getTld()
    {
        return $this->tld;
    }
    /**
     * Set tld value
     * @param string $tld
     * @return \SubregSDK\Test\StructType\Prices
     */
    public function setTld($tld = null)
    {
        // validation for constraint: string
        if (!is_null($tld) && !is_string($tld)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tld, true), gettype($tld)), __LINE__);
        }
        $this->tld = $tld;
        return $this;
    }
}
