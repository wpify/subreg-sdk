<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Credit_Credit StructType
 * @subpackage Structs
 */
class Get_Credit_Credit extends AbstractStructBase
{
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The threshold
     * @var float
     */
    public $threshold;
    /**
     * The users
     * @var float
     */
    public $users;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * Constructor method for Get_Credit_Credit
     * @uses Get_Credit_Credit::setAmount()
     * @uses Get_Credit_Credit::setThreshold()
     * @uses Get_Credit_Credit::setUsers()
     * @uses Get_Credit_Credit::setCurrency()
     * @param float $amount
     * @param float $threshold
     * @param float $users
     * @param string $currency
     */
    public function __construct($amount = null, $threshold = null, $users = null, $currency = null)
    {
        $this
            ->setAmount($amount)
            ->setThreshold($threshold)
            ->setUsers($users)
            ->setCurrency($currency);
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Test\StructType\Get_Credit_Credit
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get threshold value
     * @return float|null
     */
    public function getThreshold()
    {
        return $this->threshold;
    }
    /**
     * Set threshold value
     * @param float $threshold
     * @return \SubregSDK\Test\StructType\Get_Credit_Credit
     */
    public function setThreshold($threshold = null)
    {
        // validation for constraint: float
        if (!is_null($threshold) && !(is_float($threshold) || is_numeric($threshold))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($threshold, true), gettype($threshold)), __LINE__);
        }
        $this->threshold = $threshold;
        return $this;
    }
    /**
     * Get users value
     * @return float|null
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * Set users value
     * @param float $users
     * @return \SubregSDK\Test\StructType\Get_Credit_Credit
     */
    public function setUsers($users = null)
    {
        // validation for constraint: float
        if (!is_null($users) && !(is_float($users) || is_numeric($users))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($users, true), gettype($users)), __LINE__);
        }
        $this->users = $users;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Test\StructType\Get_Credit_Credit
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
}
