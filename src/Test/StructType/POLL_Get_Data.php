<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for POLL_Get_Data StructType
 * @subpackage Structs
 */
class POLL_Get_Data extends AbstractStructBase
{
    /**
     * The count
     * @var int
     */
    public $count;
    /**
     * The date
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $date;
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $id;
    /**
     * The orderid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $orderid;
    /**
     * The orderstatus
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $orderstatus;
    /**
     * The message
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $message;
    /**
     * The errorcode
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $errorcode;
    /**
     * Constructor method for POLL_Get_Data
     * @uses POLL_Get_Data::setCount()
     * @uses POLL_Get_Data::setDate()
     * @uses POLL_Get_Data::setId()
     * @uses POLL_Get_Data::setOrderid()
     * @uses POLL_Get_Data::setOrderstatus()
     * @uses POLL_Get_Data::setMessage()
     * @uses POLL_Get_Data::setErrorcode()
     * @param int $count
     * @param string $date
     * @param int $id
     * @param int $orderid
     * @param string $orderstatus
     * @param string $message
     * @param string $errorcode
     */
    public function __construct($count = null, $date = null, $id = null, $orderid = null, $orderstatus = null, $message = null, $errorcode = null)
    {
        $this
            ->setCount($count)
            ->setDate($date)
            ->setId($id)
            ->setOrderid($orderid)
            ->setOrderstatus($orderstatus)
            ->setMessage($message)
            ->setErrorcode($errorcode);
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setCount($count = null)
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        return $this;
    }
    /**
     * Get date value
     * @return string|null
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Set date value
     * @param string $date
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setDate($date = null)
    {
        // validation for constraint: string
        if (!is_null($date) && !is_string($date)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($date, true), gettype($date)), __LINE__);
        }
        $this->date = $date;
        return $this;
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get orderid value
     * @return int|null
     */
    public function getOrderid()
    {
        return $this->orderid;
    }
    /**
     * Set orderid value
     * @param int $orderid
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setOrderid($orderid = null)
    {
        // validation for constraint: int
        if (!is_null($orderid) && !(is_int($orderid) || ctype_digit($orderid))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($orderid, true), gettype($orderid)), __LINE__);
        }
        $this->orderid = $orderid;
        return $this;
    }
    /**
     * Get orderstatus value
     * @return string|null
     */
    public function getOrderstatus()
    {
        return $this->orderstatus;
    }
    /**
     * Set orderstatus value
     * @param string $orderstatus
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setOrderstatus($orderstatus = null)
    {
        // validation for constraint: string
        if (!is_null($orderstatus) && !is_string($orderstatus)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($orderstatus, true), gettype($orderstatus)), __LINE__);
        }
        $this->orderstatus = $orderstatus;
        return $this;
    }
    /**
     * Get message value
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }
    /**
     * Set message value
     * @param string $message
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setMessage($message = null)
    {
        // validation for constraint: string
        if (!is_null($message) && !is_string($message)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($message, true), gettype($message)), __LINE__);
        }
        $this->message = $message;
        return $this;
    }
    /**
     * Get errorcode value
     * @return string|null
     */
    public function getErrorcode()
    {
        return $this->errorcode;
    }
    /**
     * Set errorcode value
     * @param string $errorcode
     * @return \SubregSDK\Test\StructType\POLL_Get_Data
     */
    public function setErrorcode($errorcode = null)
    {
        // validation for constraint: string
        if (!is_null($errorcode) && !is_string($errorcode)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($errorcode, true), gettype($errorcode)), __LINE__);
        }
        $this->errorcode = $errorcode;
        return $this;
    }
}
