<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Contact_Contact StructType
 * @subpackage Structs
 */
class Info_Contact_Contact extends AbstractStructBase
{
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * Constructor method for Info_Contact_Contact
     * @uses Info_Contact_Contact::setId()
     * @param string $id
     */
    public function __construct($id = null)
    {
        $this
            ->setId($id);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Test\StructType\Info_Contact_Contact
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
}
