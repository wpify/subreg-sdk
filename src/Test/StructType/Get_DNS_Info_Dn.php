<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_DNS_Info_Dn StructType
 * @subpackage Structs
 */
class Get_DNS_Info_Dn extends AbstractStructBase
{
    /**
     * The nameserver
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $nameserver;
    /**
     * The anydata
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Get_DNS_Info_Anydata[]
     */
    public $anydata;
    /**
     * The nslist
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $nslist;
    /**
     * The soaid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $soaid;
    /**
     * Constructor method for Get_DNS_Info_Dn
     * @uses Get_DNS_Info_Dn::setNameserver()
     * @uses Get_DNS_Info_Dn::setAnydata()
     * @uses Get_DNS_Info_Dn::setNslist()
     * @uses Get_DNS_Info_Dn::setSoaid()
     * @param string $nameserver
     * @param \SubregSDK\Test\StructType\Get_DNS_Info_Anydata[] $anydata
     * @param string[] $nslist
     * @param string $soaid
     */
    public function __construct($nameserver = null, array $anydata = array(), array $nslist = array(), $soaid = null)
    {
        $this
            ->setNameserver($nameserver)
            ->setAnydata($anydata)
            ->setNslist($nslist)
            ->setSoaid($soaid);
    }
    /**
     * Get nameserver value
     * @return string|null
     */
    public function getNameserver()
    {
        return $this->nameserver;
    }
    /**
     * Set nameserver value
     * @param string $nameserver
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function setNameserver($nameserver = null)
    {
        // validation for constraint: string
        if (!is_null($nameserver) && !is_string($nameserver)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nameserver, true), gettype($nameserver)), __LINE__);
        }
        $this->nameserver = $nameserver;
        return $this;
    }
    /**
     * Get anydata value
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Anydata[]|null
     */
    public function getAnydata()
    {
        return $this->anydata;
    }
    /**
     * This method is responsible for validating the values passed to the setAnydata method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAnydata method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAnydataForArrayConstraintsFromSetAnydata(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_DNS_Info_DnAnydataItem) {
            // validation for constraint: itemType
            if (!$get_DNS_Info_DnAnydataItem instanceof \SubregSDK\Test\StructType\Get_DNS_Info_Anydata) {
                $invalidValues[] = is_object($get_DNS_Info_DnAnydataItem) ? get_class($get_DNS_Info_DnAnydataItem) : sprintf('%s(%s)', gettype($get_DNS_Info_DnAnydataItem), var_export($get_DNS_Info_DnAnydataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The anydata property can only contain items of type \SubregSDK\Test\StructType\Get_DNS_Info_Anydata, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set anydata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_DNS_Info_Anydata[] $anydata
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function setAnydata(array $anydata = array())
    {
        // validation for constraint: array
        if ('' !== ($anydataArrayErrorMessage = self::validateAnydataForArrayConstraintsFromSetAnydata($anydata))) {
            throw new \InvalidArgumentException($anydataArrayErrorMessage, __LINE__);
        }
        $this->anydata = $anydata;
        return $this;
    }
    /**
     * Add item to anydata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_DNS_Info_Anydata $item
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function addToAnydata(\SubregSDK\Test\StructType\Get_DNS_Info_Anydata $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Get_DNS_Info_Anydata) {
            throw new \InvalidArgumentException(sprintf('The anydata property can only contain items of type \SubregSDK\Test\StructType\Get_DNS_Info_Anydata, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->anydata[] = $item;
        return $this;
    }
    /**
     * Get nslist value
     * @return string[]|null
     */
    public function getNslist()
    {
        return $this->nslist;
    }
    /**
     * This method is responsible for validating the values passed to the setNslist method
     * This method is willingly generated in order to preserve the one-line inline validation within the setNslist method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateNslistForArrayConstraintsFromSetNslist(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_DNS_Info_DnNslistItem) {
            // validation for constraint: itemType
            if (!is_string($get_DNS_Info_DnNslistItem)) {
                $invalidValues[] = is_object($get_DNS_Info_DnNslistItem) ? get_class($get_DNS_Info_DnNslistItem) : sprintf('%s(%s)', gettype($get_DNS_Info_DnNslistItem), var_export($get_DNS_Info_DnNslistItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The nslist property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set nslist value
     * @throws \InvalidArgumentException
     * @param string[] $nslist
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function setNslist(array $nslist = array())
    {
        // validation for constraint: array
        if ('' !== ($nslistArrayErrorMessage = self::validateNslistForArrayConstraintsFromSetNslist($nslist))) {
            throw new \InvalidArgumentException($nslistArrayErrorMessage, __LINE__);
        }
        $this->nslist = $nslist;
        return $this;
    }
    /**
     * Add item to nslist value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function addToNslist($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The nslist property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->nslist[] = $item;
        return $this;
    }
    /**
     * Get soaid value
     * @return string|null
     */
    public function getSoaid()
    {
        return $this->soaid;
    }
    /**
     * Set soaid value
     * @param string $soaid
     * @return \SubregSDK\Test\StructType\Get_DNS_Info_Dn
     */
    public function setSoaid($soaid = null)
    {
        // validation for constraint: string
        if (!is_null($soaid) && !is_string($soaid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($soaid, true), gettype($soaid)), __LINE__);
        }
        $this->soaid = $soaid;
        return $this;
    }
}
