<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Login_Container StructType
 * @subpackage Structs
 */
class Login_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Login_Response
     */
    public $response;
    /**
     * Constructor method for Login_Container
     * @uses Login_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Login_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Login_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Login_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Login_Response $response
     * @return \SubregSDK\Test\StructType\Login_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Login_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
