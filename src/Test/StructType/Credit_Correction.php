<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Credit_Correction StructType
 * @subpackage Structs
 */
class Credit_Correction extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The username
     * @var string
     */
    public $username;
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The reason
     * @var string
     */
    public $reason;
    /**
     * Constructor method for Credit_Correction
     * @uses Credit_Correction::setSsid()
     * @uses Credit_Correction::setUsername()
     * @uses Credit_Correction::setAmount()
     * @uses Credit_Correction::setReason()
     * @param string $ssid
     * @param string $username
     * @param float $amount
     * @param string $reason
     */
    public function __construct($ssid = null, $username = null, $amount = null, $reason = null)
    {
        $this
            ->setSsid($ssid)
            ->setUsername($username)
            ->setAmount($amount)
            ->setReason($reason);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Credit_Correction
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get username value
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }
    /**
     * Set username value
     * @param string $username
     * @return \SubregSDK\Test\StructType\Credit_Correction
     */
    public function setUsername($username = null)
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($username, true), gettype($username)), __LINE__);
        }
        $this->username = $username;
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Test\StructType\Credit_Correction
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get reason value
     * @return string|null
     */
    public function getReason()
    {
        return $this->reason;
    }
    /**
     * Set reason value
     * @param string $reason
     * @return \SubregSDK\Test\StructType\Credit_Correction
     */
    public function setReason($reason = null)
    {
        // validation for constraint: string
        if (!is_null($reason) && !is_string($reason)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($reason, true), gettype($reason)), __LINE__);
        }
        $this->reason = $reason;
        return $this;
    }
}
