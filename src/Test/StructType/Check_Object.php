<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Object StructType
 * @subpackage Structs
 */
class Check_Object extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The object
     * @var string
     */
    public $object;
    /**
     * The id
     * @var string
     */
    public $id;
    /**
     * Constructor method for Check_Object
     * @uses Check_Object::setSsid()
     * @uses Check_Object::setObject()
     * @uses Check_Object::setId()
     * @param string $ssid
     * @param string $object
     * @param string $id
     */
    public function __construct($ssid = null, $object = null, $id = null)
    {
        $this
            ->setSsid($ssid)
            ->setObject($object)
            ->setId($id);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Check_Object
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get object value
     * @return string|null
     */
    public function getObject()
    {
        return $this->object;
    }
    /**
     * Set object value
     * @param string $object
     * @return \SubregSDK\Test\StructType\Check_Object
     */
    public function setObject($object = null)
    {
        // validation for constraint: string
        if (!is_null($object) && !is_string($object)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($object, true), gettype($object)), __LINE__);
        }
        $this->object = $object;
        return $this;
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Test\StructType\Check_Object
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
}
