<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Domain_Params StructType
 * @subpackage Structs
 */
class Check_Domain_Params extends AbstractStructBase
{
    /**
     * The lang_info
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $lang_info;
    /**
     * Constructor method for Check_Domain_Params
     * @uses Check_Domain_Params::setLang_info()
     * @param string $lang_info
     */
    public function __construct($lang_info = null)
    {
        $this
            ->setLang_info($lang_info);
    }
    /**
     * Get lang_info value
     * @return string|null
     */
    public function getLang_info()
    {
        return $this->lang_info;
    }
    /**
     * Set lang_info value
     * @param string $lang_info
     * @return \SubregSDK\Test\StructType\Check_Domain_Params
     */
    public function setLang_info($lang_info = null)
    {
        // validation for constraint: string
        if (!is_null($lang_info) && !is_string($lang_info)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($lang_info, true), gettype($lang_info)), __LINE__);
        }
        $this->lang_info = $lang_info;
        return $this;
    }
}
