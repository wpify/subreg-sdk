<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Add_DNS_Zone_Container StructType
 * @subpackage Structs
 */
class Add_DNS_Zone_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Add_DNS_Zone_Response
     */
    public $response;
    /**
     * Constructor method for Add_DNS_Zone_Container
     * @uses Add_DNS_Zone_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Add_DNS_Zone_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Add_DNS_Zone_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Add_DNS_Zone_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Add_DNS_Zone_Response $response
     * @return \SubregSDK\Test\StructType\Add_DNS_Zone_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Add_DNS_Zone_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
