<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Contact StructType
 * @subpackage Structs
 */
class Make_Order_Contact extends AbstractStructBase
{
    /**
     * The id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $id;
    /**
     * The regid
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $regid;
    /**
     * The new
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Make_Order_Contact_New
     */
    public $new;
    /**
     * Constructor method for Make_Order_Contact
     * @uses Make_Order_Contact::setId()
     * @uses Make_Order_Contact::setRegid()
     * @uses Make_Order_Contact::setNew()
     * @param string $id
     * @param string $regid
     * @param \SubregSDK\Test\StructType\Make_Order_Contact_New $new
     */
    public function __construct($id = null, $regid = null, \SubregSDK\Test\StructType\Make_Order_Contact_New $new = null)
    {
        $this
            ->setId($id)
            ->setRegid($regid)
            ->setNew($new);
    }
    /**
     * Get id value
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param string $id
     * @return \SubregSDK\Test\StructType\Make_Order_Contact
     */
    public function setId($id = null)
    {
        // validation for constraint: string
        if (!is_null($id) && !is_string($id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get regid value
     * @return string|null
     */
    public function getRegid()
    {
        return $this->regid;
    }
    /**
     * Set regid value
     * @param string $regid
     * @return \SubregSDK\Test\StructType\Make_Order_Contact
     */
    public function setRegid($regid = null)
    {
        // validation for constraint: string
        if (!is_null($regid) && !is_string($regid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($regid, true), gettype($regid)), __LINE__);
        }
        $this->regid = $regid;
        return $this;
    }
    /**
     * Get new value
     * @return \SubregSDK\Test\StructType\Make_Order_Contact_New|null
     */
    public function getNew()
    {
        return $this->new;
    }
    /**
     * Set new value
     * @param \SubregSDK\Test\StructType\Make_Order_Contact_New $new
     * @return \SubregSDK\Test\StructType\Make_Order_Contact
     */
    public function setNew(\SubregSDK\Test\StructType\Make_Order_Contact_New $new = null)
    {
        $this->new = $new;
        return $this;
    }
}
