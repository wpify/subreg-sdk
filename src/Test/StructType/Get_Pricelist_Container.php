<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Pricelist_Container StructType
 * @subpackage Structs
 */
class Get_Pricelist_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Get_Pricelist_Response
     */
    public $response;
    /**
     * Constructor method for Get_Pricelist_Container
     * @uses Get_Pricelist_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Get_Pricelist_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Get_Pricelist_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Get_Pricelist_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Get_Pricelist_Response $response
     * @return \SubregSDK\Test\StructType\Get_Pricelist_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Get_Pricelist_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
