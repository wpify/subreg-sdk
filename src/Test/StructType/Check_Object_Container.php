<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Object_Container StructType
 * @subpackage Structs
 */
class Check_Object_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Check_Object_Response
     */
    public $response;
    /**
     * Constructor method for Check_Object_Container
     * @uses Check_Object_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Check_Object_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Check_Object_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Check_Object_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Check_Object_Response $response
     * @return \SubregSDK\Test\StructType\Check_Object_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Check_Object_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
