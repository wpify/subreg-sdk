<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Accountings_Response StructType
 * @subpackage Structs
 */
class Get_Accountings_Response extends AbstractStructBase
{
    /**
     * The status
     * @var string
     */
    public $status;
    /**
     * The data
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Get_Accountings_Data
     */
    public $data;
    /**
     * The error
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Error_Info
     */
    public $error;
    /**
     * Constructor method for Get_Accountings_Response
     * @uses Get_Accountings_Response::setStatus()
     * @uses Get_Accountings_Response::setData()
     * @uses Get_Accountings_Response::setError()
     * @param string $status
     * @param \SubregSDK\Test\StructType\Get_Accountings_Data $data
     * @param \SubregSDK\Test\StructType\Error_Info $error
     */
    public function __construct($status = null, \SubregSDK\Test\StructType\Get_Accountings_Data $data = null, \SubregSDK\Test\StructType\Error_Info $error = null)
    {
        $this
            ->setStatus($status)
            ->setData($data)
            ->setError($error);
    }
    /**
     * Get status value
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set status value
     * @param string $status
     * @return \SubregSDK\Test\StructType\Get_Accountings_Response
     */
    public function setStatus($status = null)
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($status, true), gettype($status)), __LINE__);
        }
        $this->status = $status;
        return $this;
    }
    /**
     * Get data value
     * @return \SubregSDK\Test\StructType\Get_Accountings_Data|null
     */
    public function getData()
    {
        return $this->data;
    }
    /**
     * Set data value
     * @param \SubregSDK\Test\StructType\Get_Accountings_Data $data
     * @return \SubregSDK\Test\StructType\Get_Accountings_Response
     */
    public function setData(\SubregSDK\Test\StructType\Get_Accountings_Data $data = null)
    {
        $this->data = $data;
        return $this;
    }
    /**
     * Get error value
     * @return \SubregSDK\Test\StructType\Error_Info|null
     */
    public function getError()
    {
        return $this->error;
    }
    /**
     * Set error value
     * @param \SubregSDK\Test\StructType\Error_Info $error
     * @return \SubregSDK\Test\StructType\Get_Accountings_Response
     */
    public function setError(\SubregSDK\Test\StructType\Error_Info $error = null)
    {
        $this->error = $error;
        return $this;
    }
}
