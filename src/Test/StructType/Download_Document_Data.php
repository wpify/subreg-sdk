<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Download_Document_Data StructType
 * @subpackage Structs
 */
class Download_Document_Data extends AbstractStructBase
{
    /**
     * The id
     * @var int
     */
    public $id;
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $type;
    /**
     * The filetype
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $filetype;
    /**
     * The account
     * @var string
     */
    public $account;
    /**
     * The document
     * @var string
     */
    public $document;
    /**
     * Constructor method for Download_Document_Data
     * @uses Download_Document_Data::setId()
     * @uses Download_Document_Data::setName()
     * @uses Download_Document_Data::setType()
     * @uses Download_Document_Data::setFiletype()
     * @uses Download_Document_Data::setAccount()
     * @uses Download_Document_Data::setDocument()
     * @param int $id
     * @param string $name
     * @param string $type
     * @param string $filetype
     * @param string $account
     * @param string $document
     */
    public function __construct($id = null, $name = null, $type = null, $filetype = null, $account = null, $document = null)
    {
        $this
            ->setId($id)
            ->setName($name)
            ->setType($type)
            ->setFiletype($filetype)
            ->setAccount($account)
            ->setDocument($document);
    }
    /**
     * Get id value
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Set id value
     * @param int $id
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setId($id = null)
    {
        // validation for constraint: int
        if (!is_null($id) && !(is_int($id) || ctype_digit($id))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($id, true), gettype($id)), __LINE__);
        }
        $this->id = $id;
        return $this;
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get type value
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * Set type value
     * @param string $type
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setType($type = null)
    {
        // validation for constraint: string
        if (!is_null($type) && !is_string($type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($type, true), gettype($type)), __LINE__);
        }
        $this->type = $type;
        return $this;
    }
    /**
     * Get filetype value
     * @return string|null
     */
    public function getFiletype()
    {
        return $this->filetype;
    }
    /**
     * Set filetype value
     * @param string $filetype
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setFiletype($filetype = null)
    {
        // validation for constraint: string
        if (!is_null($filetype) && !is_string($filetype)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($filetype, true), gettype($filetype)), __LINE__);
        }
        $this->filetype = $filetype;
        return $this;
    }
    /**
     * Get account value
     * @return string|null
     */
    public function getAccount()
    {
        return $this->account;
    }
    /**
     * Set account value
     * @param string $account
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setAccount($account = null)
    {
        // validation for constraint: string
        if (!is_null($account) && !is_string($account)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($account, true), gettype($account)), __LINE__);
        }
        $this->account = $account;
        return $this;
    }
    /**
     * Get document value
     * @return string|null
     */
    public function getDocument()
    {
        return $this->document;
    }
    /**
     * Set document value
     * @param string $document
     * @return \SubregSDK\Test\StructType\Download_Document_Data
     */
    public function setDocument($document = null)
    {
        // validation for constraint: string
        if (!is_null($document) && !is_string($document)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($document, true), gettype($document)), __LINE__);
        }
        $this->document = $document;
        return $this;
    }
}
