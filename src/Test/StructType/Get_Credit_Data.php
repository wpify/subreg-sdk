<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_Credit_Data StructType
 * @subpackage Structs
 */
class Get_Credit_Data extends AbstractStructBase
{
    /**
     * The credit
     * @var \SubregSDK\Test\StructType\Get_Credit_Credit
     */
    public $credit;
    /**
     * Constructor method for Get_Credit_Data
     * @uses Get_Credit_Data::setCredit()
     * @param \SubregSDK\Test\StructType\Get_Credit_Credit $credit
     */
    public function __construct(\SubregSDK\Test\StructType\Get_Credit_Credit $credit = null)
    {
        $this
            ->setCredit($credit);
    }
    /**
     * Get credit value
     * @return \SubregSDK\Test\StructType\Get_Credit_Credit|null
     */
    public function getCredit()
    {
        return $this->credit;
    }
    /**
     * Set credit value
     * @param \SubregSDK\Test\StructType\Get_Credit_Credit $credit
     * @return \SubregSDK\Test\StructType\Get_Credit_Data
     */
    public function setCredit(\SubregSDK\Test\StructType\Get_Credit_Credit $credit = null)
    {
        $this->credit = $credit;
        return $this;
    }
}
