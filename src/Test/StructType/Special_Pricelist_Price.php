<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Special_Pricelist_Price StructType
 * @subpackage Structs
 */
class Special_Pricelist_Price extends AbstractStructBase
{
    /**
     * The register
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $register;
    /**
     * The renew
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $renew;
    /**
     * The transfer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var float
     */
    public $transfer;
    /**
     * Constructor method for Special_Pricelist_Price
     * @uses Special_Pricelist_Price::setRegister()
     * @uses Special_Pricelist_Price::setRenew()
     * @uses Special_Pricelist_Price::setTransfer()
     * @param float $register
     * @param float $renew
     * @param float $transfer
     */
    public function __construct($register = null, $renew = null, $transfer = null)
    {
        $this
            ->setRegister($register)
            ->setRenew($renew)
            ->setTransfer($transfer);
    }
    /**
     * Get register value
     * @return float|null
     */
    public function getRegister()
    {
        return $this->register;
    }
    /**
     * Set register value
     * @param float $register
     * @return \SubregSDK\Test\StructType\Special_Pricelist_Price
     */
    public function setRegister($register = null)
    {
        // validation for constraint: float
        if (!is_null($register) && !(is_float($register) || is_numeric($register))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($register, true), gettype($register)), __LINE__);
        }
        $this->register = $register;
        return $this;
    }
    /**
     * Get renew value
     * @return float|null
     */
    public function getRenew()
    {
        return $this->renew;
    }
    /**
     * Set renew value
     * @param float $renew
     * @return \SubregSDK\Test\StructType\Special_Pricelist_Price
     */
    public function setRenew($renew = null)
    {
        // validation for constraint: float
        if (!is_null($renew) && !(is_float($renew) || is_numeric($renew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($renew, true), gettype($renew)), __LINE__);
        }
        $this->renew = $renew;
        return $this;
    }
    /**
     * Get transfer value
     * @return float|null
     */
    public function getTransfer()
    {
        return $this->transfer;
    }
    /**
     * Set transfer value
     * @param float $transfer
     * @return \SubregSDK\Test\StructType\Special_Pricelist_Price
     */
    public function setTransfer($transfer = null)
    {
        // validation for constraint: float
        if (!is_null($transfer) && !(is_float($transfer) || is_numeric($transfer))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($transfer, true), gettype($transfer)), __LINE__);
        }
        $this->transfer = $transfer;
        return $this;
    }
}
