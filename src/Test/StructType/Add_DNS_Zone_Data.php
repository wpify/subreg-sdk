<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Add_DNS_Zone_Data StructType
 * @subpackage Structs
 */
class Add_DNS_Zone_Data extends AbstractStructBase
{
}
