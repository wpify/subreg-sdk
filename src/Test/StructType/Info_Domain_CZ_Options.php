<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Domain_CZ_Options StructType
 * @subpackage Structs
 */
class Info_Domain_CZ_Options extends AbstractStructBase
{
    /**
     * The nsset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $nsset;
    /**
     * The keyset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $keyset;
    /**
     * The dsdata
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata
     */
    public $dsdata;
    /**
     * The keygroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $keygroup;
    /**
     * The quarantined
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $quarantined;
    /**
     * Constructor method for Info_Domain_CZ_Options
     * @uses Info_Domain_CZ_Options::setNsset()
     * @uses Info_Domain_CZ_Options::setKeyset()
     * @uses Info_Domain_CZ_Options::setDsdata()
     * @uses Info_Domain_CZ_Options::setKeygroup()
     * @uses Info_Domain_CZ_Options::setQuarantined()
     * @param string $nsset
     * @param string $keyset
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata $dsdata
     * @param string $keygroup
     * @param string $quarantined
     */
    public function __construct($nsset = null, $keyset = null, \SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata $dsdata = null, $keygroup = null, $quarantined = null)
    {
        $this
            ->setNsset($nsset)
            ->setKeyset($keyset)
            ->setDsdata($dsdata)
            ->setKeygroup($keygroup)
            ->setQuarantined($quarantined);
    }
    /**
     * Get nsset value
     * @return string|null
     */
    public function getNsset()
    {
        return $this->nsset;
    }
    /**
     * Set nsset value
     * @param string $nsset
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Options
     */
    public function setNsset($nsset = null)
    {
        // validation for constraint: string
        if (!is_null($nsset) && !is_string($nsset)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nsset, true), gettype($nsset)), __LINE__);
        }
        $this->nsset = $nsset;
        return $this;
    }
    /**
     * Get keyset value
     * @return string|null
     */
    public function getKeyset()
    {
        return $this->keyset;
    }
    /**
     * Set keyset value
     * @param string $keyset
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Options
     */
    public function setKeyset($keyset = null)
    {
        // validation for constraint: string
        if (!is_null($keyset) && !is_string($keyset)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($keyset, true), gettype($keyset)), __LINE__);
        }
        $this->keyset = $keyset;
        return $this;
    }
    /**
     * Get dsdata value
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata|null
     */
    public function getDsdata()
    {
        return $this->dsdata;
    }
    /**
     * Set dsdata value
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata $dsdata
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Options
     */
    public function setDsdata(\SubregSDK\Test\StructType\Info_Domain_CZ_Dsdata $dsdata = null)
    {
        $this->dsdata = $dsdata;
        return $this;
    }
    /**
     * Get keygroup value
     * @return string|null
     */
    public function getKeygroup()
    {
        return $this->keygroup;
    }
    /**
     * Set keygroup value
     * @param string $keygroup
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Options
     */
    public function setKeygroup($keygroup = null)
    {
        // validation for constraint: string
        if (!is_null($keygroup) && !is_string($keygroup)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($keygroup, true), gettype($keygroup)), __LINE__);
        }
        $this->keygroup = $keygroup;
        return $this;
    }
    /**
     * Get quarantined value
     * @return string|null
     */
    public function getQuarantined()
    {
        return $this->quarantined;
    }
    /**
     * Set quarantined value
     * @param string $quarantined
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Options
     */
    public function setQuarantined($quarantined = null)
    {
        // validation for constraint: string
        if (!is_null($quarantined) && !is_string($quarantined)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($quarantined, true), gettype($quarantined)), __LINE__);
        }
        $this->quarantined = $quarantined;
        return $this;
    }
}
