<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Set_Prices_Container StructType
 * @subpackage Structs
 */
class Set_Prices_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Set_Prices_Response
     */
    public $response;
    /**
     * Constructor method for Set_Prices_Container
     * @uses Set_Prices_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Set_Prices_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Set_Prices_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Set_Prices_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Set_Prices_Response $response
     * @return \SubregSDK\Test\StructType\Set_Prices_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Set_Prices_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
