<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Domains_List StructType
 * @subpackage Structs
 */
class Domains_List extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The for_user
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $for_user;
    /**
     * Constructor method for Domains_List
     * @uses Domains_List::setSsid()
     * @uses Domains_List::setFor_user()
     * @param string $ssid
     * @param int $for_user
     */
    public function __construct($ssid = null, $for_user = null)
    {
        $this
            ->setSsid($ssid)
            ->setFor_user($for_user);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Domains_List
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get for_user value
     * @return int|null
     */
    public function getFor_user()
    {
        return $this->for_user;
    }
    /**
     * Set for_user value
     * @param int $for_user
     * @return \SubregSDK\Test\StructType\Domains_List
     */
    public function setFor_user($for_user = null)
    {
        // validation for constraint: int
        if (!is_null($for_user) && !(is_int($for_user) || ctype_digit($for_user))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($for_user, true), gettype($for_user)), __LINE__);
        }
        $this->for_user = $for_user;
        return $this;
    }
}
