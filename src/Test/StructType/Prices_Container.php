<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Prices_Container StructType
 * @subpackage Structs
 */
class Prices_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Prices_Response
     */
    public $response;
    /**
     * Constructor method for Prices_Container
     * @uses Prices_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Prices_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Prices_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Prices_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Prices_Response $response
     * @return \SubregSDK\Test\StructType\Prices_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Prices_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
