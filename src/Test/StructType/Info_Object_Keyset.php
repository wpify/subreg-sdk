<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Keyset StructType
 * @subpackage Structs
 */
class Info_Object_Keyset extends AbstractStructBase
{
    /**
     * The tech
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $tech;
    /**
     * The dnskey
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Object_Dnskey[]
     */
    public $dnskey;
    /**
     * The clID
     * @var string
     */
    public $clID;
    /**
     * Constructor method for Info_Object_Keyset
     * @uses Info_Object_Keyset::setTech()
     * @uses Info_Object_Keyset::setDnskey()
     * @uses Info_Object_Keyset::setClID()
     * @param string $tech
     * @param \SubregSDK\Test\StructType\Info_Object_Dnskey[] $dnskey
     * @param string $clID
     */
    public function __construct($tech = null, array $dnskey = array(), $clID = null)
    {
        $this
            ->setTech($tech)
            ->setDnskey($dnskey)
            ->setClID($clID);
    }
    /**
     * Get tech value
     * @return string|null
     */
    public function getTech()
    {
        return $this->tech;
    }
    /**
     * Set tech value
     * @param string $tech
     * @return \SubregSDK\Test\StructType\Info_Object_Keyset
     */
    public function setTech($tech = null)
    {
        // validation for constraint: string
        if (!is_null($tech) && !is_string($tech)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tech, true), gettype($tech)), __LINE__);
        }
        $this->tech = $tech;
        return $this;
    }
    /**
     * Get dnskey value
     * @return \SubregSDK\Test\StructType\Info_Object_Dnskey[]|null
     */
    public function getDnskey()
    {
        return $this->dnskey;
    }
    /**
     * This method is responsible for validating the values passed to the setDnskey method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDnskey method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDnskeyForArrayConstraintsFromSetDnskey(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Object_KeysetDnskeyItem) {
            // validation for constraint: itemType
            if (!$info_Object_KeysetDnskeyItem instanceof \SubregSDK\Test\StructType\Info_Object_Dnskey) {
                $invalidValues[] = is_object($info_Object_KeysetDnskeyItem) ? get_class($info_Object_KeysetDnskeyItem) : sprintf('%s(%s)', gettype($info_Object_KeysetDnskeyItem), var_export($info_Object_KeysetDnskeyItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dnskey property can only contain items of type \SubregSDK\Test\StructType\Info_Object_Dnskey, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set dnskey value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Object_Dnskey[] $dnskey
     * @return \SubregSDK\Test\StructType\Info_Object_Keyset
     */
    public function setDnskey(array $dnskey = array())
    {
        // validation for constraint: array
        if ('' !== ($dnskeyArrayErrorMessage = self::validateDnskeyForArrayConstraintsFromSetDnskey($dnskey))) {
            throw new \InvalidArgumentException($dnskeyArrayErrorMessage, __LINE__);
        }
        $this->dnskey = $dnskey;
        return $this;
    }
    /**
     * Add item to dnskey value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Object_Dnskey $item
     * @return \SubregSDK\Test\StructType\Info_Object_Keyset
     */
    public function addToDnskey(\SubregSDK\Test\StructType\Info_Object_Dnskey $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Object_Dnskey) {
            throw new \InvalidArgumentException(sprintf('The dnskey property can only contain items of type \SubregSDK\Test\StructType\Info_Object_Dnskey, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dnskey[] = $item;
        return $this;
    }
    /**
     * Get clID value
     * @return string|null
     */
    public function getClID()
    {
        return $this->clID;
    }
    /**
     * Set clID value
     * @param string $clID
     * @return \SubregSDK\Test\StructType\Info_Object_Keyset
     */
    public function setClID($clID = null)
    {
        // validation for constraint: string
        if (!is_null($clID) && !is_string($clID)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($clID, true), gettype($clID)), __LINE__);
        }
        $this->clID = $clID;
        return $this;
    }
}
