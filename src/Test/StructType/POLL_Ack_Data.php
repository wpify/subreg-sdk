<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for POLL_Ack_Data StructType
 * @subpackage Structs
 */
class POLL_Ack_Data extends AbstractStructBase
{
}
