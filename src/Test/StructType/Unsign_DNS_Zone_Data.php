<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Unsign_DNS_Zone_Data StructType
 * @subpackage Structs
 */
class Unsign_DNS_Zone_Data extends AbstractStructBase
{
}
