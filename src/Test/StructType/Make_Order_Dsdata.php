<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Make_Order_Dsdata StructType
 * @subpackage Structs
 */
class Make_Order_Dsdata extends AbstractStructBase
{
    /**
     * The tag
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $tag;
    /**
     * The alg
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $alg;
    /**
     * The digest_type
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $digest_type;
    /**
     * The digest
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $digest;
    /**
     * Constructor method for Make_Order_Dsdata
     * @uses Make_Order_Dsdata::setTag()
     * @uses Make_Order_Dsdata::setAlg()
     * @uses Make_Order_Dsdata::setDigest_type()
     * @uses Make_Order_Dsdata::setDigest()
     * @param string $tag
     * @param string $alg
     * @param string $digest_type
     * @param string $digest
     */
    public function __construct($tag = null, $alg = null, $digest_type = null, $digest = null)
    {
        $this
            ->setTag($tag)
            ->setAlg($alg)
            ->setDigest_type($digest_type)
            ->setDigest($digest);
    }
    /**
     * Get tag value
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }
    /**
     * Set tag value
     * @param string $tag
     * @return \SubregSDK\Test\StructType\Make_Order_Dsdata
     */
    public function setTag($tag = null)
    {
        // validation for constraint: string
        if (!is_null($tag) && !is_string($tag)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tag, true), gettype($tag)), __LINE__);
        }
        $this->tag = $tag;
        return $this;
    }
    /**
     * Get alg value
     * @return string|null
     */
    public function getAlg()
    {
        return $this->alg;
    }
    /**
     * Set alg value
     * @param string $alg
     * @return \SubregSDK\Test\StructType\Make_Order_Dsdata
     */
    public function setAlg($alg = null)
    {
        // validation for constraint: string
        if (!is_null($alg) && !is_string($alg)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($alg, true), gettype($alg)), __LINE__);
        }
        $this->alg = $alg;
        return $this;
    }
    /**
     * Get digest_type value
     * @return string|null
     */
    public function getDigest_type()
    {
        return $this->digest_type;
    }
    /**
     * Set digest_type value
     * @param string $digest_type
     * @return \SubregSDK\Test\StructType\Make_Order_Dsdata
     */
    public function setDigest_type($digest_type = null)
    {
        // validation for constraint: string
        if (!is_null($digest_type) && !is_string($digest_type)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($digest_type, true), gettype($digest_type)), __LINE__);
        }
        $this->digest_type = $digest_type;
        return $this;
    }
    /**
     * Get digest value
     * @return string|null
     */
    public function getDigest()
    {
        return $this->digest;
    }
    /**
     * Set digest value
     * @param string $digest
     * @return \SubregSDK\Test\StructType\Make_Order_Dsdata
     */
    public function setDigest($digest = null)
    {
        // validation for constraint: string
        if (!is_null($digest) && !is_string($digest)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($digest, true), gettype($digest)), __LINE__);
        }
        $this->digest = $digest;
        return $this;
    }
}
