<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Object_Ns StructType
 * @subpackage Structs
 */
class Info_Object_Ns extends AbstractStructBase
{
    /**
     * The host
     * @var string
     */
    public $host;
    /**
     * The ip
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ip;
    /**
     * Constructor method for Info_Object_Ns
     * @uses Info_Object_Ns::setHost()
     * @uses Info_Object_Ns::setIp()
     * @param string $host
     * @param string $ip
     */
    public function __construct($host = null, $ip = null)
    {
        $this
            ->setHost($host)
            ->setIp($ip);
    }
    /**
     * Get host value
     * @return string|null
     */
    public function getHost()
    {
        return $this->host;
    }
    /**
     * Set host value
     * @param string $host
     * @return \SubregSDK\Test\StructType\Info_Object_Ns
     */
    public function setHost($host = null)
    {
        // validation for constraint: string
        if (!is_null($host) && !is_string($host)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($host, true), gettype($host)), __LINE__);
        }
        $this->host = $host;
        return $this;
    }
    /**
     * Get ip value
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }
    /**
     * Set ip value
     * @param string $ip
     * @return \SubregSDK\Test\StructType\Info_Object_Ns
     */
    public function setIp($ip = null)
    {
        // validation for constraint: string
        if (!is_null($ip) && !is_string($ip)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ip, true), gettype($ip)), __LINE__);
        }
        $this->ip = $ip;
        return $this;
    }
}
