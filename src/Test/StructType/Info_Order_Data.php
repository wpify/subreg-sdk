<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Order_Data StructType
 * @subpackage Structs
 */
class Info_Order_Data extends AbstractStructBase
{
    /**
     * The order
     * @var \SubregSDK\Test\StructType\Info_Order_Order
     */
    public $order;
    /**
     * Constructor method for Info_Order_Data
     * @uses Info_Order_Data::setOrder()
     * @param \SubregSDK\Test\StructType\Info_Order_Order $order
     */
    public function __construct(\SubregSDK\Test\StructType\Info_Order_Order $order = null)
    {
        $this
            ->setOrder($order);
    }
    /**
     * Get order value
     * @return \SubregSDK\Test\StructType\Info_Order_Order|null
     */
    public function getOrder()
    {
        return $this->order;
    }
    /**
     * Set order value
     * @param \SubregSDK\Test\StructType\Info_Order_Order $order
     * @return \SubregSDK\Test\StructType\Info_Order_Data
     */
    public function setOrder(\SubregSDK\Test\StructType\Info_Order_Order $order = null)
    {
        $this->order = $order;
        return $this;
    }
}
