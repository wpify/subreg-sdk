<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Modify_DNS_Record_Data StructType
 * @subpackage Structs
 */
class Modify_DNS_Record_Data extends AbstractStructBase
{
}
