<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Domains_List_Data StructType
 * @subpackage Structs
 */
class Domains_List_Data extends AbstractStructBase
{
    /**
     * The count
     * @var int
     */
    public $count;
    /**
     * The domains
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Domains_List_Domain[]
     */
    public $domains;
    /**
     * Constructor method for Domains_List_Data
     * @uses Domains_List_Data::setCount()
     * @uses Domains_List_Data::setDomains()
     * @param int $count
     * @param \SubregSDK\Test\StructType\Domains_List_Domain[] $domains
     */
    public function __construct($count = null, array $domains = array())
    {
        $this
            ->setCount($count)
            ->setDomains($domains);
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \SubregSDK\Test\StructType\Domains_List_Data
     */
    public function setCount($count = null)
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        return $this;
    }
    /**
     * Get domains value
     * @return \SubregSDK\Test\StructType\Domains_List_Domain[]|null
     */
    public function getDomains()
    {
        return $this->domains;
    }
    /**
     * This method is responsible for validating the values passed to the setDomains method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDomains method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDomainsForArrayConstraintsFromSetDomains(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $domains_List_DataDomainsItem) {
            // validation for constraint: itemType
            if (!$domains_List_DataDomainsItem instanceof \SubregSDK\Test\StructType\Domains_List_Domain) {
                $invalidValues[] = is_object($domains_List_DataDomainsItem) ? get_class($domains_List_DataDomainsItem) : sprintf('%s(%s)', gettype($domains_List_DataDomainsItem), var_export($domains_List_DataDomainsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\Domains_List_Domain, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Domains_List_Domain[] $domains
     * @return \SubregSDK\Test\StructType\Domains_List_Data
     */
    public function setDomains(array $domains = array())
    {
        // validation for constraint: array
        if ('' !== ($domainsArrayErrorMessage = self::validateDomainsForArrayConstraintsFromSetDomains($domains))) {
            throw new \InvalidArgumentException($domainsArrayErrorMessage, __LINE__);
        }
        $this->domains = $domains;
        return $this;
    }
    /**
     * Add item to domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Domains_List_Domain $item
     * @return \SubregSDK\Test\StructType\Domains_List_Data
     */
    public function addToDomains(\SubregSDK\Test\StructType\Domains_List_Domain $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Domains_List_Domain) {
            throw new \InvalidArgumentException(sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\Domains_List_Domain, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->domains[] = $item;
        return $this;
    }
}
