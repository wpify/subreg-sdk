<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Order_Payment StructType
 * @subpackage Structs
 */
class Order_Payment extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The payment
     * @var int
     */
    public $payment;
    /**
     * The amount
     * @var float
     */
    public $amount;
    /**
     * The currency
     * @var string
     */
    public $currency;
    /**
     * Constructor method for Order_Payment
     * @uses Order_Payment::setSsid()
     * @uses Order_Payment::setPayment()
     * @uses Order_Payment::setAmount()
     * @uses Order_Payment::setCurrency()
     * @param string $ssid
     * @param int $payment
     * @param float $amount
     * @param string $currency
     */
    public function __construct($ssid = null, $payment = null, $amount = null, $currency = null)
    {
        $this
            ->setSsid($ssid)
            ->setPayment($payment)
            ->setAmount($amount)
            ->setCurrency($currency);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\Order_Payment
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get payment value
     * @return int|null
     */
    public function getPayment()
    {
        return $this->payment;
    }
    /**
     * Set payment value
     * @param int $payment
     * @return \SubregSDK\Test\StructType\Order_Payment
     */
    public function setPayment($payment = null)
    {
        // validation for constraint: int
        if (!is_null($payment) && !(is_int($payment) || ctype_digit($payment))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($payment, true), gettype($payment)), __LINE__);
        }
        $this->payment = $payment;
        return $this;
    }
    /**
     * Get amount value
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * Set amount value
     * @param float $amount
     * @return \SubregSDK\Test\StructType\Order_Payment
     */
    public function setAmount($amount = null)
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a float value, %s given', var_export($amount, true), gettype($amount)), __LINE__);
        }
        $this->amount = $amount;
        return $this;
    }
    /**
     * Get currency value
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }
    /**
     * Set currency value
     * @param string $currency
     * @return \SubregSDK\Test\StructType\Order_Payment
     */
    public function setCurrency($currency = null)
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($currency, true), gettype($currency)), __LINE__);
        }
        $this->currency = $currency;
        return $this;
    }
}
