<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Contacts_List_Container StructType
 * @subpackage Structs
 */
class Contacts_List_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Contacts_List_Response
     */
    public $response;
    /**
     * Constructor method for Contacts_List_Container
     * @uses Contacts_List_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Contacts_List_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Contacts_List_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Contacts_List_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Contacts_List_Response $response
     * @return \SubregSDK\Test\StructType\Contacts_List_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Contacts_List_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
