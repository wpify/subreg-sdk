<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Check_Domain_Data StructType
 * @subpackage Structs
 */
class Check_Domain_Data extends AbstractStructBase
{
    /**
     * The name
     * @var string
     */
    public $name;
    /**
     * The avail
     * @var int
     */
    public $avail;
    /**
     * The existing_claim_id
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $existing_claim_id;
    /**
     * The price
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Check_Domain_Price
     */
    public $price;
    /**
     * The price_renew
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Check_Domain_Price
     */
    public $price_renew;
    /**
     * The price_transfer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Check_Domain_Price
     */
    public $price_transfer;
    /**
     * Constructor method for Check_Domain_Data
     * @uses Check_Domain_Data::setName()
     * @uses Check_Domain_Data::setAvail()
     * @uses Check_Domain_Data::setExisting_claim_id()
     * @uses Check_Domain_Data::setPrice()
     * @uses Check_Domain_Data::setPrice_renew()
     * @uses Check_Domain_Data::setPrice_transfer()
     * @param string $name
     * @param int $avail
     * @param string $existing_claim_id
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price_renew
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price_transfer
     */
    public function __construct($name = null, $avail = null, $existing_claim_id = null, \SubregSDK\Test\StructType\Check_Domain_Price $price = null, \SubregSDK\Test\StructType\Check_Domain_Price $price_renew = null, \SubregSDK\Test\StructType\Check_Domain_Price $price_transfer = null)
    {
        $this
            ->setName($name)
            ->setAvail($avail)
            ->setExisting_claim_id($existing_claim_id)
            ->setPrice($price)
            ->setPrice_renew($price_renew)
            ->setPrice_transfer($price_transfer);
    }
    /**
     * Get name value
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set name value
     * @param string $name
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setName($name = null)
    {
        // validation for constraint: string
        if (!is_null($name) && !is_string($name)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($name, true), gettype($name)), __LINE__);
        }
        $this->name = $name;
        return $this;
    }
    /**
     * Get avail value
     * @return int|null
     */
    public function getAvail()
    {
        return $this->avail;
    }
    /**
     * Set avail value
     * @param int $avail
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setAvail($avail = null)
    {
        // validation for constraint: int
        if (!is_null($avail) && !(is_int($avail) || ctype_digit($avail))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($avail, true), gettype($avail)), __LINE__);
        }
        $this->avail = $avail;
        return $this;
    }
    /**
     * Get existing_claim_id value
     * @return string|null
     */
    public function getExisting_claim_id()
    {
        return $this->existing_claim_id;
    }
    /**
     * Set existing_claim_id value
     * @param string $existing_claim_id
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setExisting_claim_id($existing_claim_id = null)
    {
        // validation for constraint: string
        if (!is_null($existing_claim_id) && !is_string($existing_claim_id)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($existing_claim_id, true), gettype($existing_claim_id)), __LINE__);
        }
        $this->existing_claim_id = $existing_claim_id;
        return $this;
    }
    /**
     * Get price value
     * @return \SubregSDK\Test\StructType\Check_Domain_Price|null
     */
    public function getPrice()
    {
        return $this->price;
    }
    /**
     * Set price value
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setPrice(\SubregSDK\Test\StructType\Check_Domain_Price $price = null)
    {
        $this->price = $price;
        return $this;
    }
    /**
     * Get price_renew value
     * @return \SubregSDK\Test\StructType\Check_Domain_Price|null
     */
    public function getPrice_renew()
    {
        return $this->price_renew;
    }
    /**
     * Set price_renew value
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price_renew
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setPrice_renew(\SubregSDK\Test\StructType\Check_Domain_Price $price_renew = null)
    {
        $this->price_renew = $price_renew;
        return $this;
    }
    /**
     * Get price_transfer value
     * @return \SubregSDK\Test\StructType\Check_Domain_Price|null
     */
    public function getPrice_transfer()
    {
        return $this->price_transfer;
    }
    /**
     * Set price_transfer value
     * @param \SubregSDK\Test\StructType\Check_Domain_Price $price_transfer
     * @return \SubregSDK\Test\StructType\Check_Domain_Data
     */
    public function setPrice_transfer(\SubregSDK\Test\StructType\Check_Domain_Price $price_transfer = null)
    {
        $this->price_transfer = $price_transfer;
        return $this;
    }
}
