<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_TLD_Info_Container StructType
 * @subpackage Structs
 */
class Get_TLD_Info_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Get_TLD_Info_Response
     */
    public $response;
    /**
     * Constructor method for Get_TLD_Info_Container
     * @uses Get_TLD_Info_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Get_TLD_Info_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Response $response
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Get_TLD_Info_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
