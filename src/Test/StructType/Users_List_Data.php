<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Users_List_Data StructType
 * @subpackage Structs
 */
class Users_List_Data extends AbstractStructBase
{
    /**
     * The count
     * @var int
     */
    public $count;
    /**
     * The users
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Users_List_User[]
     */
    public $users;
    /**
     * Constructor method for Users_List_Data
     * @uses Users_List_Data::setCount()
     * @uses Users_List_Data::setUsers()
     * @param int $count
     * @param \SubregSDK\Test\StructType\Users_List_User[] $users
     */
    public function __construct($count = null, array $users = array())
    {
        $this
            ->setCount($count)
            ->setUsers($users);
    }
    /**
     * Get count value
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }
    /**
     * Set count value
     * @param int $count
     * @return \SubregSDK\Test\StructType\Users_List_Data
     */
    public function setCount($count = null)
    {
        // validation for constraint: int
        if (!is_null($count) && !(is_int($count) || ctype_digit($count))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($count, true), gettype($count)), __LINE__);
        }
        $this->count = $count;
        return $this;
    }
    /**
     * Get users value
     * @return \SubregSDK\Test\StructType\Users_List_User[]|null
     */
    public function getUsers()
    {
        return $this->users;
    }
    /**
     * This method is responsible for validating the values passed to the setUsers method
     * This method is willingly generated in order to preserve the one-line inline validation within the setUsers method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateUsersForArrayConstraintsFromSetUsers(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $users_List_DataUsersItem) {
            // validation for constraint: itemType
            if (!$users_List_DataUsersItem instanceof \SubregSDK\Test\StructType\Users_List_User) {
                $invalidValues[] = is_object($users_List_DataUsersItem) ? get_class($users_List_DataUsersItem) : sprintf('%s(%s)', gettype($users_List_DataUsersItem), var_export($users_List_DataUsersItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The users property can only contain items of type \SubregSDK\Test\StructType\Users_List_User, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set users value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Users_List_User[] $users
     * @return \SubregSDK\Test\StructType\Users_List_Data
     */
    public function setUsers(array $users = array())
    {
        // validation for constraint: array
        if ('' !== ($usersArrayErrorMessage = self::validateUsersForArrayConstraintsFromSetUsers($users))) {
            throw new \InvalidArgumentException($usersArrayErrorMessage, __LINE__);
        }
        $this->users = $users;
        return $this;
    }
    /**
     * Add item to users value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Users_List_User $item
     * @return \SubregSDK\Test\StructType\Users_List_Data
     */
    public function addToUsers(\SubregSDK\Test\StructType\Users_List_User $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Users_List_User) {
            throw new \InvalidArgumentException(sprintf('The users property can only contain items of type \SubregSDK\Test\StructType\Users_List_User, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->users[] = $item;
        return $this;
    }
}
