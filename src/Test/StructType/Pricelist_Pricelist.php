<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Pricelist_Pricelist StructType
 * @subpackage Structs
 */
class Pricelist_Pricelist extends AbstractStructBase
{
    /**
     * The tld
     * @var string
     */
    public $tld;
    /**
     * The promo
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $promo;
    /**
     * The promoexp
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $promoexp;
    /**
     * The country
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $country;
    /**
     * The continent
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $continent;
    /**
     * The minyear
     * @var int
     */
    public $minyear;
    /**
     * The maxyear
     * @var int
     */
    public $maxyear;
    /**
     * The minyear_renew
     * @var int
     */
    public $minyear_renew;
    /**
     * The maxyear_renew
     * @var int
     */
    public $maxyear_renew;
    /**
     * The local_presence
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var int
     */
    public $local_presence;
    /**
     * The prices
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Pricelist_Price[]
     */
    public $prices;
    /**
     * The statuses
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $statuses;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Pricelist_Param[]
     */
    public $params;
    /**
     * Constructor method for Pricelist_Pricelist
     * @uses Pricelist_Pricelist::setTld()
     * @uses Pricelist_Pricelist::setPromo()
     * @uses Pricelist_Pricelist::setPromoexp()
     * @uses Pricelist_Pricelist::setCountry()
     * @uses Pricelist_Pricelist::setContinent()
     * @uses Pricelist_Pricelist::setMinyear()
     * @uses Pricelist_Pricelist::setMaxyear()
     * @uses Pricelist_Pricelist::setMinyear_renew()
     * @uses Pricelist_Pricelist::setMaxyear_renew()
     * @uses Pricelist_Pricelist::setLocal_presence()
     * @uses Pricelist_Pricelist::setPrices()
     * @uses Pricelist_Pricelist::setStatuses()
     * @uses Pricelist_Pricelist::setParams()
     * @param string $tld
     * @param int $promo
     * @param string $promoexp
     * @param string $country
     * @param string $continent
     * @param int $minyear
     * @param int $maxyear
     * @param int $minyear_renew
     * @param int $maxyear_renew
     * @param int $local_presence
     * @param \SubregSDK\Test\StructType\Pricelist_Price[] $prices
     * @param string[] $statuses
     * @param \SubregSDK\Test\StructType\Pricelist_Param[] $params
     */
    public function __construct($tld = null, $promo = null, $promoexp = null, $country = null, $continent = null, $minyear = null, $maxyear = null, $minyear_renew = null, $maxyear_renew = null, $local_presence = null, array $prices = array(), array $statuses = array(), array $params = array())
    {
        $this
            ->setTld($tld)
            ->setPromo($promo)
            ->setPromoexp($promoexp)
            ->setCountry($country)
            ->setContinent($continent)
            ->setMinyear($minyear)
            ->setMaxyear($maxyear)
            ->setMinyear_renew($minyear_renew)
            ->setMaxyear_renew($maxyear_renew)
            ->setLocal_presence($local_presence)
            ->setPrices($prices)
            ->setStatuses($statuses)
            ->setParams($params);
    }
    /**
     * Get tld value
     * @return string|null
     */
    public function getTld()
    {
        return $this->tld;
    }
    /**
     * Set tld value
     * @param string $tld
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setTld($tld = null)
    {
        // validation for constraint: string
        if (!is_null($tld) && !is_string($tld)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($tld, true), gettype($tld)), __LINE__);
        }
        $this->tld = $tld;
        return $this;
    }
    /**
     * Get promo value
     * @return int|null
     */
    public function getPromo()
    {
        return $this->promo;
    }
    /**
     * Set promo value
     * @param int $promo
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setPromo($promo = null)
    {
        // validation for constraint: int
        if (!is_null($promo) && !(is_int($promo) || ctype_digit($promo))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($promo, true), gettype($promo)), __LINE__);
        }
        $this->promo = $promo;
        return $this;
    }
    /**
     * Get promoexp value
     * @return string|null
     */
    public function getPromoexp()
    {
        return $this->promoexp;
    }
    /**
     * Set promoexp value
     * @param string $promoexp
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setPromoexp($promoexp = null)
    {
        // validation for constraint: string
        if (!is_null($promoexp) && !is_string($promoexp)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($promoexp, true), gettype($promoexp)), __LINE__);
        }
        $this->promoexp = $promoexp;
        return $this;
    }
    /**
     * Get country value
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }
    /**
     * Set country value
     * @param string $country
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setCountry($country = null)
    {
        // validation for constraint: string
        if (!is_null($country) && !is_string($country)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($country, true), gettype($country)), __LINE__);
        }
        $this->country = $country;
        return $this;
    }
    /**
     * Get continent value
     * @return string|null
     */
    public function getContinent()
    {
        return $this->continent;
    }
    /**
     * Set continent value
     * @param string $continent
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setContinent($continent = null)
    {
        // validation for constraint: string
        if (!is_null($continent) && !is_string($continent)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($continent, true), gettype($continent)), __LINE__);
        }
        $this->continent = $continent;
        return $this;
    }
    /**
     * Get minyear value
     * @return int|null
     */
    public function getMinyear()
    {
        return $this->minyear;
    }
    /**
     * Set minyear value
     * @param int $minyear
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setMinyear($minyear = null)
    {
        // validation for constraint: int
        if (!is_null($minyear) && !(is_int($minyear) || ctype_digit($minyear))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minyear, true), gettype($minyear)), __LINE__);
        }
        $this->minyear = $minyear;
        return $this;
    }
    /**
     * Get maxyear value
     * @return int|null
     */
    public function getMaxyear()
    {
        return $this->maxyear;
    }
    /**
     * Set maxyear value
     * @param int $maxyear
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setMaxyear($maxyear = null)
    {
        // validation for constraint: int
        if (!is_null($maxyear) && !(is_int($maxyear) || ctype_digit($maxyear))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxyear, true), gettype($maxyear)), __LINE__);
        }
        $this->maxyear = $maxyear;
        return $this;
    }
    /**
     * Get minyear_renew value
     * @return int|null
     */
    public function getMinyear_renew()
    {
        return $this->minyear_renew;
    }
    /**
     * Set minyear_renew value
     * @param int $minyear_renew
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setMinyear_renew($minyear_renew = null)
    {
        // validation for constraint: int
        if (!is_null($minyear_renew) && !(is_int($minyear_renew) || ctype_digit($minyear_renew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($minyear_renew, true), gettype($minyear_renew)), __LINE__);
        }
        $this->minyear_renew = $minyear_renew;
        return $this;
    }
    /**
     * Get maxyear_renew value
     * @return int|null
     */
    public function getMaxyear_renew()
    {
        return $this->maxyear_renew;
    }
    /**
     * Set maxyear_renew value
     * @param int $maxyear_renew
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setMaxyear_renew($maxyear_renew = null)
    {
        // validation for constraint: int
        if (!is_null($maxyear_renew) && !(is_int($maxyear_renew) || ctype_digit($maxyear_renew))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($maxyear_renew, true), gettype($maxyear_renew)), __LINE__);
        }
        $this->maxyear_renew = $maxyear_renew;
        return $this;
    }
    /**
     * Get local_presence value
     * @return int|null
     */
    public function getLocal_presence()
    {
        return $this->local_presence;
    }
    /**
     * Set local_presence value
     * @param int $local_presence
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setLocal_presence($local_presence = null)
    {
        // validation for constraint: int
        if (!is_null($local_presence) && !(is_int($local_presence) || ctype_digit($local_presence))) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide an integer value, %s given', var_export($local_presence, true), gettype($local_presence)), __LINE__);
        }
        $this->local_presence = $local_presence;
        return $this;
    }
    /**
     * Get prices value
     * @return \SubregSDK\Test\StructType\Pricelist_Price[]|null
     */
    public function getPrices()
    {
        return $this->prices;
    }
    /**
     * This method is responsible for validating the values passed to the setPrices method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPrices method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePricesForArrayConstraintsFromSetPrices(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $pricelist_PricelistPricesItem) {
            // validation for constraint: itemType
            if (!$pricelist_PricelistPricesItem instanceof \SubregSDK\Test\StructType\Pricelist_Price) {
                $invalidValues[] = is_object($pricelist_PricelistPricesItem) ? get_class($pricelist_PricelistPricesItem) : sprintf('%s(%s)', gettype($pricelist_PricelistPricesItem), var_export($pricelist_PricelistPricesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The prices property can only contain items of type \SubregSDK\Test\StructType\Pricelist_Price, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Pricelist_Price[] $prices
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setPrices(array $prices = array())
    {
        // validation for constraint: array
        if ('' !== ($pricesArrayErrorMessage = self::validatePricesForArrayConstraintsFromSetPrices($prices))) {
            throw new \InvalidArgumentException($pricesArrayErrorMessage, __LINE__);
        }
        $this->prices = $prices;
        return $this;
    }
    /**
     * Add item to prices value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Pricelist_Price $item
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function addToPrices(\SubregSDK\Test\StructType\Pricelist_Price $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Pricelist_Price) {
            throw new \InvalidArgumentException(sprintf('The prices property can only contain items of type \SubregSDK\Test\StructType\Pricelist_Price, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->prices[] = $item;
        return $this;
    }
    /**
     * Get statuses value
     * @return string[]|null
     */
    public function getStatuses()
    {
        return $this->statuses;
    }
    /**
     * This method is responsible for validating the values passed to the setStatuses method
     * This method is willingly generated in order to preserve the one-line inline validation within the setStatuses method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateStatusesForArrayConstraintsFromSetStatuses(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $pricelist_PricelistStatusesItem) {
            // validation for constraint: itemType
            if (!is_string($pricelist_PricelistStatusesItem)) {
                $invalidValues[] = is_object($pricelist_PricelistStatusesItem) ? get_class($pricelist_PricelistStatusesItem) : sprintf('%s(%s)', gettype($pricelist_PricelistStatusesItem), var_export($pricelist_PricelistStatusesItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The statuses property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set statuses value
     * @throws \InvalidArgumentException
     * @param string[] $statuses
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setStatuses(array $statuses = array())
    {
        // validation for constraint: array
        if ('' !== ($statusesArrayErrorMessage = self::validateStatusesForArrayConstraintsFromSetStatuses($statuses))) {
            throw new \InvalidArgumentException($statusesArrayErrorMessage, __LINE__);
        }
        $this->statuses = $statuses;
        return $this;
    }
    /**
     * Add item to statuses value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function addToStatuses($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The statuses property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->statuses[] = $item;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Test\StructType\Pricelist_Param[]|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * This method is responsible for validating the values passed to the setParams method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParams method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParamsForArrayConstraintsFromSetParams(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $pricelist_PricelistParamsItem) {
            // validation for constraint: itemType
            if (!$pricelist_PricelistParamsItem instanceof \SubregSDK\Test\StructType\Pricelist_Param) {
                $invalidValues[] = is_object($pricelist_PricelistParamsItem) ? get_class($pricelist_PricelistParamsItem) : sprintf('%s(%s)', gettype($pricelist_PricelistParamsItem), var_export($pricelist_PricelistParamsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Pricelist_Param, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Pricelist_Param[] $params
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function setParams(array $params = array())
    {
        // validation for constraint: array
        if ('' !== ($paramsArrayErrorMessage = self::validateParamsForArrayConstraintsFromSetParams($params))) {
            throw new \InvalidArgumentException($paramsArrayErrorMessage, __LINE__);
        }
        $this->params = $params;
        return $this;
    }
    /**
     * Add item to params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Pricelist_Param $item
     * @return \SubregSDK\Test\StructType\Pricelist_Pricelist
     */
    public function addToParams(\SubregSDK\Test\StructType\Pricelist_Param $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Pricelist_Param) {
            throw new \InvalidArgumentException(sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Pricelist_Param, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->params[] = $item;
        return $this;
    }
}
