<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Pricelist_Container StructType
 * @subpackage Structs
 */
class Pricelist_Container extends AbstractStructBase
{
    /**
     * The response
     * @var \SubregSDK\Test\StructType\Pricelist_Response
     */
    public $response;
    /**
     * Constructor method for Pricelist_Container
     * @uses Pricelist_Container::setResponse()
     * @param \SubregSDK\Test\StructType\Pricelist_Response $response
     */
    public function __construct(\SubregSDK\Test\StructType\Pricelist_Response $response = null)
    {
        $this
            ->setResponse($response);
    }
    /**
     * Get response value
     * @return \SubregSDK\Test\StructType\Pricelist_Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }
    /**
     * Set response value
     * @param \SubregSDK\Test\StructType\Pricelist_Response $response
     * @return \SubregSDK\Test\StructType\Pricelist_Container
     */
    public function setResponse(\SubregSDK\Test\StructType\Pricelist_Response $response = null)
    {
        $this->response = $response;
        return $this;
    }
}
