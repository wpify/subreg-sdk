<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_List_Domains_Data StructType
 * @subpackage Structs
 */
class Anycast_List_Domains_Data extends AbstractStructBase
{
    /**
     * The domains
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Anycast_List_Domains_Domain[]
     */
    public $domains;
    /**
     * Constructor method for Anycast_List_Domains_Data
     * @uses Anycast_List_Domains_Data::setDomains()
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains_Domain[] $domains
     */
    public function __construct(array $domains = array())
    {
        $this
            ->setDomains($domains);
    }
    /**
     * Get domains value
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Domain[]|null
     */
    public function getDomains()
    {
        return $this->domains;
    }
    /**
     * This method is responsible for validating the values passed to the setDomains method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDomains method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDomainsForArrayConstraintsFromSetDomains(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $anycast_List_Domains_DataDomainsItem) {
            // validation for constraint: itemType
            if (!$anycast_List_Domains_DataDomainsItem instanceof \SubregSDK\Test\StructType\Anycast_List_Domains_Domain) {
                $invalidValues[] = is_object($anycast_List_Domains_DataDomainsItem) ? get_class($anycast_List_Domains_DataDomainsItem) : sprintf('%s(%s)', gettype($anycast_List_Domains_DataDomainsItem), var_export($anycast_List_Domains_DataDomainsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\Anycast_List_Domains_Domain, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains_Domain[] $domains
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Data
     */
    public function setDomains(array $domains = array())
    {
        // validation for constraint: array
        if ('' !== ($domainsArrayErrorMessage = self::validateDomainsForArrayConstraintsFromSetDomains($domains))) {
            throw new \InvalidArgumentException($domainsArrayErrorMessage, __LINE__);
        }
        $this->domains = $domains;
        return $this;
    }
    /**
     * Add item to domains value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains_Domain $item
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Data
     */
    public function addToDomains(\SubregSDK\Test\StructType\Anycast_List_Domains_Domain $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Anycast_List_Domains_Domain) {
            throw new \InvalidArgumentException(sprintf('The domains property can only contain items of type \SubregSDK\Test\StructType\Anycast_List_Domains_Domain, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->domains[] = $item;
        return $this;
    }
}
