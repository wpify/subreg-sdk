<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Get_TLD_Info_Data StructType
 * @subpackage Structs
 */
class Get_TLD_Info_Data extends AbstractStructBase
{
    /**
     * The periodsCreate
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $periodsCreate;
    /**
     * The periodsRenew
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var string[]
     */
    public $periodsRenew;
    /**
     * The transfer
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $transfer;
    /**
     * The trade
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $trade;
    /**
     * The idn
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $idn;
    /**
     * The trustee
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $trustee;
    /**
     * The ns
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $ns;
    /**
     * The contacts
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Get_TLD_Info_Contact[]
     */
    public $contacts;
    /**
     * The params
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Get_TLD_Info_Param[]
     */
    public $params;
    /**
     * Constructor method for Get_TLD_Info_Data
     * @uses Get_TLD_Info_Data::setPeriodsCreate()
     * @uses Get_TLD_Info_Data::setPeriodsRenew()
     * @uses Get_TLD_Info_Data::setTransfer()
     * @uses Get_TLD_Info_Data::setTrade()
     * @uses Get_TLD_Info_Data::setIdn()
     * @uses Get_TLD_Info_Data::setTrustee()
     * @uses Get_TLD_Info_Data::setNs()
     * @uses Get_TLD_Info_Data::setContacts()
     * @uses Get_TLD_Info_Data::setParams()
     * @param string[] $periodsCreate
     * @param string[] $periodsRenew
     * @param string $transfer
     * @param string $trade
     * @param string $idn
     * @param string $trustee
     * @param string $ns
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Contact[] $contacts
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Param[] $params
     */
    public function __construct(array $periodsCreate = array(), array $periodsRenew = array(), $transfer = null, $trade = null, $idn = null, $trustee = null, $ns = null, array $contacts = array(), array $params = array())
    {
        $this
            ->setPeriodsCreate($periodsCreate)
            ->setPeriodsRenew($periodsRenew)
            ->setTransfer($transfer)
            ->setTrade($trade)
            ->setIdn($idn)
            ->setTrustee($trustee)
            ->setNs($ns)
            ->setContacts($contacts)
            ->setParams($params);
    }
    /**
     * Get periodsCreate value
     * @return string[]|null
     */
    public function getPeriodsCreate()
    {
        return $this->periodsCreate;
    }
    /**
     * This method is responsible for validating the values passed to the setPeriodsCreate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPeriodsCreate method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePeriodsCreateForArrayConstraintsFromSetPeriodsCreate(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_TLD_Info_DataPeriodsCreateItem) {
            // validation for constraint: itemType
            if (!is_string($get_TLD_Info_DataPeriodsCreateItem)) {
                $invalidValues[] = is_object($get_TLD_Info_DataPeriodsCreateItem) ? get_class($get_TLD_Info_DataPeriodsCreateItem) : sprintf('%s(%s)', gettype($get_TLD_Info_DataPeriodsCreateItem), var_export($get_TLD_Info_DataPeriodsCreateItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The periodsCreate property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set periodsCreate value
     * @throws \InvalidArgumentException
     * @param string[] $periodsCreate
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setPeriodsCreate(array $periodsCreate = array())
    {
        // validation for constraint: array
        if ('' !== ($periodsCreateArrayErrorMessage = self::validatePeriodsCreateForArrayConstraintsFromSetPeriodsCreate($periodsCreate))) {
            throw new \InvalidArgumentException($periodsCreateArrayErrorMessage, __LINE__);
        }
        $this->periodsCreate = $periodsCreate;
        return $this;
    }
    /**
     * Add item to periodsCreate value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function addToPeriodsCreate($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The periodsCreate property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->periodsCreate[] = $item;
        return $this;
    }
    /**
     * Get periodsRenew value
     * @return string[]|null
     */
    public function getPeriodsRenew()
    {
        return $this->periodsRenew;
    }
    /**
     * This method is responsible for validating the values passed to the setPeriodsRenew method
     * This method is willingly generated in order to preserve the one-line inline validation within the setPeriodsRenew method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validatePeriodsRenewForArrayConstraintsFromSetPeriodsRenew(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_TLD_Info_DataPeriodsRenewItem) {
            // validation for constraint: itemType
            if (!is_string($get_TLD_Info_DataPeriodsRenewItem)) {
                $invalidValues[] = is_object($get_TLD_Info_DataPeriodsRenewItem) ? get_class($get_TLD_Info_DataPeriodsRenewItem) : sprintf('%s(%s)', gettype($get_TLD_Info_DataPeriodsRenewItem), var_export($get_TLD_Info_DataPeriodsRenewItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The periodsRenew property can only contain items of type string, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set periodsRenew value
     * @throws \InvalidArgumentException
     * @param string[] $periodsRenew
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setPeriodsRenew(array $periodsRenew = array())
    {
        // validation for constraint: array
        if ('' !== ($periodsRenewArrayErrorMessage = self::validatePeriodsRenewForArrayConstraintsFromSetPeriodsRenew($periodsRenew))) {
            throw new \InvalidArgumentException($periodsRenewArrayErrorMessage, __LINE__);
        }
        $this->periodsRenew = $periodsRenew;
        return $this;
    }
    /**
     * Add item to periodsRenew value
     * @throws \InvalidArgumentException
     * @param string $item
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function addToPeriodsRenew($item)
    {
        // validation for constraint: itemType
        if (!is_string($item)) {
            throw new \InvalidArgumentException(sprintf('The periodsRenew property can only contain items of type string, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->periodsRenew[] = $item;
        return $this;
    }
    /**
     * Get transfer value
     * @return string|null
     */
    public function getTransfer()
    {
        return $this->transfer;
    }
    /**
     * Set transfer value
     * @param string $transfer
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setTransfer($transfer = null)
    {
        // validation for constraint: string
        if (!is_null($transfer) && !is_string($transfer)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($transfer, true), gettype($transfer)), __LINE__);
        }
        $this->transfer = $transfer;
        return $this;
    }
    /**
     * Get trade value
     * @return string|null
     */
    public function getTrade()
    {
        return $this->trade;
    }
    /**
     * Set trade value
     * @param string $trade
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setTrade($trade = null)
    {
        // validation for constraint: string
        if (!is_null($trade) && !is_string($trade)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trade, true), gettype($trade)), __LINE__);
        }
        $this->trade = $trade;
        return $this;
    }
    /**
     * Get idn value
     * @return string|null
     */
    public function getIdn()
    {
        return $this->idn;
    }
    /**
     * Set idn value
     * @param string $idn
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setIdn($idn = null)
    {
        // validation for constraint: string
        if (!is_null($idn) && !is_string($idn)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($idn, true), gettype($idn)), __LINE__);
        }
        $this->idn = $idn;
        return $this;
    }
    /**
     * Get trustee value
     * @return string|null
     */
    public function getTrustee()
    {
        return $this->trustee;
    }
    /**
     * Set trustee value
     * @param string $trustee
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setTrustee($trustee = null)
    {
        // validation for constraint: string
        if (!is_null($trustee) && !is_string($trustee)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($trustee, true), gettype($trustee)), __LINE__);
        }
        $this->trustee = $trustee;
        return $this;
    }
    /**
     * Get ns value
     * @return string|null
     */
    public function getNs()
    {
        return $this->ns;
    }
    /**
     * Set ns value
     * @param string $ns
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setNs($ns = null)
    {
        // validation for constraint: string
        if (!is_null($ns) && !is_string($ns)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ns, true), gettype($ns)), __LINE__);
        }
        $this->ns = $ns;
        return $this;
    }
    /**
     * Get contacts value
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Contact[]|null
     */
    public function getContacts()
    {
        return $this->contacts;
    }
    /**
     * This method is responsible for validating the values passed to the setContacts method
     * This method is willingly generated in order to preserve the one-line inline validation within the setContacts method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateContactsForArrayConstraintsFromSetContacts(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_TLD_Info_DataContactsItem) {
            // validation for constraint: itemType
            if (!$get_TLD_Info_DataContactsItem instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Contact) {
                $invalidValues[] = is_object($get_TLD_Info_DataContactsItem) ? get_class($get_TLD_Info_DataContactsItem) : sprintf('%s(%s)', gettype($get_TLD_Info_DataContactsItem), var_export($get_TLD_Info_DataContactsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The contacts property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Contact, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set contacts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Contact[] $contacts
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setContacts(array $contacts = array())
    {
        // validation for constraint: array
        if ('' !== ($contactsArrayErrorMessage = self::validateContactsForArrayConstraintsFromSetContacts($contacts))) {
            throw new \InvalidArgumentException($contactsArrayErrorMessage, __LINE__);
        }
        $this->contacts = $contacts;
        return $this;
    }
    /**
     * Add item to contacts value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Contact $item
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function addToContacts(\SubregSDK\Test\StructType\Get_TLD_Info_Contact $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Contact) {
            throw new \InvalidArgumentException(sprintf('The contacts property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Contact, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->contacts[] = $item;
        return $this;
    }
    /**
     * Get params value
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Param[]|null
     */
    public function getParams()
    {
        return $this->params;
    }
    /**
     * This method is responsible for validating the values passed to the setParams method
     * This method is willingly generated in order to preserve the one-line inline validation within the setParams method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateParamsForArrayConstraintsFromSetParams(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $get_TLD_Info_DataParamsItem) {
            // validation for constraint: itemType
            if (!$get_TLD_Info_DataParamsItem instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Param) {
                $invalidValues[] = is_object($get_TLD_Info_DataParamsItem) ? get_class($get_TLD_Info_DataParamsItem) : sprintf('%s(%s)', gettype($get_TLD_Info_DataParamsItem), var_export($get_TLD_Info_DataParamsItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Param, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Param[] $params
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function setParams(array $params = array())
    {
        // validation for constraint: array
        if ('' !== ($paramsArrayErrorMessage = self::validateParamsForArrayConstraintsFromSetParams($params))) {
            throw new \InvalidArgumentException($paramsArrayErrorMessage, __LINE__);
        }
        $this->params = $params;
        return $this;
    }
    /**
     * Add item to params value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Get_TLD_Info_Param $item
     * @return \SubregSDK\Test\StructType\Get_TLD_Info_Data
     */
    public function addToParams(\SubregSDK\Test\StructType\Get_TLD_Info_Param $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Get_TLD_Info_Param) {
            throw new \InvalidArgumentException(sprintf('The params property can only contain items of type \SubregSDK\Test\StructType\Get_TLD_Info_Param, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->params[] = $item;
        return $this;
    }
}
