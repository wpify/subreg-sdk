<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for In_Subreg_Data StructType
 * @subpackage Structs
 */
class In_Subreg_Data extends AbstractStructBase
{
    /**
     * The myaccount
     * @var string
     */
    public $myaccount;
    /**
     * Constructor method for In_Subreg_Data
     * @uses In_Subreg_Data::setMyaccount()
     * @param string $myaccount
     */
    public function __construct($myaccount = null)
    {
        $this
            ->setMyaccount($myaccount);
    }
    /**
     * Get myaccount value
     * @return string|null
     */
    public function getMyaccount()
    {
        return $this->myaccount;
    }
    /**
     * Set myaccount value
     * @param string $myaccount
     * @return \SubregSDK\Test\StructType\In_Subreg_Data
     */
    public function setMyaccount($myaccount = null)
    {
        // validation for constraint: string
        if (!is_null($myaccount) && !is_string($myaccount)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($myaccount, true), gettype($myaccount)), __LINE__);
        }
        $this->myaccount = $myaccount;
        return $this;
    }
}
