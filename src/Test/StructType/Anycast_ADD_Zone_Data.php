<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Anycast_ADD_Zone_Data StructType
 * @subpackage Structs
 */
class Anycast_ADD_Zone_Data extends AbstractStructBase
{
}
