<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Info_Domain_Options StructType
 * @subpackage Structs
 */
class Info_Domain_Options extends AbstractStructBase
{
    /**
     * The nsset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $nsset;
    /**
     * The keyset
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $keyset;
    /**
     * The dsdata
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     * - minOccurs: 0
     * @var \SubregSDK\Test\StructType\Info_Domain_Dsdata[]
     */
    public $dsdata;
    /**
     * The keygroup
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $keygroup;
    /**
     * The quarantined
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     * @var string
     */
    public $quarantined;
    /**
     * Constructor method for Info_Domain_Options
     * @uses Info_Domain_Options::setNsset()
     * @uses Info_Domain_Options::setKeyset()
     * @uses Info_Domain_Options::setDsdata()
     * @uses Info_Domain_Options::setKeygroup()
     * @uses Info_Domain_Options::setQuarantined()
     * @param string $nsset
     * @param string $keyset
     * @param \SubregSDK\Test\StructType\Info_Domain_Dsdata[] $dsdata
     * @param string $keygroup
     * @param string $quarantined
     */
    public function __construct($nsset = null, $keyset = null, array $dsdata = array(), $keygroup = null, $quarantined = null)
    {
        $this
            ->setNsset($nsset)
            ->setKeyset($keyset)
            ->setDsdata($dsdata)
            ->setKeygroup($keygroup)
            ->setQuarantined($quarantined);
    }
    /**
     * Get nsset value
     * @return string|null
     */
    public function getNsset()
    {
        return $this->nsset;
    }
    /**
     * Set nsset value
     * @param string $nsset
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function setNsset($nsset = null)
    {
        // validation for constraint: string
        if (!is_null($nsset) && !is_string($nsset)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($nsset, true), gettype($nsset)), __LINE__);
        }
        $this->nsset = $nsset;
        return $this;
    }
    /**
     * Get keyset value
     * @return string|null
     */
    public function getKeyset()
    {
        return $this->keyset;
    }
    /**
     * Set keyset value
     * @param string $keyset
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function setKeyset($keyset = null)
    {
        // validation for constraint: string
        if (!is_null($keyset) && !is_string($keyset)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($keyset, true), gettype($keyset)), __LINE__);
        }
        $this->keyset = $keyset;
        return $this;
    }
    /**
     * Get dsdata value
     * @return \SubregSDK\Test\StructType\Info_Domain_Dsdata[]|null
     */
    public function getDsdata()
    {
        return $this->dsdata;
    }
    /**
     * This method is responsible for validating the values passed to the setDsdata method
     * This method is willingly generated in order to preserve the one-line inline validation within the setDsdata method
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateDsdataForArrayConstraintsFromSetDsdata(array $values = array())
    {
        $message = '';
        $invalidValues = [];
        foreach ($values as $info_Domain_OptionsDsdataItem) {
            // validation for constraint: itemType
            if (!$info_Domain_OptionsDsdataItem instanceof \SubregSDK\Test\StructType\Info_Domain_Dsdata) {
                $invalidValues[] = is_object($info_Domain_OptionsDsdataItem) ? get_class($info_Domain_OptionsDsdataItem) : sprintf('%s(%s)', gettype($info_Domain_OptionsDsdataItem), var_export($info_Domain_OptionsDsdataItem, true));
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf('The dsdata property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_Dsdata, %s given', is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(', ', $invalidValues) : gettype($invalidValues)));
        }
        unset($invalidValues);
        return $message;
    }
    /**
     * Set dsdata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_Dsdata[] $dsdata
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function setDsdata(array $dsdata = array())
    {
        // validation for constraint: array
        if ('' !== ($dsdataArrayErrorMessage = self::validateDsdataForArrayConstraintsFromSetDsdata($dsdata))) {
            throw new \InvalidArgumentException($dsdataArrayErrorMessage, __LINE__);
        }
        $this->dsdata = $dsdata;
        return $this;
    }
    /**
     * Add item to dsdata value
     * @throws \InvalidArgumentException
     * @param \SubregSDK\Test\StructType\Info_Domain_Dsdata $item
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function addToDsdata(\SubregSDK\Test\StructType\Info_Domain_Dsdata $item)
    {
        // validation for constraint: itemType
        if (!$item instanceof \SubregSDK\Test\StructType\Info_Domain_Dsdata) {
            throw new \InvalidArgumentException(sprintf('The dsdata property can only contain items of type \SubregSDK\Test\StructType\Info_Domain_Dsdata, %s given', is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))), __LINE__);
        }
        $this->dsdata[] = $item;
        return $this;
    }
    /**
     * Get keygroup value
     * @return string|null
     */
    public function getKeygroup()
    {
        return $this->keygroup;
    }
    /**
     * Set keygroup value
     * @param string $keygroup
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function setKeygroup($keygroup = null)
    {
        // validation for constraint: string
        if (!is_null($keygroup) && !is_string($keygroup)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($keygroup, true), gettype($keygroup)), __LINE__);
        }
        $this->keygroup = $keygroup;
        return $this;
    }
    /**
     * Get quarantined value
     * @return string|null
     */
    public function getQuarantined()
    {
        return $this->quarantined;
    }
    /**
     * Set quarantined value
     * @param string $quarantined
     * @return \SubregSDK\Test\StructType\Info_Domain_Options
     */
    public function setQuarantined($quarantined = null)
    {
        // validation for constraint: string
        if (!is_null($quarantined) && !is_string($quarantined)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($quarantined, true), gettype($quarantined)), __LINE__);
        }
        $this->quarantined = $quarantined;
        return $this;
    }
}
