<?php

namespace SubregSDK\Test\StructType;

use \WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for OIB_Search StructType
 * @subpackage Structs
 */
class OIB_Search extends AbstractStructBase
{
    /**
     * The ssid
     * @var string
     */
    public $ssid;
    /**
     * The oib
     * @var string
     */
    public $oib;
    /**
     * Constructor method for OIB_Search
     * @uses OIB_Search::setSsid()
     * @uses OIB_Search::setOib()
     * @param string $ssid
     * @param string $oib
     */
    public function __construct($ssid = null, $oib = null)
    {
        $this
            ->setSsid($ssid)
            ->setOib($oib);
    }
    /**
     * Get ssid value
     * @return string|null
     */
    public function getSsid()
    {
        return $this->ssid;
    }
    /**
     * Set ssid value
     * @param string $ssid
     * @return \SubregSDK\Test\StructType\OIB_Search
     */
    public function setSsid($ssid = null)
    {
        // validation for constraint: string
        if (!is_null($ssid) && !is_string($ssid)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($ssid, true), gettype($ssid)), __LINE__);
        }
        $this->ssid = $ssid;
        return $this;
    }
    /**
     * Get oib value
     * @return string|null
     */
    public function getOib()
    {
        return $this->oib;
    }
    /**
     * Set oib value
     * @param string $oib
     * @return \SubregSDK\Test\StructType\OIB_Search
     */
    public function setOib($oib = null)
    {
        // validation for constraint: string
        if (!is_null($oib) && !is_string($oib)) {
            throw new \InvalidArgumentException(sprintf('Invalid value %s, please provide a string, %s given', var_export($oib, true), gettype($oib)), __LINE__);
        }
        $this->oib = $oib;
        return $this;
    }
}
