<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for List ServiceType
 * @subpackage Services
 */
class _List extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named List_Documents
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\List_Documents $parameters
     * @return \SubregSDK\Test\StructType\List_Documents_Container|bool
     */
    public function List_Documents(\SubregSDK\Test\StructType\List_Documents $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('List_Documents', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\List_Documents_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
