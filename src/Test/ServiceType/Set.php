<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Set ServiceType
 * @subpackage Services
 */
class Set extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Set_Autorenew
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Set_Autorenew $parameters
     * @return \SubregSDK\Test\StructType\Set_Autorenew_Container|bool
     */
    public function Set_Autorenew(\SubregSDK\Test\StructType\Set_Autorenew $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Set_Autorenew', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Set_Prices
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Set_Prices $parameters
     * @return \SubregSDK\Test\StructType\Set_Prices_Container|bool
     */
    public function Set_Prices(\SubregSDK\Test\StructType\Set_Prices $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Set_Prices', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Set_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Set_DNS_Zone $parameters
     * @return \SubregSDK\Test\StructType\Set_DNS_Zone_Container|bool
     */
    public function Set_DNS_Zone(\SubregSDK\Test\StructType\Set_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Set_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Set_Autorenew_Container|\SubregSDK\Test\StructType\Set_DNS_Zone_Container|\SubregSDK\Test\StructType\Set_Prices_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
