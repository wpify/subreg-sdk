<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Users ServiceType
 * @subpackage Services
 */
class Users extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Users_List
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Users_List $parameters
     * @return \SubregSDK\Test\StructType\Users_List_Container|bool
     */
    public function Users_List(\SubregSDK\Test\StructType\Users_List $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Users_List', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Users_List_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
