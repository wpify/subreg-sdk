<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Anycast ServiceType
 * @subpackage Services
 */
class Anycast extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Anycast_ADD_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Anycast_ADD_Zone $parameters
     * @return \SubregSDK\Test\StructType\Anycast_ADD_Zone_Container|bool
     */
    public function Anycast_ADD_Zone(\SubregSDK\Test\StructType\Anycast_ADD_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Anycast_ADD_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Anycast_Remove_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Anycast_Remove_Zone $parameters
     * @return \SubregSDK\Test\StructType\Anycast_Remove_Zone_Container|bool
     */
    public function Anycast_Remove_Zone(\SubregSDK\Test\StructType\Anycast_Remove_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Anycast_Remove_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Anycast_List_Domains
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Anycast_List_Domains $parameters
     * @return \SubregSDK\Test\StructType\Anycast_List_Domains_Container|bool
     */
    public function Anycast_List_Domains(\SubregSDK\Test\StructType\Anycast_List_Domains $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Anycast_List_Domains', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Anycast_ADD_Zone_Container|\SubregSDK\Test\StructType\Anycast_List_Domains_Container|\SubregSDK\Test\StructType\Anycast_Remove_Zone_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
