<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Modify ServiceType
 * @subpackage Services
 */
class Modify extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Modify_DNS_Record
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Modify_DNS_Record $parameters
     * @return \SubregSDK\Test\StructType\Modify_DNS_Record_Container|bool
     */
    public function Modify_DNS_Record(\SubregSDK\Test\StructType\Modify_DNS_Record $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Modify_DNS_Record', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Modify_DNS_Record_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
