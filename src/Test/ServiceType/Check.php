<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Check ServiceType
 * @subpackage Services
 */
class Check extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Check_Domain
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Check_Domain $parameters
     * @return \SubregSDK\Test\StructType\Check_Domain_Container|bool
     */
    public function Check_Domain(\SubregSDK\Test\StructType\Check_Domain $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Check_Domain', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Check_Object
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Check_Object $parameters
     * @return \SubregSDK\Test\StructType\Check_Object_Container|bool
     */
    public function Check_Object(\SubregSDK\Test\StructType\Check_Object $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Check_Object', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Check_Domain_Container|\SubregSDK\Test\StructType\Check_Object_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
