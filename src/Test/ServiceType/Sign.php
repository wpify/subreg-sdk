<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Sign ServiceType
 * @subpackage Services
 */
class Sign extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Sign_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Sign_DNS_Zone $parameters
     * @return \SubregSDK\Test\StructType\Sign_DNS_Zone_Container|bool
     */
    public function Sign_DNS_Zone(\SubregSDK\Test\StructType\Sign_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Sign_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Sign_DNS_Zone_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
