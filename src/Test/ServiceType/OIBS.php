<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for OIBS ServiceType
 * @subpackage Services
 */
class OIBS extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named OIB_Search
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\OIB_Search $parameters
     * @return \SubregSDK\Test\StructType\OIB_Search_Container|bool
     */
    public function OIB_Search(\SubregSDK\Test\StructType\OIB_Search $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('OIB_Search', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\OIB_Search_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
