<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Login ServiceType
 * @subpackage Services
 */
class Login extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Login
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Login $parameters
     * @return \SubregSDK\Test\StructType\Login_Container|bool
     */
    public function Login(\SubregSDK\Test\StructType\Login $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Login', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Login_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
