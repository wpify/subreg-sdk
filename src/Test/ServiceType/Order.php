<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Order ServiceType
 * @subpackage Services
 */
class Order extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Order_Payment
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Order_Payment $parameters
     * @return \SubregSDK\Test\StructType\Order_Payment_Container|bool
     */
    public function Order_Payment(\SubregSDK\Test\StructType\Order_Payment $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Order_Payment', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Order_Payment_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
