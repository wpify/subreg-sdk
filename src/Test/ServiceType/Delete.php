<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Delete ServiceType
 * @subpackage Services
 */
class Delete extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Delete_DNS_Zone
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Delete_DNS_Zone $parameters
     * @return \SubregSDK\Test\StructType\Delete_DNS_Zone_Container|bool
     */
    public function Delete_DNS_Zone(\SubregSDK\Test\StructType\Delete_DNS_Zone $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Delete_DNS_Zone', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Delete_DNS_Record
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Delete_DNS_Record $parameters
     * @return \SubregSDK\Test\StructType\Delete_DNS_Record_Container|bool
     */
    public function Delete_DNS_Record(\SubregSDK\Test\StructType\Delete_DNS_Record $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Delete_DNS_Record', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Delete_DNS_Record_Container|\SubregSDK\Test\StructType\Delete_DNS_Zone_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
