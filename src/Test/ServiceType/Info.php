<?php

namespace SubregSDK\Test\ServiceType;

use \WsdlToPhp\PackageBase\AbstractSoapClientBase;

/**
 * This class stands for Info ServiceType
 * @subpackage Services
 */
class Info extends AbstractSoapClientBase
{
    /**
     * Method to call the operation originally named Info_Domain
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Info_Domain $parameters
     * @return \SubregSDK\Test\StructType\Info_Domain_Container|bool
     */
    public function Info_Domain(\SubregSDK\Test\StructType\Info_Domain $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Info_Domain', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Info_Domain_CZ
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Info_Domain_CZ $parameters
     * @return \SubregSDK\Test\StructType\Info_Domain_CZ_Container|bool
     */
    public function Info_Domain_CZ(\SubregSDK\Test\StructType\Info_Domain_CZ $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Info_Domain_CZ', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Info_Contact
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Info_Contact $parameters
     * @return \SubregSDK\Test\StructType\Info_Contact_Container|bool
     */
    public function Info_Contact(\SubregSDK\Test\StructType\Info_Contact $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Info_Contact', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Info_Object
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Info_Object $parameters
     * @return \SubregSDK\Test\StructType\Info_Object_Container|bool
     */
    public function Info_Object(\SubregSDK\Test\StructType\Info_Object $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Info_Object', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Method to call the operation originally named Info_Order
     * @uses AbstractSoapClientBase::getSoapClient()
     * @uses AbstractSoapClientBase::setResult()
     * @uses AbstractSoapClientBase::getResult()
     * @uses AbstractSoapClientBase::saveLastError()
     * @param \SubregSDK\Test\StructType\Info_Order $parameters
     * @return \SubregSDK\Test\StructType\Info_Order_Container|bool
     */
    public function Info_Order(\SubregSDK\Test\StructType\Info_Order $parameters)
    {
        try {
            $this->setResult($this->getSoapClient()->__soapCall('Info_Order', array(
                $parameters,
            ), array(), array(), $this->outputHeaders));
            return $this->getResult();
        } catch (\SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);
            return false;
        }
    }
    /**
     * Returns the result
     * @see AbstractSoapClientBase::getResult()
     * @return \SubregSDK\Test\StructType\Info_Contact_Container|\SubregSDK\Test\StructType\Info_Domain_Container|\SubregSDK\Test\StructType\Info_Domain_CZ_Container|\SubregSDK\Test\StructType\Info_Object_Container|\SubregSDK\Test\StructType\Info_Order_Container
     */
    public function getResult()
    {
        return parent::getResult();
    }
}
